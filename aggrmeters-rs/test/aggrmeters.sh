#! /bin/bash -ev
set -u
# Run the aggregator for development testing
cd ~/gitrepos/enmprv
clear
ping -c 2 -w 10 10.1.0.3 # reconnect with p1encryptor by using wg tunnel endpoint
time cargo run --package aggrmeters-rs aggrmeters-rs/aggrmeters.toml aggrmeters-rs/agrmlog4rs.yaml
