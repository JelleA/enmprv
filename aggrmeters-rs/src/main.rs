use std::env;
use std::fs::File;
use std::io::Read;
use std::net::{IpAddr, SocketAddr};
use std::process::exit;
use std::str::FromStr;
use std::sync::Arc;
use std::sync::{mpsc, Mutex};
use std::thread;

use anyhow::{anyhow, Error};
use enmplib_rs::util::RunState;
use log::{error, info, warn};
use serde::Deserialize;

use enmplib_rs::{
    measurement::{Measurement, MeasurementTime},
    openssl::OpenSslSigVerifier,
    util::{wait_clock_synchronized, DurationConfig},
};

use crate::aggrka::AggregatedMeasurement;

mod aggrka;
mod aggrp1;

// Measurements as received from a p1encryptor in text message:
#[derive(Debug, Clone)]
pub(crate) struct MeasurementsMessage {
    pub msrmnts_non_enc: Vec<Measurement>,
    pub msrmnts_enc: Vec<Measurement>,
}

impl MeasurementsMessage {
    fn total_num_msrmnts(&self) -> usize {
        self.msrmnts_non_enc.len() + self.msrmnts_enc.len()
    }
}

// Message on queue from aggregator p1 input and interval creator to aggregator key adder interface:
pub(crate) enum AggregationMessage {
    EndInterval,
    Measurements(MeasurementsMessage),
}

const CONFIG_FILE_NAME: &str = "aggrmeters.toml";
const LOG_CONFIG_FILE_NAME: &str = "agrmlog4rs.yaml";

#[derive(Deserialize, Debug)]
pub(crate) struct PrivAggrConfig {
    wait_clock_sync: DurationConfig,
    bind_address_p1_encryptors: String,
    udp_port_p1_encryptors: u16,
    ten_pow_max_decrypt: u16,
    aggregator_id: String,
    remote_address_key_adder: String,
    remote_port_key_add: u16,
    msrmnts_partial_request: u64,
    max_keep_eq_ids: u64,
    aggregation_interval: DurationConfig,
    max_secs_ahead: i64,
    key_adder_receive_timeout: DurationConfig,
    max_queue_size: u64,
    key_adder_public_key: String,
    temp_dir: String,
    grid_connection_eans_by_public_keys: toml::Value,
}

fn usage_exit(exit_code: i32) -> ! {
    eprintln!("argument: -v");
    eprintln!("  show invocation and version number");
    eprintln!("arguments: [config_file [log_config_file]]");
    eprintln!("  config_file:     default {CONFIG_FILE_NAME}");
    eprintln!("  log_config_file: default {LOG_CONFIG_FILE_NAME}");
    exit(exit_code);
}

fn error_exit(e: Error) -> ! {
    error!("{e:#}, exiting");
    exit(1)
}

fn main() {
    let mut args = env::args();
    let binary_name = args.next().unwrap_or("meter aggregator".to_owned());
    let config_file_name = args.next().unwrap_or_else(|| CONFIG_FILE_NAME.to_string());
    let version = env!("CARGO_PKG_VERSION");
    if config_file_name == "-v" {
        println!("{binary_name} version {version}");
        exit(0);
    }
    if config_file_name == "-h" {
        usage_exit(0);
    }
    let log_config_file_name = args
        .next()
        .unwrap_or_else(|| LOG_CONFIG_FILE_NAME.to_string());
    if args.next().is_some() {
        usage_exit(1);
    }

    log4rs::init_file(
        &log_config_file_name,
        log4rs::config::Deserializers::default(),
    )
    .unwrap_or_else(|e| {
        eprintln!("{e:?} at log4rs::init_file {log_config_file_name}, exiting");
        exit(1);
    });
    info!(
        "starting meter aggregator version {version} as {binary_name} user {} pid {}",
        std::env::var("USER").unwrap_or("".to_owned()),
        std::process::id()
    );
    let run_state = match RunState::new() {
        Err(e) => error_exit(e.context("Runstate::new()")),
        Ok(r) => r,
    };

    let mut toml_buf = Vec::<u8>::new();
    {
        let mut config_file = File::open(&config_file_name).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(format!("File::open {config_file_name}")));
        });
        let _ = config_file.read_to_end(&mut toml_buf).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(format!("read_to_end {config_file_name}")));
        });
    }
    let toml_str = std::str::from_utf8(&toml_buf).unwrap_or_else(|e| {
        error_exit(Error::new(e).context(config_file_name.to_owned()));
    });
    let config = Arc::new(
        toml::from_str::<PrivAggrConfig>(toml_str).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(config_file_name));
        }),
    );

    let keyadder_sig_verifier =
        OpenSslSigVerifier::new(&config.key_adder_public_key, &config.temp_dir).unwrap_or_else(
            |e| {
                error_exit(e.context("open_ssl_sig_verifier() for keyadder_sig_verifier"));
            },
        );

    let aggregation_interval = config.aggregation_interval.std_duration();
    if aggregation_interval.as_secs() == 0 {
        error_exit(anyhow!("zero aggregation interval, too small"));
    }

    let key_adder_socket_addr = match IpAddr::from_str(&config.remote_address_key_adder) {
        Err(e) => {
            error_exit(Error::new(e).context(format!(
                "remote_address_key_adder {:?}",
                &config.remote_address_key_adder
            )));
        },
        Ok(ip_addr) => SocketAddr::new(ip_addr, config.remote_port_key_add),
    };

    if let Err(e) = wait_clock_synchronized(config.wait_clock_sync.std_duration().as_secs()) {
        error_exit(e.context("wait_clock_synchronized"));
    }

    let last_aggr_result = Arc::<Mutex<Vec<AggregatedMeasurement>>>::new(Mutex::new(Vec::new()));

    let (sender_mkvs, receiver_msrmnt_msgs) =
        mpsc::sync_channel::<AggregationMessage>(config.max_queue_size as usize);
    let sender_intervals = sender_mkvs.clone();

    let config_clone = Arc::clone(&config);
    let last_aggr_result_clone = Arc::clone(&last_aggr_result);

    let bind_address_p1_encryptors = config.bind_address_p1_encryptors.clone();
    let send_thread = thread::spawn(move || {
        if let Err(e) = aggrp1::verify_msgs_from_sock_to_queue(
            &config_clone,
            &bind_address_p1_encryptors,
            &sender_mkvs,
            &last_aggr_result_clone,
        ) {
            error_exit(e.context("aggrp1::verify_msgs_from_sock_to_queue"));
        }
    });

    let interval_thread = thread::spawn(move || loop {
        thread::sleep(aggregation_interval);
        if let Err(queue_send_err) = sender_intervals.try_send(AggregationMessage::EndInterval) {
            match queue_send_err {
                mpsc::TrySendError::Full(_full_error) => {
                    warn!("queue full_error, no aggregation interval ending");
                },
                mpsc::TrySendError::Disconnected(ref _option_mkvs) => {
                    error_exit(Error::new(queue_send_err).context("aggregation queue"));
                },
            }
        }
    });

    let config_clone = Arc::clone(&config);
    if let Err(e) = aggrka::aggr_msrmnts_from_queue(
        &config_clone,
        &receiver_msrmnt_msgs,
        &key_adder_socket_addr,
        &keyadder_sig_verifier,
        &run_state,
        &Arc::clone(&last_aggr_result),
    ) {
        error_exit(e.context("aggr_msrmnts_from_queue"));
    }

    if let Err(send_thread_join_error) = send_thread.join() {
        error_exit(anyhow!("send_thread_join_error {send_thread_join_error:?}"));
    }

    if let Err(interval_thread_join_error) = interval_thread.join() {
        error_exit(anyhow!(
            "interval_thread_join_error {interval_thread_join_error:?}"
        ));
    }
    info!("normal exit");
    exit(0);
}
