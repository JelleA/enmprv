use std::collections::HashSet;
use std::io::ErrorKind;
use std::net::{SocketAddr, UdpSocket};
use std::rc::Rc;
use std::str::FromStr;
use std::sync::{mpsc, Mutex};
use std::time::Duration;

use anyhow::{anyhow, Error, Result};
use chrono::{DateTime, Utc};
use enmplib_rs::util::{time_stamp_milli_secs, time_stamp_to_string};
use log::{error, warn};

use enmplib_rs::{
    bigintpowten::BigIntPowTen,
    bincoded::{slice_u8_to_string, MeasurementType},
    openssl::{
        sig_verifier_by_grid_connection_ean_from_config, OpenSslSigVerifier, SigVerifierByGridEan,
    },
    util::{addr_port_str, ip_addr_in_wg_intf, time_stamp_from_str, udp_bind},
};

use crate::aggrka::{AggregatedMeasurement, AggregatedMeasurementAnonymous};
use crate::{
    AggregationMessage, Measurement, MeasurementTime, MeasurementsMessage, PrivAggrConfig,
};

const EAN_KEY: &str = "ean";
const MUTC_KEY: &str = "mutc";
const DOT_ENC_SUFFIX: &str = ".enc"; // suffix for keys of encrypted values.

struct ReceivedMessage<'a> {
    received_dt: DateTime<Utc>,
    src_addr_port: SocketAddr,
    num_bytes_received: usize,
    receive_buffer: &'a [u8],
}

impl<'a> ReceivedMessage<'a> {
    fn new(
        received_dt: DateTime<Utc>,
        src_addr_port: SocketAddr,
        num_bytes_received: usize,
        receive_buffer: &'a [u8],
    ) -> Self {
        ReceivedMessage {
            received_dt,
            src_addr_port,
            num_bytes_received,
            receive_buffer,
        }
    }

    fn decode_and_verify(
        &self,
        sig_verifier_by_grid_connection_ean: &SigVerifierByGridEan,
        msrmnt_max_secs_ahead: i64, // verify measurements not too far in the future.
    ) -> Result<MeasurementsMessage> {
        // Input: the message from the p1encryptor
        /* The example content below is preceded by two \0 zero bytes to indicate the message version,
          and after the content there is an empty line, followed by the bytes of the signature:

               \0\0mutc: 2022-02-01T19:30:55.922
               ean: 90123456789123456789
               el.kWh: 53971.801
               el.kWh.enc: 51875772575.782
               el.kW: 0.780
               el.kW.enc: 56409102165.163
               mutc: 2022-02-01T19:00:00.000
               ean: 90223456789123456789
               ng.nl.m3: 18269.703
               ng.nl.m3.enc: 66427870288.491

               (bytes of signature)

          The goal of this message is aggregation of the latest measurement values
          in an aggregation interval.
        */
        const NUM_VERSION_BYTES: usize = 2;
        let expected_version: [u8; NUM_VERSION_BYTES] = [0; NUM_VERSION_BYTES];
        if self.num_bytes_received < NUM_VERSION_BYTES + 6 {
            return Err(anyhow!(
                "message too short {:?}",
                &self.receive_buffer[..self.num_bytes_received]
            ));
        }
        if self.receive_buffer[..NUM_VERSION_BYTES] != expected_version {
            return Err(anyhow!(
                "unknown message version {:?}",
                &self.receive_buffer[..NUM_VERSION_BYTES]
            ));
        }

        let receive_str = match std::str::from_utf8(
            &self.receive_buffer[NUM_VERSION_BYTES..self.num_bytes_received],
        ) {
            Ok(s) => s,
            Err(utf8_err) => match std::str::from_utf8(
                &self.receive_buffer[NUM_VERSION_BYTES..NUM_VERSION_BYTES + utf8_err.valid_up_to()],
            ) {
                Ok(s) => s,
                Err(utf8_err) => {
                    // very unlikely, normally there are 256 trailing signature bytes
                    return Err(Error::new(utf8_err).context(format!(
                        "repeated utf8_error for message received at {:?}",
                        self.received_dt
                    )));
                },
            },
        }
        .to_string();

        // Interpret the characters until there is an empty line before the signature
        let mut last_line = String::new();
        let mut signed_data_end_pos_opt: Option<usize> = None;

        let mut msrmnts: Vec<Measurement> = Vec::new();
        let mut msrmnts_enc: Vec<Measurement> = Vec::new();

        let mut last_ean_opt: Option<String> = None;
        let mut last_mutc_opt: Option<MeasurementTime> = None;

        let mut grid_connection_eans = Vec::<Vec<u8>>::new();

        for (byte_pos, c) in receive_str.char_indices() {
            if c != '\n' {
                last_line.push(c);
                continue;
            }
            if last_line.is_empty() {
                signed_data_end_pos_opt = Some(NUM_VERSION_BYTES + byte_pos - 1);
                break;
            }
            let Some(colon_space_pos) = last_line.find(": ") else {
                return Err(anyhow!(
                    "expected empty line, or key: value. last_line: {last_line}"
                ));
            };

            let key = &last_line[..colon_space_pos];
            let val = &last_line[colon_space_pos + 2..];
            let max_allowed_msmrnt_timestamp = self.received_dt.timestamp() + msrmnt_max_secs_ahead;
            match key {
                EAN_KEY => {
                    last_ean_opt = Some(val.to_string());
                    grid_connection_eans.push(val.as_bytes().to_vec());
                },
                MUTC_KEY => {
                    // time stamp, example: 2021-05-25T13:05:51.480Z
                    match time_stamp_from_str(val) {
                        Err(e) => {
                            return Err(
                                e.context(format!("time_stamp_from_str {val}, line: {last_line}"))
                            );
                        },
                        Ok(dt_utc) => {
                            if dt_utc.timestamp() > max_allowed_msmrnt_timestamp {
                                return Err(anyhow!(
                                    "measurement time {dt_utc:?} more than {msrmnt_max_secs_ahead:?} secs after receive time {:?}",
                                    self.received_dt
                                ));
                            }
                            last_mutc_opt = Some(dt_utc);
                        },
                    }
                },
                _ => {
                    // measurement type key and measured value
                    let Some(ref ean) = last_ean_opt else {
                        return Err(anyhow!("missing ean before line {last_line}"));
                    };
                    let Some(mutc) = last_mutc_opt else {
                        return Err(anyhow!("missing mutc before line {last_line}"));
                    };
                    let (encrypted, msrmnt_type_key) = match key.find(DOT_ENC_SUFFIX) {
                        Some(dot_enc_pos) => (true, &key[..dot_enc_pos]),
                        None => (false, key),
                    };
                    match MeasurementType::try_from(msrmnt_type_key) {
                        Err(e) => {
                            return Err(e.context(format!(
                                "MeasurementType::try_from(), on line {last_line}"
                            )));
                        },
                        Ok(msrmnt_type) => match BigIntPowTen::from_str(val) {
                            Err(e) => {
                                return Err(e.context(format!(
                                    "BigIntPowTen::from_str(), on line: {last_line}"
                                )));
                            },
                            Ok(value) => {
                                if encrypted {
                                    &mut msrmnts_enc
                                } else {
                                    &mut msrmnts
                                }
                                .push(Measurement {
                                    ref_id: ean.to_string(),
                                    unit: msrmnt_type,
                                    utc_time_stamp: mutc,
                                    value,
                                });
                            },
                        },
                    }
                },
            }
            last_line.clear();
        }

        let Some(signed_data_end_pos) = signed_data_end_pos_opt else {
            return Err(anyhow!("missing empty line before signature"));
        };

        if msrmnts.is_empty() && msrmnts_enc.is_empty() {
            return Err(anyhow!("no measurements in measurements message"));
        }

        /* Each message may contain at most one measured value
          for a combination of ean and measurement type (el.kWh, el.kW, ng.nl.m3 etc)
          for the unencrypted values, and the .enc variants for encrypted values).
        */
        check_unique_msrmnts(&msrmnts, "unencrypted")?;
        check_unique_msrmnts(&msrmnts_enc, "encrypted")?;

        let signed_data = &self.receive_buffer[NUM_VERSION_BYTES..signed_data_end_pos];
        let signature = &self.receive_buffer[signed_data_end_pos + 2..self.num_bytes_received];

        // There may be only one signature verifier for the grid connection eans in the message.
        let mut sig_verifier_opt: Option<&Rc<OpenSslSigVerifier>> = None;
        for grid_connection_ean in &grid_connection_eans {
            if let Some(sig_verifier_for_ean) =
                sig_verifier_by_grid_connection_ean.get(grid_connection_ean)
            {
                if let Some(prev_sig_verifier) = sig_verifier_opt {
                    if prev_sig_verifier != sig_verifier_for_ean {
                        return Err(anyhow!(
                            "multiple signature verifiers for the grid connection eans in\n{}",
                            slice_u8_to_string(signed_data),
                        ));
                    }
                } else {
                    sig_verifier_opt = Some(sig_verifier_for_ean);
                }
            } else {
                return Err(anyhow!(
                    "no signature verifier for {}",
                    slice_u8_to_string(grid_connection_ean)
                ));
            }
        }
        let Some(sig_verifier) = sig_verifier_opt else {
            return Err(anyhow!(
                "unexpected missing signature verifier for\n{}",
                slice_u8_to_string(signed_data)
            ));
        };

        match sig_verifier.verify_signature(signed_data, signature) {
            Err(e) => Err(e.context("verify_signature")),
            Ok(verified) => {
                if verified {
                    Ok(MeasurementsMessage {
                        msrmnts_non_enc: msrmnts,
                        msrmnts_enc,
                    })
                } else {
                    Err(anyhow!("incorrect signature"))
                }
            },
        }
    }
}

fn check_unique_msrmnts(msrmnts: &[Measurement], enc_mes: &str) -> Result<()> {
    type EanTypeCombi<'a> = (&'a str, MeasurementType);
    let mut ean_type_combis: HashSet<EanTypeCombi> = HashSet::new();
    for msrmnt in msrmnts {
        let combi: EanTypeCombi = (msrmnt.ref_id.as_str(), msrmnt.unit);
        if !ean_type_combis.insert(combi) {
            return Err(anyhow!(
                "combination {combi:?} present more than once in {enc_mes} measurements"
            ));
        }
    }
    Ok(())
}

pub(crate) fn verify_msgs_from_sock_to_queue(
    config: &PrivAggrConfig,
    local_ip_addr: &str,
    sync_sender: &mpsc::SyncSender<AggregationMessage>,
    last_aggr_result: &Mutex<Vec<AggregatedMeasurement>>,
) -> Result<()> {
    let local_port = config.udp_port_p1_encryptors;
    let local_addr_port = addr_port_str(local_ip_addr, &local_port.to_string())?;
    let max_secs_ahead = config.max_secs_ahead;

    let grid_connection_eans_by_public_keys = &config.grid_connection_eans_by_public_keys;
    let temp_dir = &config.temp_dir;

    let sig_verifier_by_grid_connection_ean = sig_verifier_by_grid_connection_ean_from_config(
        "grid_connection_eans_by_public_keys",
        grid_connection_eans_by_public_keys,
        temp_dir,
    )?;

    loop {
        let p1_encryptors_udp_socket = match udp_bind(&local_addr_port) {
            Ok(sock) => sock,
            Err(e) => {
                error!("{e:#} at: udp_bind, sleeping one aggregation_interval");
                std::thread::sleep(config.aggregation_interval.std_duration());
                continue;
            },
        };
        if let Err(e) = p1_encryptors_udp_socket.set_read_timeout(Some(Duration::new(15, 0))) {
            return Err(Error::new(e).context("set_read_timeout"));
        }
        // check the local wg interface after each udp socket creation:
        match ip_addr_in_wg_intf(local_ip_addr) {
            Err(e) => {
                return Err(e.context(format!("ip_addr_in_wg_intf {local_ip_addr}")));
            },
            Ok(res) => {
                if !res {
                    return Err(anyhow!(
                        "error: no wireguard ip interface for {local_ip_addr}"
                    ));
                }
            },
        }

        loop {
            // receive udp messages as long as p1_encryptors_udp_socket works normally
            let mut receive_buffer = [0u8; 1024];
            match p1_encryptors_udp_socket.recv_from(&mut receive_buffer) {
                Err(sock_err) => {
                    if sock_err.kind() == ErrorKind::WouldBlock {
                        warn!("timeout at recv_from (is the wg tunnel up?)");
                        match p1_encryptors_udp_socket.take_error() {
                            Err(error) => error!("UdpSocket.take_error {error:?}"),
                            Ok(Some(error)) => error!("UdpSocket take_error Some {error:?}"),
                            Ok(None) => {}, // normal timeout
                        }
                        break; // stop using p1_encryptors_udp_socket
                    }
                    return Err(Error::new(sock_err).context("recv_from"));
                },
                Ok((num_bytes_received, src_addr_port)) => {
                    let rcvd_msg = ReceivedMessage::new(
                        time_stamp_milli_secs(),
                        src_addr_port,
                        num_bytes_received,
                        &receive_buffer,
                    );
                    // CHECKME: keep a temporary list of src_addr_port's that failed signature verification
                    // so the packet can be dropped early?
                    if num_bytes_received == rcvd_msg.receive_buffer.len() {
                        warn!(
                            "message too long: {num_bytes_received} bytes from {src_addr_port:?} at {:?}, dropped",
                            rcvd_msg.received_dt
                        );
                    } else {
                        let reply: String;
                        match rcvd_msg
                            .decode_and_verify(&sig_verifier_by_grid_connection_ean, max_secs_ahead)
                        {
                            Err(e) => {
                                error!("{e:#} at decode_and_verify");
                                // reply V to p1 encryptor: decode_and_verify failed
                                reply = "V".to_string();
                            },
                            Ok(msrmnts_msg) => {
                                if let Err(queue_send_err) = sync_sender
                                    .try_send(AggregationMessage::Measurements(msrmnts_msg.clone()))
                                {
                                    match queue_send_err {
                                        mpsc::TrySendError::Full(_full_error) => {
                                            // _full_error contains the dropped message
                                            warn!(
                                            "TrySendError::Full, dropping verified message from {:?} received at {:?}",
                                            rcvd_msg.src_addr_port, rcvd_msg.received_dt);
                                            reply = "-".to_string(); // message verified ok, and then dropped
                                        },
                                        mpsc::TrySendError::Disconnected(_option_msrmnt_msg) => {
                                            return Err(anyhow!(
                                                "TrySendError::Disconnected sync_sender"
                                            ));
                                        },
                                    }
                                } else {
                                    let received_eans = msrmnts_msg
                                        .msrmnts_non_enc
                                        .iter()
                                        .chain(msrmnts_msg.msrmnts_enc.iter())
                                        .map(|msrmnt| msrmnt.ref_id.clone())
                                        .collect::<HashSet<_>>();

                                    match last_aggr_result.lock() {
                                        Ok(res) => {
                                            let mut result_with_received_ean: Vec<
                                                AggregatedMeasurementAnonymous,
                                            > = (*res)
                                                .iter()
                                                .filter_map(|aggr_msrmnt| {
                                                    if received_eans.iter().any(|ean| {
                                                        aggr_msrmnt.original_eans.contains(ean)
                                                    }) {
                                                        Some(AggregatedMeasurementAnonymous {
                                                            aggr_measurement: aggr_msrmnt
                                                                .aggr_measurement
                                                                .clone(),
                                                            total_delay_ms: aggr_msrmnt
                                                                .total_delay_ms,
                                                            num_msrmnts: aggr_msrmnt
                                                                .original_eans
                                                                .len()
                                                                as u64,
                                                        })
                                                    } else {
                                                        None
                                                    }
                                                })
                                                .collect::<Vec<_>>();
                                            result_with_received_ean.sort_by(|a, b| {
                                                a.total_delay_ms.cmp(&b.total_delay_ms)
                                            });
                                            // reply with initial dot: received ok.
                                            let aggr_result =
                                                datagram_ams(&result_with_received_ean);
                                            reply = if aggr_result.is_empty() {
                                                ".".to_string()
                                            } else {
                                                format!(".\n{aggr_result}")
                                            };
                                        },
                                        Err(e) => {
                                            error!("{e:?} on last_aggr_result");
                                            reply = ".".to_string(); // ok, received message accepted
                                        },
                                    }
                                }
                            },
                        }
                        send_mes_udp(&reply, &p1_encryptors_udp_socket, &src_addr_port)?;
                    }
                },
            }
        }
    }
}

fn send_mes_udp(message: &str, udp_socket: &UdpSocket, addr_port: &SocketAddr) -> Result<()> {
    let mes_bytes = message.as_bytes();
    match udp_socket.send_to(mes_bytes, addr_port) {
        Ok(size) => {
            if size == mes_bytes.len() {
                Ok(())
            } else {
                Err(anyhow!(
                    "send_to sent {size} of {} bytes of {message}",
                    mes_bytes.len(),
                ))
            }
        },
        Err(e) => Err(Error::new(e).context(format!("send_to message {message}"))),
    }
}

fn datagram_ams(ag_msrmnts: &[AggregatedMeasurementAnonymous]) -> String {
    let colon_space = ": ";
    let mut lines = Vec::<String>::new();
    let mut last_ean_opt: Option<&str> = None;
    let mut last_mutc_opt: Option<&DateTime<Utc>> = None;
    let mut last_delay_ms_opt: Option<i64> = None;
    let mut last_num_msrmnts: u64 = 0;
    for ag_msrmnt in ag_msrmnts {
        let msrmnt = &ag_msrmnt.aggr_measurement;

        let put_ean_line = if let Some(last_ean) = last_ean_opt {
            if last_ean == msrmnt.ref_id {
                false
            } else {
                last_ean_opt = Some(&msrmnt.ref_id);
                true
            }
        } else {
            last_ean_opt = Some(&msrmnt.ref_id);
            true
        };
        if put_ean_line {
            lines.push([EAN_KEY, &msrmnt.ref_id].join(colon_space));
        }

        let put_mutc_line = if let Some(last_mutc) = last_mutc_opt {
            if *last_mutc == msrmnt.utc_time_stamp {
                false
            } else {
                last_mutc_opt = Some(&msrmnt.utc_time_stamp);
                true
            }
        } else {
            last_mutc_opt = Some(&msrmnt.utc_time_stamp);
            true
        };
        if put_mutc_line {
            lines.push([MUTC_KEY, &time_stamp_to_string(&msrmnt.utc_time_stamp)].join(colon_space));
        }

        if last_num_msrmnts != ag_msrmnt.num_msrmnts {
            last_num_msrmnts = ag_msrmnt.num_msrmnts;
            lines.push(format!("num_msrmnts{colon_space}{}", ag_msrmnt.num_msrmnts));
        }

        let put_delay_ms_line = if let Some(last_delay_ms) = last_delay_ms_opt {
            if last_delay_ms == ag_msrmnt.total_delay_ms {
                false
            } else {
                last_delay_ms_opt = Some(ag_msrmnt.total_delay_ms);
                true
            }
        } else {
            last_delay_ms_opt = Some(ag_msrmnt.total_delay_ms);
            true
        };
        if put_delay_ms_line {
            lines.push(format!(
                "total_delay_ms{colon_space}{}",
                ag_msrmnt.total_delay_ms
            ));
        }

        lines.push([msrmnt.unit.to_str(), &msrmnt.value.to_string(3)].join(colon_space));
    }
    lines.join("\n")
}
