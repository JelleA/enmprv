use std::io;
use std::io::Write;
use std::net::{SocketAddr, TcpStream};
use std::str::FromStr;
use std::sync::mpsc;
use std::{
    collections::{
        hash_map::Entry::{Occupied, Vacant},
        BTreeSet, HashMap, HashSet,
    },
    sync::Mutex,
};

use anyhow::{anyhow, Context, Error, Result};
use bincode::Options;
use chrono::{DateTime, Utc};
use log::{debug, error, info, log, trace, warn};
use num::BigInt;

use enmplib_rs::bigintpowten::BigIntPowTen;
use enmplib_rs::bincoded::{
    get_bincode_options, slice_u8_to_string, EncKeyAddReply, EncKeyAddRequest, EncryptionResult,
    GridConnectionEanMeasTimeTypesBytes, IncrementalRequest, MeasurementType,
    MeasurementTypeBitVec,
};
use enmplib_rs::measurement::{MeasuredValue, Measurement, MeasurementTime};
use enmplib_rs::openssl::OpenSslSigVerifier;
use enmplib_rs::util::{
    receive_version_size_message, send_version_size_message, tcp_stream_in_wg_intf,
    time_stamp_milli_secs, time_stamp_to_string, RunState,
};

use crate::{AggregationMessage, MeasurementsMessage, PrivAggrConfig};

#[derive(Debug, Clone)]
pub(crate) struct AggregatedMeasurement {
    pub aggr_measurement: Measurement, // ean of the aggregator, utc of the aggregation, aggregated measurement type and value.
    pub total_delay_ms: i64,           // from measurement utc's to aggregation utc.
    pub original_eans: HashSet<String>, // used for reporting the full aggregation, for example for further aggregation.
}

#[derive(Debug)]
pub(crate) struct AggregatedMeasurementAnonymous {
    pub aggr_measurement: Measurement, // ean of the aggregator, utc of the aggregation, aggregated measurement type and value.
    pub total_delay_ms: i64,           // from measurement utc's to aggregation utc.
    pub num_msrmnts: u64, // used for reporting the aggregation back to measurements originator
}

fn aggr_key_add_req_from_mrsmnts(
    ndkr_msrmnts_vec: &mut [Measurement], // mut to allow sorting.
    request_stage: IncrementalRequest,
) -> Result<Option<EncKeyAddRequest>> {
    // Compress by making sure that the ean/time combination is there
    // for every measurement type bit set in the bitvec,
    // sort the ndkr_msrmnts_vec by ean/time
    use std::cmp::Ordering::{Equal, Greater, Less};
    ndkr_msrmnts_vec.sort_unstable_by(|a, b| match a.ref_id.cmp(&b.ref_id) {
        Less => Less,
        Greater => Greater,
        Equal => a.utc_time_stamp.cmp(&b.utc_time_stamp),
    });
    if let Some(first_msrmnt) = ndkr_msrmnts_vec.get(0) {
        let mut key_add_request = EncKeyAddRequest {
            request_stage,
            gc_ean_meas_dt_types_vec: Vec::new(),
        };

        let mut last_ean = &first_msrmnt.ref_id;
        let mut last_dt_utc = &first_msrmnt.utc_time_stamp;
        let mut encr_msrmnt_types_bitvec = MeasurementTypeBitVec::new();
        encr_msrmnt_types_bitvec.set_bit(first_msrmnt.unit, true)?;
        for msrmnt in &ndkr_msrmnts_vec[1..] {
            if &msrmnt.ref_id == last_ean && &msrmnt.utc_time_stamp == last_dt_utc {
                encr_msrmnt_types_bitvec.set_bit(msrmnt.unit, true)?;
            } else {
                let meas_time_stamp = time_stamp_to_string(last_dt_utc);
                key_add_request.gc_ean_meas_dt_types_vec.push(
                    GridConnectionEanMeasTimeTypesBytes {
                        grid_connection_ean: last_ean.as_bytes().to_vec(),
                        meas_dt: meas_time_stamp.as_bytes().to_vec(),
                        msrmnt_types_bit_vec: encr_msrmnt_types_bitvec,
                    },
                );
                last_ean = &msrmnt.ref_id;
                last_dt_utc = &msrmnt.utc_time_stamp;
                encr_msrmnt_types_bitvec = MeasurementTypeBitVec::new();
                encr_msrmnt_types_bitvec.set_bit(msrmnt.unit, true)?;
            }
        }
        let meas_time_stamp = time_stamp_to_string(last_dt_utc);
        // CHECKME: duplicate code, perhaps use a function
        key_add_request
            .gc_ean_meas_dt_types_vec
            .push(GridConnectionEanMeasTimeTypesBytes {
                grid_connection_ean: last_ean.as_bytes().to_vec(),
                meas_dt: meas_time_stamp.as_bytes().to_vec(),
                msrmnt_types_bit_vec: encr_msrmnt_types_bitvec,
            });
        Ok(Some(key_add_request))
    } else {
        // no measurements to be decrypted:
        match request_stage {
            IncrementalRequest::Partial => Ok(None),
            IncrementalRequest::Final => {
                // Final EncKeyAddRequest may be empty
                Ok(Some(EncKeyAddRequest {
                    request_stage,
                    gc_ean_meas_dt_types_vec: Vec::new(),
                }))
            },
        }
    }
}

type MeasurementTimeByEanType = HashMap<(String, MeasurementType), MeasurementTime>;

fn keep_last_meas_time(
    msrmnt: &Measurement,
    last_meas_time_by_ean_type: &mut MeasurementTimeByEanType,
) -> Result<()> {
    match last_meas_time_by_ean_type.entry((msrmnt.ref_id.to_string(), msrmnt.unit)) {
        Vacant(entry) => {
            entry.insert(msrmnt.utc_time_stamp);
            Ok(())
        },
        Occupied(mut entry) => {
            let prev_msmrnt_dt_utc = entry.get();
            if msrmnt.utc_time_stamp >= *prev_msmrnt_dt_utc {
                // msrmnt times can be equal when encrypted and non encrypted values are present in same msrmnt_msg
                entry.insert(msrmnt.utc_time_stamp);
                Ok(())
            } else {
                Err(anyhow!(
                    "ignored message with measurement {msrmnt:?} before previous one at {prev_msmrnt_dt_utc:?}"
                ))
            }
        },
    }
}

type MeasurementByEanType = HashMap<(String, MeasurementType), Measurement>;

fn collect_msrmnts_msg_for_interval(
    msrmnts_msg: &MeasurementsMessage,
    last_meas_time_by_ean_type: &mut MeasurementTimeByEanType,
    interval_msrmnt_by_ean_type: &mut MeasurementByEanType,
    interval_msrmnt_enc_by_ean_type: &mut MeasurementByEanType,
    ndkr_msrmnts_vec: &mut Vec<Measurement>,
    show_msrmnt_dots: bool,
) -> Result<()> {
    // Filter incoming measurements in msrmnts_msg:
    // ignore the message when it is before a previous one:

    let mut msmrnts_ok: bool = true;
    for msrmnt in &msrmnts_msg.msrmnts_non_enc {
        if let Err(e) = keep_last_meas_time(msrmnt, last_meas_time_by_ean_type) {
            error!("{e:#} for unencrypted msrmnt");
            msmrnts_ok = false;
        }
    }
    for msrmnt in &msrmnts_msg.msrmnts_enc {
        if let Err(e) = keep_last_meas_time(msrmnt, last_meas_time_by_ean_type) {
            error!("{e:#} for encrypted msrmnt");
            msmrnts_ok = false;
        }
    }
    if !msmrnts_ok {
        return Err(anyhow!("at least one late measurement"));
    }

    for msrmnt in &msrmnts_msg.msrmnts_non_enc {
        match interval_msrmnt_by_ean_type.entry((msrmnt.ref_id.clone(), msrmnt.unit)) {
            Vacant(entry) => {
                entry.insert(msrmnt.clone());
                if show_msrmnt_dots {
                    print!("."); // accept msrmnt
                }
            },
            Occupied(mut entry) => {
                *entry.get_mut() = msrmnt.clone();
                if show_msrmnt_dots {
                    print!("+"); // override earlier msrmnt
                }
            },
        }
    }
    for msrmnt in &msrmnts_msg.msrmnts_enc {
        match interval_msrmnt_enc_by_ean_type.entry((msrmnt.ref_id.clone(), msrmnt.unit)) {
            Vacant(entry) => {
                entry.insert(msrmnt.clone());
                if show_msrmnt_dots {
                    print!(","); // accept encrypted msrmnt
                }
            },
            Occupied(mut entry) => {
                *entry.get_mut() = msrmnt.clone();
                if show_msrmnt_dots {
                    print!("*"); // override earlier encrypted msrmnt
                }
            },
        };
        // key adder will override earlier decryption requests for earlier msrmnts:
        ndkr_msrmnts_vec.push(msrmnt.clone());
    }
    // flush to show dots/plusses without a newline at the end:
    if io::stdout().flush().is_err() {
        error!("stdout flush failed");
    }
    Ok(())
}

fn report_eans(aggr_res: &[AggregatedMeasurement]) {
    let eans_ordered = aggr_res
        .iter()
        .flat_map(|am| am.original_eans.iter())
        .map(String::as_str)
        .collect::<BTreeSet<&str>>();
    info!(
        "{} network eans: {}",
        eans_ordered.len(),
        Vec::from_iter(eans_ordered).join(" ")
    );
}

fn unencrypted_aggregation(
    interval_msrmnt_by_ean_type: &MeasurementByEanType,
    now_dt: DateTime<Utc>,
    aggregator_id: &str,
) -> Vec<AggregatedMeasurement> {
    let mut res = Vec::new();
    for msrmnt_type in MeasurementType::iter() {
        let mut aggr_measurement = Measurement {
            ref_id: aggregator_id.to_owned(),
            utc_time_stamp: now_dt,
            unit: *msrmnt_type,
            value: MeasuredValue::zero(),
        };
        let mut num_msrmnts = 0;
        let mut total_delay_ms = 0;
        let mut original_eans = HashSet::new();

        for msrmnt in interval_msrmnt_by_ean_type.values() {
            if msrmnt.unit == *msrmnt_type {
                num_msrmnts += 1;
                total_delay_ms += now_dt
                    .signed_duration_since(msrmnt.utc_time_stamp)
                    .num_milliseconds();
                aggr_measurement.value += msrmnt.value.clone();
                original_eans.insert(msrmnt.ref_id.clone());
            }
        }

        if num_msrmnts > 0 {
            res.push(AggregatedMeasurement {
                aggr_measurement,
                total_delay_ms,
                original_eans,
            });
        }
    }
    res
}

fn decrypted_aggregation(
    interval_msrmnts_enc_by_ean_type: &MeasurementByEanType,
    key_add_reply: &EncKeyAddReply,
    max_abs_decrypted_per_participant: &BigIntPowTen,
    aggregator_id: &str,
) -> Vec<AggregatedMeasurement> {
    let mut res = Vec::new();
    let now_dt = time_stamp_milli_secs();
    for mt_total_key in &key_add_reply.msrmnt_type_total_key_vec {
        match &mt_total_key.total_encryption_key {
            EncryptionResult::PrivacyViolation => {
                info!(
                    "privacy violation, no decryption possible for {} measurement(s)",
                    mt_total_key.msrmnt_type.to_str()
                );
                continue;
            },
            EncryptionResult::TotalEncryptionKey(decryption_key) => {
                let mut refused_grid_conn_eans = HashSet::<String>::new();
                for rf_gc_ean in &mt_total_key.refused_grid_conn_ean_vec {
                    if let Ok(rf_gc_ean_str) = String::from_utf8(rf_gc_ean.clone()) {
                        if refused_grid_conn_eans.insert(rf_gc_ean_str.clone()) {
                            info!(
                                "refused grid connection ean {rf_gc_ean_str} for {}",
                                mt_total_key.msrmnt_type.to_str()
                            );
                        } else {
                            error!("grid connection ean {rf_gc_ean_str} refused more than once");
                        }
                    } else {
                        error!("refused grid connection ean non utf8: {rf_gc_ean:?}");
                    }
                }
                let msrmnts_enc: Vec<&Measurement> = interval_msrmnts_enc_by_ean_type
                    .values()
                    .filter(|&msrmnt| msrmnt.unit == mt_total_key.msrmnt_type)
                    .collect();
                if msrmnts_enc.is_empty() {
                    error!(
                        "no encrypted measurements aggregated for msrmnt_type {}",
                        mt_total_key.msrmnt_type.to_str()
                    );
                    continue; // next mt_total_key
                }
                match MeasuredValue::from_str(decryption_key) {
                    Err(e) => {
                        error!("{e:#} at from_str() for total_encryption_key");
                        continue; // next mt_total_key
                    },
                    Ok(total_encryption_val) => {
                        // take the sum over the encrypted values, except for refused_grid_conn_eans
                        let mut decr_msrmnts: usize = 0;
                        let mut total_delay_ms: i64 = 0;
                        let total_encrypted_val: MeasuredValue = msrmnts_enc
                            .iter()
                            .filter_map(|msrmnt| {
                                if refused_grid_conn_eans.contains(&msrmnt.ref_id) {
                                    None // refused equipment id, filter out.
                                } else {
                                    decr_msrmnts += 1;
                                    let msrmnt_delay_ms = now_dt
                                        .signed_duration_since(msrmnt.utc_time_stamp)
                                        .num_milliseconds();
                                    total_delay_ms += msrmnt_delay_ms;
                                    Some(&msrmnt.value)
                                }
                            })
                            .sum();
                        if decr_msrmnts == 0 {
                            error!(
                            "only ignored grid connection eans or aggregator ids for {} {:?} measurements",
                            msrmnts_enc.len(),
                            mt_total_key.msrmnt_type
                        );
                            continue;
                        }

                        let total_decrypted_val = &total_encrypted_val - &total_encryption_val;
                        let max_abs_decrypted = max_abs_decrypted_per_participant
                            * &BigIntPowTen::from(decr_msrmnts as i32);
                        if total_decrypted_val.abs() > max_abs_decrypted {
                            report_avarage(
                                log::Level::Error,
                                "decrypted out of range ",
                                decr_msrmnts,
                                total_delay_ms,
                                total_decrypted_val,
                                mt_total_key.msrmnt_type,
                            );
                            continue;
                        }
                        let original_eans = msrmnts_enc
                            .iter()
                            .filter_map(|msrmnt| {
                                if refused_grid_conn_eans.contains(&msrmnt.ref_id) {
                                    None
                                } else {
                                    Some(msrmnt.ref_id.clone())
                                }
                            })
                            .collect::<HashSet<_>>();

                        res.push(AggregatedMeasurement {
                            aggr_measurement: Measurement {
                                ref_id: aggregator_id.to_owned(),
                                utc_time_stamp: now_dt,
                                unit: mt_total_key.msrmnt_type,
                                value: total_decrypted_val,
                            },
                            total_delay_ms,
                            original_eans,
                        });
                    },
                }
            },
        };
    }

    let mut msrmnt_enc_types: HashSet<MeasurementType> = HashSet::new();
    for (_, msrmnt_type) in interval_msrmnts_enc_by_ean_type.keys() {
        msrmnt_enc_types.insert(*msrmnt_type);
    }
    if msrmnt_enc_types.len() != key_add_reply.msrmnt_type_total_key_vec.len() {
        warn!(
            "decrypted {} of {} aggregated encrypted measurement types",
            key_add_reply.msrmnt_type_total_key_vec.len(),
            msrmnt_enc_types.len(),
        );
    }
    res
}

fn report_avarage(
    log_level: log::Level,
    enc_type: &str,
    num_msrmnts: usize,
    total_delay_ms: i64,
    total_msrmnt_val: MeasuredValue,
    msrmnt_type: MeasurementType,
) {
    let av_delay_ms = total_delay_ms / (num_msrmnts as i64);
    let decimal_digits_for_av = 16;
    let Some(av_msrmnt_val) = total_msrmnt_val.divide(num_msrmnts, decimal_digits_for_av) else {
        error!("report_avarages: no measurements for {msrmnt_type:?}");
        return;
    };

    log!(
        log_level,
        "{num_msrmnts} {enc_type} measurement(s), av. delay: {:>7} ms, av.: {:>12} {}",
        av_delay_ms.to_string(),
        av_msrmnt_val.round(-5).to_string(5), // round to 10**-5, show 5 digits shown after decimal dot
        msrmnt_type.to_str(),
    );
}

fn receive_signed_key_add_reply(
    key_adder_tcp_stream: &mut TcpStream,
    read_timeout_seconds: u64,
    keyadder_sig_verifier: &OpenSslSigVerifier,
) -> Result<EncKeyAddReply> {
    let reply_buf = match receive_version_size_message(key_adder_tcp_stream, read_timeout_seconds) {
        Ok(buf) => buf,
        Err(e) => {
            return Err(e.context("receive_version_size_message reply_buf"));
        },
    };
    let signature = match receive_version_size_message(key_adder_tcp_stream, read_timeout_seconds) {
        Ok(buf) => buf,
        Err(e) => {
            return Err(e.context("receive_version_size_message signature"));
        },
    };
    // verify_signature before deserialize: strongest check first.
    match keyadder_sig_verifier.verify_signature(&reply_buf, &signature) {
        Err(e) => Err(e.context("verify_signature")),
        Ok(sig_ok) => {
            if sig_ok {
                match get_bincode_options().deserialize::<EncKeyAddReply>(&reply_buf) {
                    Err(e) => Err(Error::new(e).context("deserialize")),
                    Ok(key_add_reply) => Ok(key_add_reply),
                }
            } else {
                Err(anyhow!("verify_signature failed"))
            }
        },
    }
}

pub(crate) fn aggr_msrmnts_from_queue(
    config: &PrivAggrConfig,
    receiver_msrmnt_msgs: &mpsc::Receiver<AggregationMessage>,
    key_adder_socket_addr: &SocketAddr,
    keyadder_sig_verifier: &OpenSslSigVerifier,
    run_state: &RunState,
    last_aggr_result: &Mutex<Vec<AggregatedMeasurement>>,
) -> Result<()> {
    let ten_pow_max_decrypt = config.ten_pow_max_decrypt;
    let max_abs_decrypted = BigIntPowTen::new(BigInt::from(1), i32::from(ten_pow_max_decrypt));
    let read_timeout = config.key_adder_receive_timeout.std_duration();
    let msrmnts_partial_request = config.msrmnts_partial_request;
    let max_keep_eq_ids = config.max_keep_eq_ids;

    let mut last_meas_time_by_ean_type = HashMap::new(); // to ignore later arrived, but earlier measurements messages
    let mut interval_msrmnt_non_enc_by_ean_type = HashMap::new(); // collect non encrypted msmrnts per interval
    let mut interval_msrmnt_enc_by_ean_type = HashMap::new(); // collect encrypted msrmnts per interval
    let mut ndkr_msrmnts_vec = Vec::new(); // encrypted msmrmnts for which no decryption key request has been sent.

    // when a TcpStream goes out of scope there is an automatic shutdown(Shutdown::Both)
    let mut key_adder_tcp_stream: Option<TcpStream> =
        match tcp_stream_in_wg_intf(key_adder_socket_addr, read_timeout.as_secs()) {
            Ok(tcp_stream) => Some(tcp_stream),
            Err(e) => {
                error!("{e:#} first tcp_stream_in_wg_intf for key_adder_tcp_stream");
                None
            },
        };

    let bincode_options = get_bincode_options();

    info!("start collecting measurement messages");
    let mut msrmnts_msgs_received: usize = 0;
    let mut total_msrmnts_received: usize = 0;

    let show_msrmnt_dots = match std::env::var("SILENT_MSRMNTS") {
        Err(_) => true,
        Ok(silent_msrmnts) => silent_msrmnts == "0",
    };

    loop {
        let aggregation_message = match receiver_msrmnt_msgs.recv() {
            Err(e) => {
                return Err(Error::new(e));
            },
            Ok(m) => m,
        };
        run_state.is_running()?;
        match aggregation_message {
            AggregationMessage::Measurements(msrmnts_msg) => {
                msrmnts_msgs_received += 1;
                total_msrmnts_received += msrmnts_msg.total_num_msrmnts();
                if let Err(e) = collect_msrmnts_msg_for_interval(
                    &msrmnts_msg,
                    &mut last_meas_time_by_ean_type,
                    &mut interval_msrmnt_non_enc_by_ean_type,
                    &mut interval_msrmnt_enc_by_ean_type,
                    &mut ndkr_msrmnts_vec,
                    show_msrmnt_dots,
                ) {
                    error!("{e:#} at collect_msmrnts_msg_for_interval");
                    continue;
                }
                if ndkr_msrmnts_vec.len() >= (msrmnts_partial_request as usize) {
                    match aggr_key_add_req_from_mrsmnts(
                        &mut ndkr_msrmnts_vec,
                        IncrementalRequest::Partial,
                    ) {
                        Ok(Some(key_add_request)) => {
                            if let Some(ref mut tcp_stream) = key_adder_tcp_stream {
                                match bincode_options
                                    .serialize::<EncKeyAddRequest>(&key_add_request)
                                {
                                    Err(e) => {
                                        error!("{e:?} at serialize partial request");
                                    },
                                    Ok(ref request_bytes) => {
                                        if let Err(e) =
                                            send_version_size_message(request_bytes, tcp_stream)
                                        {
                                            error!(
                                                    "{e:#} at send_version_size_message partial request"
                                                );
                                        }
                                    },
                                }
                            }
                        },
                        Ok(None) => {
                            warn!(
                                "no partial partial key add request for ndkr_msrmnts_vec.len() {}",
                                ndkr_msrmnts_vec.len()
                            );
                        },
                        Err(e) => return Err(e.context("aggr_key_add_req_from_mrsmnts")),
                    }
                    ndkr_msrmnts_vec.clear();
                }
            },
            AggregationMessage::EndInterval => {
                if show_msrmnt_dots {
                    println!(); // end the ...+++ or ,,,*** line for the measurements
                }
                let aggreg_dt = time_stamp_milli_secs();
                if interval_msrmnt_non_enc_by_ean_type.is_empty()
                    && interval_msrmnt_enc_by_ean_type.is_empty()
                {
                    debug!(
                        "no messages available at {}",
                        time_stamp_to_string(&aggreg_dt)
                    );
                } else {
                    // send final key add request:
                    match aggr_key_add_req_from_mrsmnts(
                        &mut ndkr_msrmnts_vec,
                        IncrementalRequest::Final,
                    )
                    .context("aggr_key_add_req_from_mrsmnts")?
                    {
                        Some(key_add_request) => {
                            if let Some(ref mut tcp_stream) = key_adder_tcp_stream {
                                match bincode_options
                                    .serialize::<EncKeyAddRequest>(&key_add_request)
                                {
                                    Err(e) => {
                                        error!("{e:?} at serialize final request");
                                    },
                                    Ok(ref request_bytes) => {
                                        if let Err(e) =
                                            send_version_size_message(request_bytes, tcp_stream)
                                        {
                                            error!(
                                                "{e:#} at send_version_size_message final request"
                                            );
                                        }
                                    },
                                }
                            }
                        },
                        None => {
                            warn!(
                                "no final partial key add request for ndkr_msrmnts_vec.len() {}",
                                ndkr_msrmnts_vec.len()
                            );
                        },
                    }
                    ndkr_msrmnts_vec.clear();

                    info!("collected {msrmnts_msgs_received} measurement messages, with {total_msrmnts_received} measurements");

                    // compute and report interval aggregation results.

                    let start_report_dt = time_stamp_milli_secs();
                    let aggr_result_unencrypted_opt: Option<_> =
                        if interval_msrmnt_non_enc_by_ean_type.is_empty() {
                            None
                        } else {
                            let res = unencrypted_aggregation(
                                &interval_msrmnt_non_enc_by_ean_type,
                                start_report_dt,
                                &config.aggregator_id,
                            );
                            for am in &res {
                                report_avarage(
                                    log::Level::Info,
                                    "unencrypted ",
                                    am.original_eans.len(),
                                    am.total_delay_ms,
                                    am.aggr_measurement.value.clone(),
                                    am.aggr_measurement.unit,
                                );
                            }
                            report_eans(&res);
                            Some(res)
                        };

                    let aggr_result_decrypted_opt: Option<_> =
                        if interval_msrmnt_enc_by_ean_type.is_empty() {
                            trace!("no encrypted measurements available");
                            None
                        } else if let Some(ref mut tcp_stream) = key_adder_tcp_stream {
                            match receive_signed_key_add_reply(
                                tcp_stream,
                                read_timeout.as_secs(),
                                keyadder_sig_verifier,
                            ) {
                                Err(e) => {
                                    error!("{e:#} at receive_signed_key_add_reply");
                                    None
                                },
                                Ok(key_add_reply) => {
                                    let decrypt_delay_ms = time_stamp_milli_secs()
                                        .signed_duration_since(start_report_dt)
                                        .num_milliseconds();
                                    info!("decryption delay: {decrypt_delay_ms} ms");

                                    if !key_add_reply.grid_conn_eans_no_encryption_info.is_empty() {
                                        let ean_string_iter = key_add_reply
                                            .grid_conn_eans_no_encryption_info
                                            .iter()
                                            .map(|ean| slice_u8_to_string(ean.as_slice()));
                                        warn!(
                                            "no decryption info for {} network ean(s): {}",
                                            key_add_reply.grid_conn_eans_no_encryption_info.len(),
                                            ean_string_iter.collect::<Vec<_>>().join(" ")
                                        );
                                    }

                                    let res = decrypted_aggregation(
                                        &interval_msrmnt_enc_by_ean_type,
                                        &key_add_reply,
                                        &max_abs_decrypted,
                                        &config.aggregator_id,
                                    );
                                    for am in &res {
                                        report_avarage(
                                            log::Level::Info,
                                            "  decrypted  ",
                                            am.original_eans.len(),
                                            am.total_delay_ms,
                                            am.aggr_measurement.value.clone(),
                                            am.aggr_measurement.unit,
                                        );
                                    }
                                    report_eans(&res);
                                    Some(res)
                                },
                            }
                        } else {
                            error!("no key_adder_tcp_stream for encrypted measurements");
                            None
                        };

                    let mut total_aggr_result: Vec<AggregatedMeasurement> = Vec::new();
                    match (aggr_result_unencrypted_opt, aggr_result_decrypted_opt) {
                        (Some(aggr_result_unencrypted), Some(aggr_result_decrypted)) => {
                            // merge by measurement type, check for equal results when equal original eans's
                            for mt in MeasurementType::iter() {
                                let mut aggr_result_unencrypted_mt_opt = None;
                                for aggr_msrmnt in &aggr_result_unencrypted {
                                    if aggr_msrmnt.aggr_measurement.unit == *mt
                                        && aggr_result_unencrypted_mt_opt.is_none()
                                    {
                                        aggr_result_unencrypted_mt_opt = Some(aggr_msrmnt);
                                        break;
                                    }
                                }
                                let mut aggr_result_decrypted_mt_opt = None;
                                for aggr_msrmnt in &aggr_result_decrypted {
                                    if aggr_msrmnt.aggr_measurement.unit == *mt
                                        && aggr_result_decrypted_mt_opt.is_none()
                                    {
                                        aggr_result_decrypted_mt_opt = Some(aggr_msrmnt);
                                        break;
                                    }
                                }
                                if let (
                                    Some(aggr_result_unencrypted_mt),
                                    Some(aggr_result_decrypted_mt),
                                ) =
                                    (aggr_result_unencrypted_mt_opt, aggr_result_decrypted_mt_opt)
                                {
                                    if aggr_result_unencrypted_mt.original_eans.len()
                                        == aggr_result_decrypted_mt.original_eans.len()
                                    {
                                        let unenc_eans = aggr_result_unencrypted_mt
                                            .original_eans
                                            .iter()
                                            .collect::<HashSet<_>>();
                                        if aggr_result_decrypted_mt
                                            .original_eans
                                            .iter()
                                            .all(|ean| unenc_eans.contains(ean))
                                        {
                                            if aggr_result_unencrypted_mt.aggr_measurement.value
                                                == aggr_result_decrypted_mt.aggr_measurement.value
                                            {
                                                total_aggr_result
                                                    .push(aggr_result_unencrypted_mt.clone());
                                            } else {
                                                error!("{} unequal totals for decrypted and unencrypted", mt.to_str());
                                            }
                                        } else {
                                            // not the same eans, ignore the unencrypted aggregation
                                            total_aggr_result
                                                .push(aggr_result_decrypted_mt.clone());
                                        }
                                    }
                                }
                            }
                        },
                        (Some(aggr_result_unencrypted), None) => {
                            total_aggr_result.extend(aggr_result_unencrypted);
                        },
                        (None, Some(aggr_result_decrypted)) => {
                            total_aggr_result.extend(aggr_result_decrypted);
                        },
                        (None, None) => {},
                    }
                    match last_aggr_result.lock() {
                        Ok(mut res) => {
                            *res = total_aggr_result;
                        },
                        Err(e) => {
                            error!("{e:?} on last_aggr_result");
                        },
                    }

                    // prepare next aggregation interval
                    interval_msrmnt_non_enc_by_ean_type.clear();
                    interval_msrmnt_enc_by_ean_type.clear();
                }
                msrmnts_msgs_received = 0;
                total_msrmnts_received = 0;
                if last_meas_time_by_ean_type.len() > (max_keep_eq_ids as usize) {
                    // Possible attack with many different combinations?
                    error!(
                        "too many combinations of grid connection ean and measurement type: {}",
                        last_meas_time_by_ean_type.len()
                    );
                    // Must clear, now an old message may be accepted:
                    last_meas_time_by_ean_type.clear();
                }
                // create a new key_adder_tcp_stream for next aggregation interval
                key_adder_tcp_stream =
                    match tcp_stream_in_wg_intf(key_adder_socket_addr, read_timeout.as_secs()) {
                        Ok(tcp_stream) => Some(tcp_stream),
                        Err(e) => {
                            error!("{e:#} at later tcp_stream_in_wg_intf key_adder_tcp_stream");
                            None
                        },
                    };
            },
        }
    }
}
