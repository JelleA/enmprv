fn main() {
    println!("Hello, world!");
}

mod common_objects {
    use enmplib_rs::bigintpowten::BigIntPowTen;
    use enmplib_rs::bincoded::MeasurementType;
    use enmplib_rs::chrono::{DateTime, Duration, Utc};
    use enmplib_rs::num::{pow, BigInt};
    use enmplib_rs::util::{encryption_key, time_stamp_to_string};

    pub struct EncryptionConfig {
        seed: Vec<u8>,
        enc_digits: u32,
        dec_digits: u32,
    }

    impl EncryptionConfig {
        pub fn new() -> Self {
            EncryptionConfig {
                seed: "asdf".to_string().as_bytes().to_vec(),
                enc_digits: 70, // decimal digits from blake2s 256 bits: 10log(2**256) = 77.06.
                dec_digits: 3,
            }
        }

        pub fn encryption_key_for_interval(
            &self,
            interval_start: &DateTime<Utc>,
            ean: &[u8],
            msrmnt_type: MeasurementType,
        ) -> BigIntPowTen {
            self.encrypt_(ean, interval_start, msrmnt_type)
        }

        fn encrypt_(
            &self,
            ean: &[u8],
            time_stamp: &DateTime<Utc>,
            msrmnt_type: MeasurementType,
        ) -> BigIntPowTen {
            let enc_modulus = pow(BigInt::from(10), self.enc_digits as usize);
            BigIntPowTen::new(
                encryption_key(
                    &self.seed,
                    ean,
                    time_stamp_to_string(time_stamp).unwrap().as_bytes(),
                    msrmnt_type,
                    &enc_modulus,
                ),
                -(self.dec_digits as i32),
            )
        }
    }

    pub struct TimesIterator {
        pub start_time: DateTime<Utc>,
        pub interval_duration: Duration,
    }

    impl Iterator for TimesIterator {
        type Item = DateTime<Utc>;
        fn next(&mut self) -> Option<Self::Item> {
            let next_start = self.start_time;
            self.start_time = next_start + self.interval_duration;
            Some(next_start)
        }
    }

    // request from p1encryptor to aggregator to check the total price for series of consecutive intervals
    pub struct IntervalsPriceCheckRequest {
        // grid_connection_ean.as_bytes():
        pub grid_conn_ean: Vec<u8>,
        pub msrmnt_type: MeasurementType,
        // provided as time_stamp_to_string(a_dt_utc).as_bytes(), used directly for decryption key:
        pub start_time: DateTime<Utc>,
        pub interval_secs: u32,
        // interval unit prices, vector the vector length is also the number of intervals.
        // each price in decimal digits, with optional sign and decimal point,
        pub unit_prices: Vec<String>,
        pub total_price: String,
        pub encrypted_total_price: String,
    }

    pub type IntervalsPriceCheckReply = Result<(), String>;

    // request from aggregator to key adder for the encryption key of the price of consecutive intervals
    #[derive(Debug, Clone)]
    pub struct IntervalsPriceKeyRequest {
        // grid_connection_ean.as_bytes():
        pub grid_conn_ean: Vec<u8>,
        pub msrmnt_type: MeasurementType,
        // provided as time_stamp_to_string(a_dt_utc).as_bytes(), used directly for decryption key:
        pub start_time: Vec<u8>,
        // interval number of seconds minus 1, 25 bits needed for a year, typically quarter 900, hour 3600.
        pub interval_secs: u32,
        // interval unit prices, vector the vector length is also the number of intervals.
        // each price in decimal digits, with optional sign and decimal point,
        pub interval_unit_prices: Vec<String>,
    }

    // reply from key adder to aggrator for an IntervalsPriceKeyRequest:
    // CHECKME: needed for serde? #[derive(Serialize, Deserialize, Debug)]
    pub type IntervalsPriceKeyReply = Result<String, String>; // price decode key as decimal digits with optional sign and decimal point,
}

mod aggregator_intervals_prices {
    use super::common_objects::{
        IntervalsPriceCheckReply, IntervalsPriceCheckRequest, IntervalsPriceKeyReply,
        IntervalsPriceKeyRequest, TimesIterator,
    };
    use enmplib_rs::bigintpowten::BigIntPowTen;
    use enmplib_rs::util::time_stamp_to_string;
    use std::str::FromStr;
    use std::sync::mpsc;

    fn ipk_request_from_ipc_request(
        ipcreq: &IntervalsPriceCheckRequest,
    ) -> IntervalsPriceKeyRequest {
        IntervalsPriceKeyRequest {
            grid_conn_ean: ipcreq.grid_conn_ean.clone(),
            msrmnt_type: ipcreq.msrmnt_type,
            start_time: time_stamp_to_string(&ipcreq.start_time)
                .unwrap()
                .as_bytes()
                .to_vec(),
            interval_secs: ipcreq.interval_secs,
            interval_unit_prices: ipcreq.unit_prices.clone(),
        }
    }

    fn ipc_reply_from_ipk_reply(
        ipcreq: &IntervalsPriceCheckRequest,
        ipkrep: &IntervalsPriceKeyReply,
    ) -> IntervalsPriceCheckReply {
        match ipkrep {
            Ok(ipkreply_str) => {
                let decrypted_total_price = BigIntPowTen::from_str(&ipcreq.encrypted_total_price)
                    .unwrap()
                    - BigIntPowTen::from_str(&ipkreply_str).unwrap();
                if BigIntPowTen::from_str(&ipcreq.total_price).unwrap() == decrypted_total_price {
                    Ok(())
                } else {
                    Err(format!(
                        "unequal {} and {}",
                        ipcreq.total_price,
                        decrypted_total_price.to_string(3)
                    ))
                }
            }
            Err(mes) => Err(format!("ipkrep {mes}")),
        }
    }

    pub fn serve_intervals_price_check_requests(
        ip_chk_req_rx: &mpsc::Receiver<Option<IntervalsPriceCheckRequest>>,
        ip_chk_rep_tx: &mpsc::SyncSender<Option<IntervalsPriceCheckReply>>,
        ip_key_req_tx: &mpsc::SyncSender<Option<IntervalsPriceKeyRequest>>,
        ip_key_rep_rx: &mpsc::Receiver<Option<IntervalsPriceKeyReply>>,
    ) -> Result<(), String> {
        loop {
            let ipcreq = match ip_chk_req_rx.recv() {
                Err(e) => {
                    return Err(format!(
                        "{e:?} at: ip_chk_req_rx.recv(), queue no longer available"
                    ));
                }
                Ok(ipcreq_opt) => match ipcreq_opt {
                    None => return Err("None from ip_chk_req_rx".to_string()),
                    Some(ipcreq) => ipcreq,
                },
            };
            let ipkreq = ipk_request_from_ipc_request(&ipcreq);

            if let Err(queue_send_err) = ip_key_req_tx.try_send(Some(ipkreq.clone())) {
                return Err(match queue_send_err {
                    mpsc::TrySendError::Full(_full_error) => {
                        "ip_key_rep_tx queue full_error, ignored ipcrep"
                    }
                    mpsc::TrySendError::Disconnected(_option_iprep) => {
                        "ip_key_rep_tx queue disconnected"
                    }
                }
                .to_string());
            }

            let ipkrep: IntervalsPriceKeyReply = match ip_key_rep_rx.recv() {
                Err(e) => {
                    return Err(format!(
                        "{e:?} at: ip_chk_req_rx.recv(), queue no longer available"
                    ));
                }
                Ok(ipkreq_opt) => match ipkreq_opt {
                    None => return Err("None from ip_chk_req_rx".to_string()),
                    Some(ipkreq) => ipkreq,
                },
            };

            let ipcrep = ipc_reply_from_ipk_reply(&ipcreq, &ipkrep);

            if let Err(queue_send_err) = ip_key_req_tx.try_send(Some(ipkreq)) {
                return Err(match queue_send_err {
                    mpsc::TrySendError::Full(_full_error) => {
                        "ip_key_rep_tx queue full_error, ignored ip_key_reply"
                    }
                    mpsc::TrySendError::Disconnected(_option_iprep) => {
                        "ip_key_rep_tx queue disconnected"
                    }
                }
                .to_string());
            }
        }
    }
}

mod key_adder_intervals_prices {
    use enmplib_rs::bigintpowten::BigIntPowTen;
    use enmplib_rs::chrono::Duration;
    use enmplib_rs::util::time_stamp_from_str;
    use std::iter::zip;
    use std::str::FromStr;
    use std::sync::mpsc;

    use super::common_objects::{
        EncryptionConfig, IntervalsPriceKeyReply, IntervalsPriceKeyRequest, TimesIterator,
    };

    fn ipk_reply_from_request(
        ipk_req: &IntervalsPriceKeyRequest,
        encryption_config: &EncryptionConfig,
    ) -> IntervalsPriceKeyReply {
        dbg!(&String::from_utf8(ipk_req.start_time.clone()).unwrap());
        let encryption_key: BigIntPowTen = zip(
            TimesIterator {
                start_time: time_stamp_from_str(
                    &String::from_utf8(ipk_req.start_time.clone()).unwrap(),
                )
                .unwrap(),
                interval_duration: Duration::seconds(ipk_req.interval_secs.into()),
            },
            &ipk_req.interval_unit_prices,
        )
        .map(|(start, price)| {
            BigIntPowTen::from_str(price).unwrap()
                * encryption_config.encryption_key_for_interval(
                    &start,
                    &ipk_req.grid_conn_ean,
                    ipk_req.msrmnt_type,
                )
        })
        .sum();
        Ok(encryption_key.to_string(3))
    }

    pub fn serve_intervals_price_key_requests(
        ip_key_req_rx: &mpsc::Receiver<Option<IntervalsPriceKeyRequest>>,
        ip_key_rep_tx: &mpsc::SyncSender<Option<IntervalsPriceKeyReply>>,
    ) -> Result<(), String> {
        let encryption_config = EncryptionConfig::new(); // should depend on grid connection ean in request.
        loop {
            let ipkreq = match ip_key_req_rx.recv() {
                Err(e) => {
                    return Err(format!("{e:?} at: recv, queue no longer available"));
                }
                Ok(ipreq_opt) => match ipreq_opt {
                    None => return Err("None from from_encrypt_chan".to_string()),
                    Some(ipkreq) => ipkreq,
                },
            };
            let ipkrep = ipk_reply_from_request(&ipkreq, &encryption_config);
            if let Err(queue_send_err) = ip_key_rep_tx.try_send(Some(ipkrep)) {
                return Err(match queue_send_err {
                    mpsc::TrySendError::Full(_full_error) => {
                        "iprep_tx queue full_error, ignored iprep"
                    }
                    mpsc::TrySendError::Disconnected(_option_iprep) => {
                        "iprep_tx queue disconnected"
                    }
                }
                .to_string());
            }
        }
    }
}

#[cfg(test)]
mod tests {

    use std::iter::zip;
    use std::str::FromStr;
    use std::sync::mpsc;
    use std::thread;

    use enmplib_rs::bigintpowten::{BigIntPowTen, One};
    use enmplib_rs::bincoded::MeasurementType;
    use enmplib_rs::chrono::{DateTime, Duration, NaiveDate, Utc};
    use enmplib_rs::util::time_stamp_to_string;

    use super::common_objects::{
        EncryptionConfig, IntervalsPriceCheckReply, IntervalsPriceCheckRequest,
        IntervalsPriceKeyReply, IntervalsPriceKeyRequest, TimesIterator,
    };

    struct IntervalsPriceEncryptor {
        grid_conn_ean: String,
        msrmnt_type: MeasurementType,
        start_time: DateTime<Utc>,
        interval_duration: Duration,
        unit_prices_amounts: Vec<(BigIntPowTen, BigIntPowTen)>,
    }

    impl IntervalsPriceEncryptor {
        // price in unencrypted form, computed at price payer:
        fn total_price(&self) -> BigIntPowTen {
            self.unit_prices_amounts
                .iter()
                .map(|(unit_price, amount)| unit_price * amount)
                .sum()
        }

        fn start_times(&self) -> TimesIterator {
            TimesIterator {
                start_time: self.start_time,
                interval_duration: self.interval_duration,
            }
        }

        // price encryption key for consecutive intervals, computed at p1encryptor and at key adder
        fn total_price_encryption_key(&self, encryption_config: &EncryptionConfig) -> BigIntPowTen {
            zip(self.start_times(), &self.unit_prices_amounts)
                .map(|(start, (unit_price, _amount))| {
                    unit_price
                        * &encryption_config.encryption_key_for_interval(
                            &start,
                            &self.grid_conn_ean.as_bytes(),
                            self.msrmnt_type,
                        )
                })
                .sum()
        }
    }

    fn test_encryption(unit_prices_amounts: &[(BigIntPowTen, BigIntPowTen)]) {
        // at price payer:
        let start_time =
            DateTime::<Utc>::from_utc(NaiveDate::from_ymd(2022, 3, 10).and_hms(0, 0, 0), Utc);
        let ip_encryptor = IntervalsPriceEncryptor {
            grid_conn_ean: "4321".to_string(),
            msrmnt_type: MeasurementType::El_kWh,
            start_time,
            interval_duration: Duration::hours(1),
            unit_prices_amounts: unit_prices_amounts.to_vec(),
        };
        let total_price = ip_encryptor.total_price();
        // keep secret from aggregator and key adder:

        let encryption_config = EncryptionConfig::new();

        let price_enc_key = ip_encryptor.total_price_encryption_key(&encryption_config);
        let encrypted_price = total_price.clone() + price_enc_key;
        // pass encrypted price to aggregator:

        // pass unit prices and intervals via aggregator to key adder, compute encryption keys at key adder:
        let mut unit_prices_keys = Vec::new();
        for (start, (p, _a)) in zip(ip_encryptor.start_times(), unit_prices_amounts) {
            unit_prices_keys.push((
                p.clone(),
                encryption_config.encryption_key_for_interval(
                    &start,
                    &ip_encryptor.grid_conn_ean.as_bytes(),
                    ip_encryptor.msrmnt_type,
                ),
            ));
        }
        let ip_decryptor = IntervalsPriceEncryptor {
            grid_conn_ean: "4321".to_string(),
            msrmnt_type: MeasurementType::El_kWh,
            start_time,
            interval_duration: Duration::hours(1),
            unit_prices_amounts: unit_prices_keys.to_vec(),
        };

        let max_queue_size = 2;
        let (ip_key_req_tx, ip_key_req_rx) =
            mpsc::sync_channel::<Option<IntervalsPriceKeyRequest>>(max_queue_size);
        let (ip_key_rep_tx, ip_key_rep_rx) =
            mpsc::sync_channel::<Option<IntervalsPriceKeyReply>>(max_queue_size);
        let ipk_thread = thread::spawn(move || {
            if let Err(mes) = super::key_adder_intervals_prices::serve_intervals_price_key_requests(
                &ip_key_req_rx,
                &ip_key_rep_tx,
            ) {
                println!("serve_intervals_price_key_requests error: {mes}");
            }
        });
        // aggregator requests decryption from key adder:
        let price_dec_key = ip_decryptor.total_price();
        let decrypted_price = encrypted_price - price_dec_key;
        assert_eq!(total_price, decrypted_price);
    }

    #[test]
    fn encrypt_single_interval() {
        let unit_prices_amounts = Vec::from_iter([(BigIntPowTen::one(), BigIntPowTen::one())]);
        test_encryption(&unit_prices_amounts);
    }

    #[test]
    fn encrypt_two_intervals() {
        let unit_prices_amounts = Vec::from_iter([
            (BigIntPowTen::from(1), BigIntPowTen::from(1)),
            (BigIntPowTen::from(2), BigIntPowTen::from(3)),
        ]);
        test_encryption(&unit_prices_amounts);
    }

    #[test]
    fn encrypt_many_intervals() {
        let num_intervals = 1000;
        let mut unit_prices_amounts = Vec::new();
        for i in 0..num_intervals {
            unit_prices_amounts
                .push((BigIntPowTen::from(i), BigIntPowTen::from(num_intervals - i)));
        }
        test_encryption(&unit_prices_amounts);
    }

    #[test]
    fn test_encryption_on_threads() {
        let grid_conn_ean = "987665".to_string();
        let msrmnt_type = MeasurementType::El_kWh;
        let start_time =
            DateTime::<Utc>::from_utc(NaiveDate::from_ymd(2022, 3, 10).and_hms(0, 0, 0), Utc);
        let interval_secs = 3600;
        let unit_prices_amounts: [(BigIntPowTen, BigIntPowTen); 2] = [
            (BigIntPowTen::from(2), BigIntPowTen::from(3)),
            (BigIntPowTen::from(5), BigIntPowTen::from(7)),
        ];

        // at p1 encryptor:
        let total_price: BigIntPowTen = unit_prices_amounts.iter().map(|(p, a)| p * a).sum();
        dbg!(&total_price.to_string(3));

        let encryption_config = EncryptionConfig::new();

        // pass from p1 encryptor to aggregator:
        let encrypted_total_price: BigIntPowTen = zip(
            TimesIterator {
                start_time,
                interval_duration: Duration::seconds(interval_secs.into()),
            },
            &unit_prices_amounts,
        )
        .map(|(start, (price, amount))| {
            (amount
                + &encryption_config.encryption_key_for_interval(
                    &start,
                    grid_conn_ean.as_bytes(),
                    msrmnt_type,
                ))
                * price.clone()
        })
        .sum();

        let max_queue_size = 2;
        let (ip_chk_req_tx, ip_chk_req_rx) =
            mpsc::sync_channel::<Option<IntervalsPriceCheckRequest>>(max_queue_size);
        let (ip_chk_rep_tx, ip_chk_rep_rx) =
            mpsc::sync_channel::<Option<IntervalsPriceCheckReply>>(max_queue_size);
        let (ip_key_req_tx, ip_key_req_rx) =
            mpsc::sync_channel::<Option<IntervalsPriceKeyRequest>>(max_queue_size);
        let (ip_key_rep_tx, ip_key_rep_rx) =
            mpsc::sync_channel::<Option<IntervalsPriceKeyReply>>(max_queue_size);

        let ipk_thread = thread::spawn(move || {
            if let Err(mes) = super::key_adder_intervals_prices::serve_intervals_price_key_requests(
                &ip_key_req_rx,
                &ip_key_rep_tx,
            ) {
                println!("serve_intervals_price_key_requests error: {mes}");
            }
        });

        let ipc_thread = thread::spawn(move || {
            if let Err(mes) =
                super::aggregator_intervals_prices::serve_intervals_price_check_requests(
                    &ip_chk_req_rx,
                    &ip_chk_rep_tx,
                    &ip_key_req_tx,
                    &ip_key_rep_rx,
                )
            {
                println!("serve_intervals_price_key_requests error: {mes}");
            }
        });

        // request from p1encryptor to aggregator to verify the total price over the intervals:
        let ip_chk_req = super::common_objects::IntervalsPriceCheckRequest {
            grid_conn_ean: grid_conn_ean.as_bytes().to_vec(),
            msrmnt_type,
            total_price: total_price.to_string(3),
            encrypted_total_price: encrypted_total_price.to_string(3),
            start_time,
            interval_secs,
            unit_prices: unit_prices_amounts
                .iter()
                .map(|(p, _)| p.to_string(3))
                .collect(),
        };

        ip_chk_req_tx.try_send(Some(ip_chk_req));
        ip_chk_req_tx.try_send(None);

        let ipcreply = ip_chk_rep_rx.recv();

        assert!(ipcreply.is_ok());

        ipk_thread.join();
        ipc_thread.join();
    }
}
