#! /bin/bash -ev
set -u
# Run the tlsclient for development testing
cd ~/gitrepos/enmprv/pricenc-tls
#certfile=~/keys/certs/ca_root_self_cert.pem
certfile=~/keys/ca_root-public3072.pem
ls -l $certfile
hostname="v110.fritz.box"
port=2344
time cargo run --bin tlsclient -- --verbose --cafile $certfile $hostname --port $port << END
Hello world!
END
