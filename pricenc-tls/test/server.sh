#! /bin/bash -ev
set -u
# Run the tlsserver for development testing
cd ~/gitrepos/enmprv/pricenc-tls
certfile=~/keys/certs/ca_root_self_cert.pem
keyfile=~/keys/ca_root-private3072.pem 
mode="echo"
ls -l $certfile
ls -l $keyfile
time cargo run --bin tlsserver -- --verbose --certs $certfile --key $keyfile -p2344 $mode 
