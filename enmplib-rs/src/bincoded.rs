use std::collections::hash_map::DefaultHasher;
use std::convert::TryFrom;
use std::fmt::{self, Display};
use std::hash::{Hash, Hasher};

use anyhow::{anyhow, Result};
use bincode::{
    config::{Bounded, VarintEncoding, WithOtherIntEncoding, WithOtherLimit},
    DefaultOptions, Options,
};
use serde::{Deserialize, Serialize};

use log::error;

const BIN_CODE_SIZE_LIMIT: u64 = 40000;

#[must_use]
pub fn get_bincode_options(
) -> WithOtherLimit<WithOtherIntEncoding<DefaultOptions, VarintEncoding>, Bounded> {
    DefaultOptions::new()
        .with_varint_encoding()
        .with_limit(BIN_CODE_SIZE_LIMIT)
}

#[derive(Serialize, Deserialize, Debug, Copy, Clone, PartialEq, Eq, Hash)]
#[allow(non_camel_case_types)]
pub enum MeasurementType {
    // Do not change the order (unless using MeasurementType only in a new db).
    El_kWh,    // no more used, but still present in db, split into _to and _by
    El_kW,     // no more used, but still present in db, split into _to and _by
    NgNl_m3,   // Natural Gas, NL, cumulative, 9.769 kWh per normalized m3.
    El_kWh_to, // cumulative, delivered to the meter from the grid
    El_kWh_by, // cumulative, delivered by meter to the grid
    El_kW_to,  // Electric, kW power to meter from the grid, non cumulative
    El_kW_by,  // Electric, kW power by meter to the grid, non cumulative
               // Only change MeasurementType by appending here
}

impl MeasurementType {
    const VALUES: [Self; 7] = [
        Self::El_kWh,
        Self::El_kW,
        Self::NgNl_m3,
        Self::El_kWh_to,
        Self::El_kWh_by,
        Self::El_kW_to,
        Self::El_kW_by,
    ];
    pub const VARIANT_COUNT: usize = Self::VALUES.len();

    pub fn iter() -> std::slice::Iter<'static, Self> {
        Self::VALUES.iter()
    }

    #[must_use]
    pub fn to_usize(self) -> usize {
        // See https://doc.rust-lang.org/reference/items/enumerations.html#custom-discriminant-values-for-field-less-enumerations
        // This is used to store and search in a database.
        self as usize
    }

    #[must_use]
    pub fn to_str(self) -> &'static str {
        match self {
            Self::El_kWh => "el.kWh",
            Self::El_kW => "el.kW",
            Self::NgNl_m3 => "ng.nl.m3",
            Self::El_kWh_to => "el.kWh.to",
            Self::El_kWh_by => "el.kWh.by",
            Self::El_kW_to => "el.kW.to",
            Self::El_kW_by => "el.kW.by",
        }
    }
}

impl TryFrom<&str> for MeasurementType {
    type Error = anyhow::Error;
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        for mt in Self::iter() {
            if value == mt.to_str() {
                return Ok(*mt);
            }
        }
        Err(anyhow!("unknown MeasurementType {value}"))
    }
}

const NUM_BYTES_MSRMNT_TYPE_BIT_VEC: usize =
    (MeasurementType::VARIANT_COUNT - 1) / (u8::BITS as usize) + 1;

#[derive(Serialize, Deserialize, Debug)]
pub struct MeasurementTypeBitVec([u8; NUM_BYTES_MSRMNT_TYPE_BIT_VEC]);

impl MeasurementTypeBitVec {
    #[must_use]
    pub fn new() -> Self {
        Self([0u8; NUM_BYTES_MSRMNT_TYPE_BIT_VEC])
    }

    #[must_use]
    pub fn any(&self) -> bool {
        self.0.iter().any(|b| *b != 0)
    }

    #[must_use]
    pub fn last_set_bit(&self) -> Option<usize> {
        // avoid combining rev() and enumerate() on self.0.iter()
        let mut last_bit_pos: i32 = self.0.len() as i32 * u8::BITS as i32 - 1;
        self.0.rchunks(1).flatten().find_map(|&b| {
            if b > 0 {
                Some(last_bit_pos as usize - b.leading_zeros() as usize)
            } else {
                last_bit_pos -= u8::BITS as i32; // may become negative
                None
            }
        })
    }

    pub fn set_bit(&mut self, msrmnt_type: MeasurementType, val: bool) -> Result<()> {
        let bit_num = msrmnt_type.to_usize();
        match self.0.get_mut(bit_num / (u8::BITS as usize)) {
            Some(byte) => {
                let mask = 1u8 << (bit_num % (u8::BITS as usize));
                if val {
                    *byte |= mask;
                } else {
                    *byte &= !mask;
                }
                Ok(())
            },
            None => Err(anyhow!("no byte available for {msrmnt_type:?} in {self:?}",)),
        }
    }

    pub fn get_bit(&self, msrmnt_type: MeasurementType) -> Result<bool> {
        let bit_num = msrmnt_type.to_usize();
        match self.0.get(bit_num / (u8::BITS as usize)) {
            Some(&b) => {
                let mask = 1u8 << (bit_num % (u8::BITS as usize));
                Ok(b & mask != 0u8)
            },
            None => Err(anyhow!("no byte available for {msrmnt_type:?} in {self:?}")),
        }
    }
}

impl Default for MeasurementTypeBitVec {
    fn default() -> Self {
        Self::new()
    }
}

impl Display for MeasurementTypeBitVec {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut r = Vec::<&str>::new();
        for mt in MeasurementType::iter() {
            match self.get_bit(*mt) {
                Err(e) => {
                    error!("{e:#} at {} at MeasurementTypeBitVec.fmt()", mt.to_str());
                },
                Ok(yes_no) => {
                    if yes_no {
                        r.push(mt.to_str());
                    }
                },
            }
        }
        write!(f, "MeasurementTypeBitVec({})", r.join(", "))
    }
}

impl TryFrom<&[u8]> for MeasurementTypeBitVec {
    type Error = anyhow::Error;
    fn try_from(bytes: &[u8]) -> Result<Self> {
        // Vec does not allow into() its underlying array, only into_boxed_slice().
        if bytes.len() == NUM_BYTES_MSRMNT_TYPE_BIT_VEC {
            let mut res = Self::new();
            res.0.iter_mut().zip(bytes.iter()).for_each(|(r, b)| {
                *r = *b;
            });
            Ok(res)
        } else {
            Err(anyhow!(
                "{bytes:?} length should be {NUM_BYTES_MSRMNT_TYPE_BIT_VEC}"
            ))
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum IncrementalRequest {
    Partial, // start a request, or add to a started request, do not expect a reply.
    Final,   // add to an earlier request, if any, and expect a reply
}

#[derive(Serialize, Deserialize, Debug)]
pub struct GridConnectionEanMeasTimeTypesBytes {
    // grid_connection_ean.as_bytes():
    pub grid_connection_ean: Vec<u8>,
    // provided as time_stamp_to_string(a_dt_utc).as_bytes(), used directly as part of encryption key:
    pub meas_dt: Vec<u8>,
    pub msrmnt_types_bit_vec: MeasurementTypeBitVec,
}

#[must_use]
pub fn slice_u8_to_string(u: &[u8]) -> String {
    String::from_utf8(u.to_owned()).unwrap_or_else(|_e| format!("{u:?}"))
}

impl Display for GridConnectionEanMeasTimeTypesBytes {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "GridConnectionEanMeasTimeTypesBytes(ean {}, meas_dt {}, {})",
            slice_u8_to_string(&self.grid_connection_ean),
            slice_u8_to_string(&self.meas_dt),
            self.msrmnt_types_bit_vec,
        )
    }
}

// Request from aggregator to key adder:
#[derive(Serialize, Deserialize, Debug)]
pub struct EncKeyAddRequest {
    pub request_stage: IncrementalRequest,
    pub gc_ean_meas_dt_types_vec: Vec<GridConnectionEanMeasTimeTypesBytes>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum EncryptionResult {
    PrivacyViolation,
    TotalEncryptionKey(String), // String with decimal digits, optional sign and decimal point
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MeasurementTypeTotalEncKey {
    pub msrmnt_type: MeasurementType,
    pub total_encryption_key: EncryptionResult,
    pub total_measurements: u64,
    pub refused_grid_conn_ean_vec: Vec<Vec<u8>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct EncKeyAddReply {
    pub msrmnt_type_total_key_vec: Vec<MeasurementTypeTotalEncKey>,
    pub grid_conn_eans_no_encryption_info: Vec<Vec<u8>>,
}

//  Measurement time interval for the shared encryption seed.
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct ShareSeedInterval {
    pub not_before_dt_vec: Vec<u8>, // bytes (normally 24) of utc string, always with millisecs and trailing Z
    pub not_after_dt_vec: Vec<u8>,  // idem,
}

// Interval borders should be expressed in millisecond resolution,
// these interval borders are normally provided by util::time_stamp_to_string().
// Since not_before_dt and not_after_dt include themselves in the intervals,
// these intervals should be seperated by at least 1 millisecond.
pub const INTERVAL_SEPARATOR_MILLIS: i64 = 1;

impl Display for ShareSeedInterval {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let r = format!(
            "not_before_dt {}, not_after_dt {}",
            slice_u8_to_string(&self.not_before_dt_vec),
            slice_u8_to_string(&self.not_after_dt_vec),
        );
        write!(f, "ShareSeedInterval({r})")
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct ShareSeedEncryptionInfo {
    pub hash_function_name: Vec<u8>, // bytes of name string, for now "blake2s256" (February 2023).
    pub seed: Vec<u8>, // normally 32 bytes, 256 bits, randomly generated by the requestor.
    pub ten_pow_modulus: u16, // modulus of 10^ten_pow_modulus for max encryption key.
    pub ten_pow_divisor: u16, // encryption key after modulus is divided by 10^ten_pow_divisor.
}

impl Display for ShareSeedEncryptionInfo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fn seed_hash(seed: &[u8]) -> u16 {
            #[derive(Hash)]
            struct U8h<'a> {
                s: &'a [u8],
            }
            let h = U8h { s: seed };
            let mut s = DefaultHasher::new();
            h.hash(&mut s);
            let h64 = s.finish();
            (h64 >> 48) as u16 ^ (h64 >> 32) as u16 ^ (h64 >> 16) as u16 ^ h64 as u16
        }

        let r = format!(
            "hash_function name {}, seed hash {:4X}, ten_pow_modulus {}, ten_pow_divisor {}",
            slice_u8_to_string(&self.hash_function_name),
            seed_hash(&self.seed),
            self.ten_pow_modulus,
            self.ten_pow_divisor,
        );
        write!(f, "ShareSeedEncryptionInfo({r})")
    }
}

// Request from p1encryptor to key adder to share an encryption seed for encryption of measured values.
// The encryption of a measurement uses the seed, and the grid connection ean, the type,
// the time and the value of the measurement.
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct ShareSeedRequest {
    pub grid_connection_ean: Vec<u8>, // grid connection ean, e.g. for electricity, gas or heat. Normally 18 decimal digits.
    pub interval: ShareSeedInterval,
    pub encryption_info: ShareSeedEncryptionInfo,
}

impl Display for ShareSeedRequest {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "ShareSeedRequest(grid_connection_ean {}, in {}, {})",
            slice_u8_to_string(&self.grid_connection_ean),
            self.interval,
            self.encryption_info,
        )
    }
}

// The reply to a request to share an encryption seed for measured values.
// Some general error conditions:
// - The grid connection ean is not allowed by the key adder.
// - The interval length is too short or too long.
// - The interval ends too far into the past, or it starts too far into the future.
// - Unreasonable/unknown values for seed, hash_function, ten_pow_modulus or ten_pow_divisor.
// Some conditions involving the same grid connection ean in an earlier request:
// - A duplicate of an earlier valid request is allowed.
// - The requested interval overlaps with an earlier requested interval
//   and some measurements have already been encrypted using the earlier seed.
//   This is an error, the request is denied.
// - The requested interval overlaps with an earlier requested interval, and
//   no measurements have already been encrypted in the currently requested interval.
//   In this case a warning is given to indicate that the validity period of the earlier,
//   now overlapping, request is reduced to a millisecond before the current request.
// - The requested interval starts more than 1 millisecond after the latest interval ends.
//   In this case a warning is given about the gap between these intervals.
#[derive(Serialize, Deserialize, Debug)]
pub struct ShareSeedReply {
    pub message: Option<String>,
}

#[cfg(test)]
mod tests {
    use super::{anyhow, MeasurementType, MeasurementTypeBitVec};

    impl TryFrom<usize> for MeasurementType {
        type Error = anyhow::Error;
        fn try_from(value: usize) -> Result<Self, Self::Error> {
            match Self::VALUES.get(value) {
                None => Err(anyhow!("MeasurementType usize too big {value}")),
                Some(mt) => Ok(*mt),
            }
        }
    }

    #[test]
    fn test_msrmnt_bit_vec_1() {
        let mut mbv1 = MeasurementTypeBitVec::default();
        assert!(!mbv1.any());
        assert!(mbv1.last_set_bit().is_none());
        mbv1.set_bit(MeasurementType::El_kW, true).expect("");
        assert!(mbv1.any());
        assert_eq!(
            MeasurementType::try_from(mbv1.last_set_bit().expect("last_set_bit"))
                .expect("try_from"),
            MeasurementType::El_kW
        );
        assert!(mbv1.get_bit(MeasurementType::El_kW).expect(""));
        assert!(!mbv1.get_bit(MeasurementType::El_kWh_by).expect(""));
    }

    #[test]
    fn test_mt_usize_consistency() {
        let mut next_mtu = 0;
        for mt in MeasurementType::iter() {
            assert_eq!(next_mtu, mt.to_usize()); // value stored/searched in a database
            let mts = mt.to_str();
            assert_eq!(*mt, MeasurementType::try_from(next_mtu).expect(mts));
            next_mtu += 1;
        }
        assert!(MeasurementType::try_from(next_mtu).is_err());

        const VALUES_IN_DB: [MeasurementType; 7] = [
            // copy of MeasurementType::VALUES to simulate mt_usize() values in a db
            MeasurementType::El_kWh,
            MeasurementType::El_kW,
            MeasurementType::NgNl_m3,
            MeasurementType::El_kWh_to,
            MeasurementType::El_kWh_by,
            MeasurementType::El_kW_to,
            MeasurementType::El_kW_by,
        ];

        assert_eq!(MeasurementType::iter().len(), VALUES_IN_DB.iter().len());
        for (mt_new, mt_old) in MeasurementType::iter().zip(VALUES_IN_DB.iter()) {
            assert_eq!(mt_new, mt_old);
        }
    }
}
