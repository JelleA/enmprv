use std::collections::{hash_map, HashMap};
use std::fs::{create_dir_all, remove_file, File};
use std::io::{Read, Write};
use std::path::{self, Path};
use std::process;
use std::rc::Rc;
use std::thread;

use anyhow::{anyhow, Error, Result};
use log::error;
use serde::Deserialize;

use crate::util::bash_cmd_output;

fn thread_id_string() -> String {
    let thread_id_dbg = format!("{:?}", thread::current().id());
    if let Some(open_pos) = thread_id_dbg.find('(') {
        if let Some(close_pos) = thread_id_dbg[open_pos + 2..].find(')') {
            return thread_id_dbg[open_pos + 1..open_pos + 2 + close_pos].to_string();
        }
    }
    thread_id_dbg
}

fn expect_empty(title: &str, mes: &[u8]) -> Result<()> {
    if mes.is_empty() {
        Ok(())
    } else if let Ok(s) = std::str::from_utf8(mes) {
        Err(anyhow!("{title} non empty: {s}"))
    } else {
        Err(anyhow!("{title} non utf8: {mes:?}"))
    }
}

fn rm_file(file_path: &Path) -> Result<()> {
    remove_file(file_path).map_err(Error::new)
}

#[derive(Deserialize, Debug, Clone)]
pub struct OpenSslSignerFile {
    pub private_key: String,
    pub key_pass_phrase: Option<String>,
}

impl OpenSslSignerFile {
    pub fn open_ssl_signer(&self, temp_dir_name: &str) -> Result<OpenSslSigner> {
        OpenSslSigner::new(
            &home_path(&self.private_key),
            self.key_pass_phrase.clone(),
            temp_dir_name,
        )
    }
}

const SIG_FILE_SUFFIX: &str = "sig";
const DATA_FILE_SUFFIX: &str = "data";

fn thread_unique_tmp_file_name(temp_dir_name: &str, suffix: &str) -> String {
    format!(
        "{temp_dir_name}{}{}-{}-{suffix}",
        path::MAIN_SEPARATOR,
        process::id(),
        thread_id_string()
    )
}

pub struct OpenSslSigner {
    private_key_file_name: String,
    key_pass_phrase: Option<String>,
    temp_dir_name: String,
}

impl OpenSslSigner {
    pub fn new(
        private_key_file_name: &str,
        key_pass_phrase: Option<String>,
        temp_dir_name: &str,
    ) -> Result<Self> {
        if let Err(e) = create_dir_all(temp_dir_name) {
            Err(Error::new(e).context(format!("create_dir_all {temp_dir_name}")))
        } else {
            let priv_key_path = home_path(private_key_file_name);
            if Path::new(&priv_key_path).exists() {
                Ok(OpenSslSigner {
                    private_key_file_name: priv_key_path,
                    key_pass_phrase,
                    temp_dir_name: temp_dir_name.to_owned(),
                })
            } else {
                Err(anyhow!("{priv_key_path} does not exist"))
            }
        }
    }

    pub fn generate_signature(&self, data: &[u8]) -> Result<Vec<u8>> {
        let data_file_name = thread_unique_tmp_file_name(&self.temp_dir_name, DATA_FILE_SUFFIX);
        {
            let mut data_file = match File::create(&data_file_name) {
                Ok(f) => f,
                Err(e) => return Err(anyhow!("{e:?} at File::create {data_file_name}")),
            };
            if let Err(e) = data_file.write_all(data) {
                return Err(anyhow!("{e:?} at write_all {data_file_name}"));
            }
        }
        let sig_file_name = thread_unique_tmp_file_name(&self.temp_dir_name, SIG_FILE_SUFFIX);

        let mut cmd_parts: Vec<&str> =
            ["openssl dgst -sha256 -sign", &self.private_key_file_name].to_vec();

        let pass_phrase_string: String; // owner to outlive reference in cmd_parts
        if let Some(ref pass_phrase) = self.key_pass_phrase {
            pass_phrase_string = format!("pass:{pass_phrase}");
            ["-passin", &pass_phrase_string]
                .iter()
                .for_each(|s| cmd_parts.push(*s));
        }

        ["-out", &sig_file_name, &data_file_name]
            .iter()
            .for_each(|s| cmd_parts.push(*s));

        let cmd = cmd_parts.join(" ");
        let result = bash_cmd_output(&cmd)?;
        expect_empty("stdout", &result.stdout)?;
        expect_empty("stderr", &result.stderr)?;
        let mut signature = Vec::<u8>::new();
        {
            let mut sig_file = match File::open(&sig_file_name) {
                Ok(f) => f,
                Err(e) => return Err(anyhow!("{e:?} at File::open {sig_file_name}")),
            };
            if let Err(e) = sig_file.read_to_end(&mut signature) {
                return Err(anyhow!("{e:?} at read_to_end {sig_file_name}"));
            }
        }
        rm_file(Path::new(&sig_file_name))?;
        rm_file(Path::new(&data_file_name))?;
        if result.status.success() {
            Ok(signature)
        } else {
            Err(anyhow!("cmd {cmd} status: {:?}", result.status))
        }
    }
}

#[derive(PartialEq)]
pub struct OpenSslSigVerifier {
    public_key_file_name: String,
    temp_dir_name: String,
}

impl OpenSslSigVerifier {
    pub fn new(public_key_file_name: &str, temp_dir_name: &str) -> Result<Self> {
        if let Err(e) = create_dir_all(temp_dir_name) {
            Err(Error::new(e).context(format!("create_dir_all {temp_dir_name}")))
        } else {
            let pub_key_path = home_path(public_key_file_name);
            if Path::new(&pub_key_path).exists() {
                Ok(OpenSslSigVerifier {
                    public_key_file_name: pub_key_path,
                    temp_dir_name: temp_dir_name.to_string(),
                })
            } else {
                Err(anyhow!("{pub_key_path} does not exist"))
            }
        }
    }

    pub fn verify_signature(&self, signed_data: &[u8], signature: &[u8]) -> Result<bool> {
        let sig_file_name = thread_unique_tmp_file_name(&self.temp_dir_name, SIG_FILE_SUFFIX);
        {
            let mut sig_file = match File::create(&sig_file_name) {
                Ok(f) => f,
                Err(e) => return Err(Error::new(e)),
            };
            if let Err(e) = sig_file.write_all(signature) {
                return Err(Error::new(e));
            }
        }
        let data_file_name = thread_unique_tmp_file_name(&self.temp_dir_name, DATA_FILE_SUFFIX);
        {
            let mut data_file = match File::create(&data_file_name) {
                Ok(f) => f,
                Err(e) => return Err(anyhow!("{e:?} at File::create {data_file_name}")),
            };
            if let Err(e) = data_file.write_all(signed_data) {
                return Err(anyhow!("{e:?} at write_all {data_file_name}"));
            }
        }
        let cmd = [
            "openssl dgst -sha256 -verify",
            &self.public_key_file_name,
            "-signature",
            &sig_file_name,
            &data_file_name,
        ]
        .join(" ");
        let result = bash_cmd_output(&cmd)?;
        rm_file(Path::new(&sig_file_name))?;
        rm_file(Path::new(&data_file_name))?;
        let title_stdout = "stdout";
        let title_stderr = "stderr";
        if result.status.success() {
            if *"Verified OK\n".as_bytes() == result.stdout {
                expect_empty(title_stderr, &result.stderr)?;
                Ok(true)
            } else {
                expect_empty(title_stdout, &result.stdout)?;
                expect_empty(title_stderr, &result.stderr)?;
                Ok(false)
            }
        } else {
            expect_empty(title_stdout, &result.stdout)?;
            expect_empty(title_stderr, &result.stderr)?;
            Err(anyhow!("cmd: {cmd} status: {:?}", result.status))
        }
    }
}

fn home_path(path: &str) -> String {
    if path.starts_with('/') {
        path.to_string()
    } else if let Ok(home) = std::env::var("HOME") {
        [&home, "/", path].join("")
    } else {
        error!("HOME environment variable not available");
        ["/", path].join("")
    }
}

pub type SigVerifierByGridEan = HashMap<Vec<u8>, Rc<OpenSslSigVerifier>>;

fn insert_verifier(
    config_name: &str,
    grid_connection_ean: &str,
    sig_verifier: &Rc<OpenSslSigVerifier>,
    res: &mut SigVerifierByGridEan,
) -> Result<()> {
    match res.entry(grid_connection_ean.as_bytes().to_vec()) {
        hash_map::Entry::Vacant(ve) => {
            ve.insert(Rc::clone(sig_verifier));
            Ok(())
        },
        hash_map::Entry::Occupied(_) => Err(anyhow!(
            "{config_name} {grid_connection_ean} occurs more than once"
        )),
    }
}

pub fn sig_verifier_by_grid_connection_ean_from_config(
    config_name: &str,
    config_value: &toml::Value,
    temp_dir_name: &str,
) -> Result<SigVerifierByGridEan> {
    let mut res = SigVerifierByGridEan::new();
    match config_value {
        toml::Value::Table(table) => {
            for (public_key_file_name, value) in table {
                let sig_verifier = Rc::new(OpenSslSigVerifier::new(
                    public_key_file_name,
                    temp_dir_name,
                )?);
                match value {
                    toml::Value::Array(grid_connection_eans) => {
                        for grid_connection_ean_conf in grid_connection_eans {
                            match grid_connection_ean_conf {
                                toml::Value::String(grid_connection_ean) => {
                                    insert_verifier(
                                        config_name,
                                        grid_connection_ean,
                                        &sig_verifier,
                                        &mut res,
                                    )
                                    .map_err(|e| e.context("insert_verifier"))?;
                                },
                                _ => {
                                    return Err(anyhow!(
                                        "{config_name} expected a string: {grid_connection_ean_conf:?}"
                                    ));
                                },
                            }
                        }
                    },
                    toml::Value::String(grid_connection_ean) => {
                        insert_verifier(config_name, grid_connection_ean, &sig_verifier, &mut res)
                            .map_err(|e| e.context("insert_verifier"))?;
                    },
                    _ => {
                        return Err(anyhow!(
                            "{config_name} for {public_key_file_name} is {value:?}"
                        ))
                    },
                }
            }
        },
        _ => return Err(anyhow!("{config_name} is {config_value:?}")),
    }
    Ok(res)
}
