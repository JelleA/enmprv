use std::cmp::Ordering;
use std::convert::From;
use std::f64::consts::{LN_10, LN_2};
use std::iter::Sum;
use std::ops::{Add, AddAssign, Mul, Neg, Sub, SubAssign};
use std::str::FromStr;

use anyhow::{anyhow, Error, Result};
use num::{bigint::Sign, rational::Ratio, BigInt, Integer, Signed};
use num::{One, Zero};

// Represents the value big_int * 10 ** pow_ten
#[derive(Debug, Clone)]
pub struct BigIntPowTen {
    pow_ten: i32,
    big_int: BigInt,
}

fn tenpow(p: u32) -> BigInt {
    BigInt::from(10).pow(p)
}

impl BigIntPowTen {
    #[must_use]
    pub fn new(big_int: BigInt, pow_ten: i32) -> Self {
        let mut r = BigIntPowTen { pow_ten, big_int };
        r.normalize();
        r
    }

    fn normalize(&mut self) {
        if self.big_int.is_zero() {
            self.pow_ten = 0;
        } else {
            let bi_ten = BigInt::from(10);
            loop {
                let (quot, rem) = self.big_int.div_rem(&bi_ten);
                if !rem.is_zero() {
                    return;
                }
                self.big_int = quot;
                self.pow_ten += 1;
            }
        }
    }

    #[must_use]
    pub fn zero() -> Self {
        BigIntPowTen {
            pow_ten: 0,
            big_int: BigInt::zero(),
        }
    }

    #[must_use]
    pub fn abs(&self) -> Self {
        BigIntPowTen {
            pow_ten: self.pow_ten,
            big_int: self.big_int.abs(),
        }
    }

    #[must_use]
    pub fn is_zero(&self) -> bool {
        self.big_int.is_zero()
    }

    #[must_use]
    pub fn one() -> Self {
        BigIntPowTen {
            pow_ten: 0,
            big_int: BigInt::one(),
        }
    }

    #[must_use]
    pub fn to_string(&self, decimal_digits: u64) -> String {
        let (sign, mut res) = if self.big_int.is_negative() {
            (Sign::Minus, self.big_int.abs().to_string())
        } else {
            (Sign::Plus, self.big_int.to_string())
        };
        if self.pow_ten >= 0 {
            // append pow_ten zeros
            for _ in 0..self.pow_ten {
                res.push('0');
            }
            if decimal_digits > 0 {
                // append dot and zeros:
                res.push('.');
                for _ in 0..decimal_digits {
                    res.push('0');
                }
            }
        } else {
            let dot_pos: usize; // self.pow_ten is negative
            if self.pow_ten <= -(res.len() as i32) {
                // prepend zeros and a dot
                for _ in 0..((-(res.len() as i32)) - self.pow_ten) {
                    res.insert(0, '0');
                }
                res.insert_str(0, "0.");
                dot_pos = 1;
            } else {
                dot_pos = ((res.len() as i32) + self.pow_ten) as usize;
                res.insert(dot_pos, '.');
            }
            let actual_dec_digits: usize = res.len() - (dot_pos + 1);
            for _ in 0..((decimal_digits as i64) - (actual_dec_digits as i64)) {
                res.push('0');
            }
            for _ in 0..((actual_dec_digits as i64) - (decimal_digits as i64)) {
                // remove final digit without rounding
                res.pop();
            }
        }
        if sign == Sign::Minus {
            res.insert(0, '-');
        }
        res
    }

    // Round to 10 ** pow_ten
    #[must_use]
    pub fn round(mut self, pow_ten: i32) -> Self {
        if pow_ten > self.pow_ten {
            // divide self.big_int by 10 ** (pow_ten - self.pow_ten)
            let tp = tenpow((pow_ten - self.pow_ten) as u32);
            let q = Ratio::<BigInt>::new(self.big_int, tp);
            // round, and to_integer() back to BigInt
            self.big_int = q.round().to_integer();
            self.pow_ten = pow_ten;
            self.normalize();
        }
        self
    }

    #[must_use]
    pub fn divide(self, rhs: usize, result_digits: u32) -> Option<Self> {
        // The number of valid bits/digits in self is unknown,
        // so the number of valid result bits/digits in the division result must be provided.
        if rhs == 0 {
            None
        } else if self.big_int.is_zero() {
            Some(self)
        } else {
            // Avoid truncation towards zero for small big_int, e.g. a power of 10 has big_int 1.
            // Multiply self.big_int by 10 until the division result has at least result_digits.
            let rhs_bits = f64::from(usize::BITS as i32 - rhs.leading_zeros() as i32);
            let result_bits = f64::from(result_digits) * LN_10 / LN_2;
            let min_bits_bi = (result_bits + rhs_bits).ceil() as i32;
            let mut big_int = self.big_int.clone();
            let mut pow_ten = self.pow_ten;
            while (big_int.bits() as i32) < min_bits_bi {
                // CHECKME: compute pow_ten directly using LN_10 / LN_2 and initial bi.bits()
                // and multiply by tenpow() instead of repeating *= 10  ?
                big_int *= 10;
                pow_ten -= 1;
            }
            Some(BigIntPowTen::new(big_int / rhs, pow_ten))
        }
    }
}

impl PartialEq for BigIntPowTen {
    fn eq(&self, rhs: &BigIntPowTen) -> bool {
        self.pow_ten == rhs.pow_ten && self.big_int == rhs.big_int
    }
}

impl PartialOrd for BigIntPowTen {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(match (self.big_int.sign(), other.big_int.sign()) {
            (Sign::Plus | Sign::NoSign, Sign::Minus) => Ordering::Greater,
            (Sign::Minus, Sign::Plus | Sign::NoSign) => Ordering::Less,
            (Sign::Plus | Sign::NoSign, Sign::Plus | Sign::NoSign) | (Sign::Minus, Sign::Minus) => {
                match self.pow_ten.cmp(&other.pow_ten) {
                    Ordering::Less => {
                        let dp = (other.pow_ten - self.pow_ten) as u32;
                        self.big_int.cmp(&(&other.big_int * tenpow(dp)))
                    },
                    Ordering::Greater => {
                        let dp = (self.pow_ten - other.pow_ten) as u32;
                        self.big_int.cmp(&(&other.big_int * tenpow(dp)))
                    },
                    Ordering::Equal => self.big_int.cmp(&other.big_int),
                }
            },
        })
    }
}

macro_rules! impl_from {
    ($int_type:ident) => {
        impl From<$int_type> for BigIntPowTen {
            fn from(i: $int_type) -> Self {
                BigIntPowTen::new(BigInt::from(i), 0)
            }
        }
    };
}

//impl_from! {i8}
//impl_from! {i16}
impl_from! {i32}
//impl_from! {i64}
//impl_from! {u8}
//impl_from! {u16}
//impl_from! {u32}
//impl_from! {u64}
//impl_from! {BigInt}
//impl_from! {BigUint}

impl Neg for BigIntPowTen {
    type Output = Self;
    fn neg(self) -> Self::Output {
        BigIntPowTen {
            pow_ten: self.pow_ten,
            big_int: -self.big_int,
        }
    }
}

impl FromStr for BigIntPowTen {
    type Err = Error;
    fn from_str(s: &str) -> Result<BigIntPowTen> {
        if let Some(dot_pos) = s.find('.') {
            if dot_pos == 0 {
                return Err(anyhow!("missing initial digit: {s}"));
            }
            let mut wn = String::new();
            wn.push_str(&s[..dot_pos]);
            wn.push_str(&s[(dot_pos + 1)..]);
            match BigInt::from_str(&wn) {
                Err(e) => Err(Error::new(e).context(format!("BigInt::from_str {s}"))),
                Ok(big_int) => Ok(BigIntPowTen::new(
                    big_int,
                    ((dot_pos + 1) as i32) - (s.len() as i32),
                )),
            }
        } else {
            match BigInt::from_str(s) {
                Err(e) => Err(Error::new(e).context(format!("BigInt::from_str {s}"))),
                Ok(big_int) => Ok(BigIntPowTen::new(big_int, 0)),
            }
        }
    }
}

impl<'a> Add for &'a BigIntPowTen {
    type Output = BigIntPowTen;
    fn add(self, rhs: &'a BigIntPowTen) -> BigIntPowTen {
        match self.pow_ten.cmp(&rhs.pow_ten) {
            Ordering::Equal => BigIntPowTen::new(&self.big_int + &rhs.big_int, self.pow_ten),
            Ordering::Less => {
                let dp = (rhs.pow_ten - self.pow_ten) as u32;
                BigIntPowTen::new(&self.big_int + &rhs.big_int * tenpow(dp), self.pow_ten)
            },
            Ordering::Greater => {
                let dp = (self.pow_ten - rhs.pow_ten) as u32;
                BigIntPowTen::new(&self.big_int * tenpow(dp) + &rhs.big_int, rhs.pow_ten)
            },
        }
    }
}

impl AddAssign for BigIntPowTen {
    fn add_assign(&mut self, rhs: BigIntPowTen) {
        match self.pow_ten.cmp(&rhs.pow_ten) {
            Ordering::Equal => {
                self.big_int += rhs.big_int;
            },
            Ordering::Less => {
                let dp = (rhs.pow_ten - self.pow_ten) as u32;
                self.big_int += rhs.big_int * tenpow(dp);
            },
            Ordering::Greater => {
                let dp = (self.pow_ten - rhs.pow_ten) as u32;
                self.big_int = self.big_int.clone() * tenpow(dp) + rhs.big_int;
                self.pow_ten = rhs.pow_ten;
            },
        };
        self.normalize();
    }
}

impl<'a> Sub for &'a BigIntPowTen {
    type Output = BigIntPowTen;
    fn sub(self, rhs: &BigIntPowTen) -> Self::Output {
        match self.pow_ten.cmp(&rhs.pow_ten) {
            Ordering::Equal => BigIntPowTen::new(&self.big_int - &rhs.big_int, self.pow_ten),
            Ordering::Less => {
                let dp = (rhs.pow_ten - self.pow_ten) as u32;
                BigIntPowTen::new(&self.big_int - &rhs.big_int * tenpow(dp), self.pow_ten)
            },
            Ordering::Greater => {
                let dp = (self.pow_ten - rhs.pow_ten) as u32;
                BigIntPowTen::new(&self.big_int * tenpow(dp) - &rhs.big_int, rhs.pow_ten)
            },
        }
    }
}

impl SubAssign for BigIntPowTen {
    fn sub_assign(&mut self, rhs: BigIntPowTen) {
        match self.pow_ten.cmp(&rhs.pow_ten) {
            Ordering::Equal => {
                self.big_int -= rhs.big_int;
            },
            Ordering::Less => {
                let dp = (rhs.pow_ten - self.pow_ten) as u32;
                self.big_int -= rhs.big_int * tenpow(dp);
            },
            Ordering::Greater => {
                let dp = (self.pow_ten - rhs.pow_ten) as u32;
                self.big_int = self.big_int.clone() * tenpow(dp) - rhs.big_int;
                self.pow_ten = rhs.pow_ten;
            },
        };
        self.normalize();
    }
}

impl<'a> Mul for &'a BigIntPowTen {
    type Output = BigIntPowTen;
    fn mul(self, rhs: &BigIntPowTen) -> Self::Output {
        BigIntPowTen::new(&self.big_int * &rhs.big_int, self.pow_ten + rhs.pow_ten)
    }
}

fn add_non_normalized(bipt: &BigIntPowTen, res: &mut BigIntPowTen) {
    match res.pow_ten.cmp(&bipt.pow_ten) {
        Ordering::Equal => {
            res.big_int += &bipt.big_int;
        },
        Ordering::Less => {
            let dp = (bipt.pow_ten - res.pow_ten) as u32;
            res.big_int += &bipt.big_int * tenpow(dp);
        },
        Ordering::Greater => {
            let dp = (res.pow_ten - bipt.pow_ten) as u32;
            res.big_int = &res.big_int * tenpow(dp) + &bipt.big_int;
            res.pow_ten = bipt.pow_ten;
        },
    };
}

impl<'a> Sum<&'a BigIntPowTen> for BigIntPowTen {
    fn sum<I>(mut iter: I) -> BigIntPowTen
    where
        I: Iterator<Item = &'a BigIntPowTen>,
    {
        if let Some(first) = iter.next() {
            let mut res = first.clone();
            for bipt in iter {
                add_non_normalized(bipt, &mut res);
            }
            res.normalize();
            res
        } else {
            BigIntPowTen::zero()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use num::BigInt;

    #[test]
    fn bipt_zero_to_string() {
        let z = BigIntPowTen::zero();
        assert_eq!(z.to_string(0), "0");
        assert_eq!(z.to_string(1), "0.0");
        assert_eq!(z.to_string(2), "0.00");
    }

    #[test]
    fn bipt_one_to_string() {
        let r = BigIntPowTen::one();
        assert_eq!(r.to_string(0), "1");
        assert_eq!(r.to_string(1), "1.0");
        assert_eq!(r.to_string(2), "1.00");
        let r = -BigIntPowTen::one();
        assert_eq!(r.to_string(0), "-1");
        assert_eq!(r.to_string(1), "-1.0");
        assert_eq!(r.to_string(2), "-1.00");
    }

    #[test]
    fn bipt_from_str_err() {
        assert!(BigIntPowTen::from_str("").is_err());
        assert!(BigIntPowTen::from_str("a").is_err());
        assert!(BigIntPowTen::from_str("5d.0").is_err());
        assert!(BigIntPowTen::from_str("0.b").is_err());
        assert!(BigIntPowTen::from_str(".c").is_err());
        assert!(BigIntPowTen::from_str(".0").is_err());
    }

    fn bipt_from_str(s: &str) -> BigIntPowTen {
        BigIntPowTen::from_str(s).unwrap()
    }

    #[test]
    fn bipt_from_str_zero() {
        let z = BigIntPowTen::zero();
        assert_eq!(bipt_from_str("0"), z);
        assert_eq!(bipt_from_str("0."), z);
        assert_eq!(bipt_from_str("0.0"), z);
        assert_eq!(bipt_from_str("0.00"), z);
        assert_eq!(bipt_from_str("+0"), z);
        assert_eq!(bipt_from_str("+0."), z);
        assert_eq!(bipt_from_str("+0.0"), z);
        assert_eq!(bipt_from_str("+0.00"), z);
        assert_eq!(bipt_from_str("-0"), z);
        assert_eq!(bipt_from_str("-0."), z);
        assert_eq!(bipt_from_str("-0.0"), z);
        assert_eq!(bipt_from_str("-0.00"), z);
    }

    #[test]
    fn bipt_whole_to_string() {
        for i in 0..101 {
            let z = BigIntPowTen::from(i);
            assert_eq!(bipt_from_str(&format!("{}", i)), z);
            assert_eq!(bipt_from_str(&format!("{}", -i)), -z);
        }
    }

    fn tenpowf32(pow: i32) -> f32 {
        let mut r = 1f32;
        for _ in 0..pow {
            r *= 10f32;
        }
        for _ in 0..-pow {
            r /= 10f32;
        }
        r
    }

    #[test]
    fn bipt_to_string_r() {
        for pow in &[1, 2, 3, 4] {
            for i in 1..101 {
                let ip = BigIntPowTen::new(BigInt::from(i), *pow);
                assert_eq!(
                    ip.to_string(4).parse::<f32>().unwrap(),
                    (i as f32) * tenpowf32(*pow),
                );
                let imp = BigIntPowTen::new(BigInt::from(i), -*pow);
                assert_eq!(
                    imp.to_string(4).parse::<f32>().unwrap(),
                    ((i as f32) * tenpowf32(-pow) * 10000f32).round() / 10000f32,
                );
            }
        }
    }

    #[test]
    fn bipt_from_str_r() {
        for pow in &[1, 2, 3, 4] {
            for i in 1..101 {
                let ip = BigIntPowTen::new(BigInt::from(i), *pow);
                let sip = format!("{}", (i as f32) * tenpowf32(*pow));
                assert_eq!(bipt_from_str(&sip), ip,);
                let imp = BigIntPowTen::new(BigInt::from(i), -*pow);
                let simp = format!("{}", (i as f32) / tenpowf32(*pow));
                assert_eq!(bipt_from_str(&simp), imp,);
            }
        }
    }

    #[test]
    fn bipt_sums() {
        let bipt_ar: [BigIntPowTen; 4] = [
            bipt_from_str("40.0"),
            bipt_from_str("3.00"),
            bipt_from_str("0.01"),
            bipt_from_str("0.29"),
        ];

        let s1 = bipt_ar.iter().sum::<BigIntPowTen>(); // runs Sum<&BigIntPowTen>
        assert_eq!(s1, bipt_from_str("43.3"));
    }

    #[test]
    fn bipt_empty_sum() {
        assert_eq!(
            std::iter::empty::<&BigIntPowTen>().sum::<BigIntPowTen>(),
            BigIntPowTen::zero()
        );
    }

    #[test]
    fn bipt_partial_ord() {
        assert!(bipt_from_str("0.1") < bipt_from_str("0.11"));
        assert!(bipt_from_str("0.1") <= bipt_from_str("0.11"));
        assert!(bipt_from_str("0.11") > bipt_from_str("0.1"));
        assert!(bipt_from_str("0.11") >= bipt_from_str("0.1"));

        assert!(bipt_from_str("-0.1") > bipt_from_str("-0.11"));
        assert!(bipt_from_str("-0.1") >= bipt_from_str("-0.11"));
        assert!(bipt_from_str("-0.11") < bipt_from_str("-0.1"));
        assert!(bipt_from_str("-0.11") <= bipt_from_str("-0.1"));
    }
}
