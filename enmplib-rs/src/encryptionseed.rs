use std::collections::{hash_map::Entry, HashMap};
use std::fmt::{self, Display};

use anyhow::{anyhow, Result};
use blake2::{Blake2s256, Digest};
use chrono::{DateTime, Utc};
use log::{error, info, trace};
use num::{
    bigint::Sign,
    {BigInt, Integer},
};

use crate::bincoded::{
    slice_u8_to_string, MeasurementType, ShareSeedEncryptionInfo, ShareSeedInterval,
    ShareSeedRequest,
};
use crate::util::time_stamp_to_string;

#[must_use]
pub fn encryption_key(
    seed: &[u8],
    grid_conn_ean: &[u8],
    meas_dt: &[u8],
    msrmnt_type: MeasurementType,
    enc_modulus: &BigInt,
) -> BigInt {
    let mut hasher: Blake2s256 = Blake2s256::new();
    for part in &[
        seed,
        grid_conn_ean,
        msrmnt_type.to_str().as_bytes(),
        meas_dt,
    ] {
        hasher.update(part);
    }
    BigInt::from_bytes_be(Sign::Plus, &hasher.finalize()).mod_floor(enc_modulus)
}

#[derive(Debug)]
pub struct IntervalAndEncryptionInfo {
    interval: ShareSeedInterval,
    encryption_info: ShareSeedEncryptionInfo,
}

impl Display for IntervalAndEncryptionInfo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let r = format!("{}, {}", self.interval, self.encryption_info);
        write!(f, "IntervalAndEncryptionInfo({r})")
    }
}

fn display_iev(iev: &Vec<IntervalAndEncryptionInfo>) -> String {
    let mut r = String::new();
    for ie in iev {
        if !r.is_empty() {
            r.push_str(", ");
        }
        r.push_str(&format!("{ie}"));
    }
    format!("[{r}]")
}

pub struct SharedSeeds(HashMap<Vec<u8>, Vec<IntervalAndEncryptionInfo>>);

#[derive(Debug)]
pub enum ShareSeedAddResult {
    Ok,
    Duplicate,
}

impl Default for SharedSeeds {
    fn default() -> Self {
        Self::new()
    }
}

impl SharedSeeds {
    #[must_use]
    pub fn new() -> Self {
        Self(HashMap::new())
    }

    pub fn add_share_seed_request(
        &mut self,
        ssr: &ShareSeedRequest,
        keep_oldest_dt: &DateTime<Utc>,
        current_time_stamp: &DateTime<Utc>,
    ) -> Result<ShareSeedAddResult> {
        if ssr.interval.not_after_dt_vec.as_slice()
            < time_stamp_to_string(current_time_stamp).as_bytes()
        {
            return Err(anyhow!(
                "interval not_after_dt {} expired {}",
                slice_u8_to_string(&ssr.interval.not_after_dt_vec),
                current_time_stamp,
            ));
        }
        if ssr.interval.not_before_dt_vec >= ssr.interval.not_after_dt_vec {
            return Err(anyhow!(
                "inconsistent interval not_before_dt {} not_after_dt {}",
                slice_u8_to_string(&ssr.interval.not_before_dt_vec),
                slice_u8_to_string(&ssr.interval.not_after_dt_vec),
            ));
        }
        // add IntervalAndEncryptionInfo for grid ean and removing all later or overlapping intervals for grid ean.
        let request_ie = IntervalAndEncryptionInfo {
            interval: ssr.interval.clone(),
            encryption_info: ssr.encryption_info.clone(),
        };
        match self.0.entry(ssr.grid_connection_ean.clone()) {
            Entry::Vacant(entry) => {
                entry.insert(vec![request_ie]);
                Ok(ShareSeedAddResult::Ok)
            },
            Entry::Occupied(mut entry) => {
                let interval_encr_vec = entry.get_mut();
                // keep iev elements unique and sorted.
                // check for repeated request:
                let share_seed_add_result: ShareSeedAddResult = match interval_encr_vec
                    .binary_search_by(|interval_encr| {
                        interval_encr
                            .interval
                            .not_before_dt_vec
                            .cmp(&request_ie.interval.not_before_dt_vec)
                    }) {
                    Err(insert_pos) => {
                        // different not_before_dt_vec found in binary search,
                        // so ie has an earlier not_before_dt than iev[insert_pos].
                        // insert request_ie and remove all later/overlapping ones.
                        // discard later in iev:
                        let actual_insert_pos: usize = if insert_pos == 0 {
                            // no earlier ie
                            0
                        } else if let Some(prev_interval_encr) =
                            interval_encr_vec.get(insert_pos - 1)
                        {
                            // check previous for non overlapping interval
                            if prev_interval_encr.interval.not_after_dt_vec
                                < request_ie.interval.not_before_dt_vec
                            {
                                // no overlap with previous
                                insert_pos
                            } else {
                                // overlap with previous
                                insert_pos - 1
                            }
                        } else {
                            // unreachable, prefer not to panic:
                            error!(
                                "iev.len() {}, insert_pos {insert_pos}",
                                interval_encr_vec.len()
                            );
                            0 // clear iev by truncate() below
                        };
                        for ie in &interval_encr_vec.as_slice()[actual_insert_pos..] {
                            info!(
                                "removing previously shared encryption info with interval {}",
                                ie.interval
                            );
                        }
                        interval_encr_vec.truncate(actual_insert_pos);
                        interval_encr_vec.push(request_ie);
                        ShareSeedAddResult::Ok
                    },
                    Ok(found_pos) => {
                        // expected when p1encryptor shares a saved seed when it restarts.
                        info!("share seed request at same interval start: {ssr}");
                        // check for same ShareSeedEncryptionInfo
                        let Some(previous_ie) = interval_encr_vec.get(found_pos) else {
                            return Err(anyhow!(
                                "{request_ie} found_pos {found_pos} is not an index in {}",
                                display_iev(interval_encr_vec)
                            ));
                        };
                        if previous_ie.interval.not_after_dt_vec
                            != request_ie.interval.not_after_dt_vec
                        {
                            return Err(anyhow!(
                                "{} {} {} {}",
                                "cannot add share seed interval with not_after_dt_vec",
                                slice_u8_to_string(&request_ie.interval.not_after_dt_vec),
                                "because it differs from previous not_after_dt_vec",
                                slice_u8_to_string(&previous_ie.interval.not_after_dt_vec),
                            ));
                        }
                        if previous_ie.encryption_info != request_ie.encryption_info {
                            return Err(anyhow!(
                                "{} {} {} {}",
                                "cannot add share seed with encryption info",
                                request_ie.encryption_info,
                                "because it differs from previous encryption info",
                                previous_ie.encryption_info,
                            ));
                        }
                        ShareSeedAddResult::Duplicate
                    },
                };
                // Check sorted status of ies.
                // unfortunately slice.is_sorted_by() is still nightly (Sep 2023).
                // verify that no unsorted pair exists, and that there is no overlapping pair.
                if let Some(unsorted_index) = interval_encr_vec[..interval_encr_vec.len() - 1]
                    .iter()
                    .enumerate()
                    .find_map(|(index, ie)| {
                        // verify sorted by not_before_dt_vec
                        if ie
                            .interval
                            .not_before_dt_vec
                            .cmp(&interval_encr_vec[index + 1].interval.not_before_dt_vec)
                            == std::cmp::Ordering::Less
                        {
                            // verify that there is no overlap
                            if ie
                                .interval
                                .not_after_dt_vec
                                .cmp(&interval_encr_vec[index + 1].interval.not_before_dt_vec)
                                == std::cmp::Ordering::Less
                            {
                                None
                            } else {
                                Some(index)
                            }
                        } else {
                            Some(index)
                        }
                    })
                {
                    return Err(anyhow!(
                        "unsorted or overlap at index {unsorted_index}, iev = {}",
                        display_iev(interval_encr_vec)
                    ));
                }
                let keep_oldest_dt_vec = time_stamp_to_string(keep_oldest_dt).as_bytes().to_vec();
                interval_encr_vec.retain(|interval_encr| {
                    if interval_encr.interval.not_after_dt_vec < keep_oldest_dt_vec {
                        trace!(
                            "removing {interval_encr} before {keep_oldest_dt} for grid connection ean {}",
                            slice_u8_to_string(&ssr.grid_connection_ean)
                        );
                        false
                    } else {
                        true
                    }
                });
                Ok(share_seed_add_result)
            },
        }
    }

    pub fn get_encryption_info(
        &self,
        grid_ean: &[u8],
        meas_dt: &[u8],
    ) -> Result<ShareSeedEncryptionInfo> {
        match self.0.get(grid_ean) {
            None => Err(anyhow!(
                "no encryption info at all for grid_ean {}",
                slice_u8_to_string(grid_ean)
            )),
            Some(eiv) => {
                if eiv.is_empty() {
                    Err(anyhow!(
                        "missing encryption infos for {}",
                        slice_u8_to_string(grid_ean)
                    ))
                } else {
                    let (found, mut position) = match eiv.binary_search_by(|ei| {
                        ei.interval.not_before_dt_vec.as_slice().cmp(meas_dt)
                    }) {
                        Err(insert_pos) => (false, insert_pos), // meas_dt smaller than not_before det at insert_pos
                        Ok(found_pos) => (true, found_pos), // unlikely, meas_dt equals not_before_dt at found_pos
                    };

                    if !found && position == 0 {
                        let not_before_dt_str =
                            slice_u8_to_string(&eiv[position].interval.not_before_dt_vec);
                        let meas_dt_str = slice_u8_to_string(meas_dt);
                        Err(anyhow!("msrmnt at {meas_dt_str} before first available interval at {not_before_dt_str}"))
                    } else {
                        if !found {
                            position -= 1;
                        }
                        match eiv.get(position) {
                            None => Err(anyhow!("binary search failed")),
                            Some(ie) => {
                                if meas_dt >= ie.interval.not_before_dt_vec.as_slice()
                                    && meas_dt <= ie.interval.not_after_dt_vec.as_slice()
                                {
                                    Ok(ie.encryption_info.clone())
                                } else {
                                    Err(anyhow!("not in interval at position {position}"))
                                }
                            },
                        }
                    }
                }
            },
        }
    }
}
