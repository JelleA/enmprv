use std::fmt::{self, Display};

use chrono::{DateTime, Utc};

use crate::bigintpowten::BigIntPowTen;
use crate::bincoded::MeasurementType;
use crate::util::time_stamp_to_string;

pub type MeasurementTime = DateTime<Utc>;
pub type MeasuredValue = BigIntPowTen;

#[derive(Debug, Clone)]
pub struct Measurement {
    pub ref_id: String, // equipment id, grid connection ean, aggregator id.
    pub utc_time_stamp: MeasurementTime, // in millisecond resolution
    pub unit: MeasurementType,
    pub value: MeasuredValue,
}

impl Display for Measurement {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Measurement(ref_id {}, at {}, {} {})",
            self.ref_id,
            time_stamp_to_string(&self.utc_time_stamp),
            self.value.to_string(5),
            self.unit.to_str(),
        )
    }
}
