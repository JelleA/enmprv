use core::fmt;
use std::fmt::{Display, Formatter};
use std::io::{Read, Write};
use std::net::{IpAddr, SocketAddr, TcpStream, UdpSocket};
use std::process::{Command, Output};
use std::str::FromStr;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread;
use std::time::{Duration, Instant, SystemTime};

use anyhow::{anyhow, Context, Error, Result};
use chrono::{DateTime, Duration as ChronoDuration, NaiveDateTime, SecondsFormat, Timelike, Utc};
use ctrlc;
use log::{error, trace, warn};
use serde::Deserialize;
use serde_json;

pub fn bash_cmd_output(cmd: &str) -> Result<Output> {
    match Command::new("/bin/bash").arg("-c").arg(cmd).output() {
        Err(e) => Err(anyhow!("{cmd} {e:?}")),
        Ok(output) => Ok(output),
    }
}

pub fn bash_cmd_stdout(cmd: &str) -> Result<String> {
    let result = bash_cmd_output(cmd)?;
    if !result.status.success() {
        Err(anyhow!(
            "cmd {cmd} result.status.success() is false {result:?}"
        ))
    } else if let Ok(s) = std::str::from_utf8(&result.stdout) {
        Ok(s.to_string())
    } else {
        Err(anyhow!(
            "from_utf8 failed on result.stdout, cmd {cmd} result {result:?}"
        ))
    }
}

pub fn wait_clock_synchronized(max_seconds: u64) -> Result<()> {
    let start_time = Instant::now();
    let cmd = "timedatectl show | grep 'NTPSynchronized=yes'";
    loop {
        let result = bash_cmd_output(cmd)?;
        if result.status.success() {
            match std::str::from_utf8(&result.stdout) {
                Ok(mut l) => {
                    if let Some(line) = l.strip_suffix('\n') {
                        l = line;
                    }
                    trace!("timedatectl show: {l}");
                    return Ok(());
                },
                Err(e) => {
                    return Err(anyhow!("{e:?} for stdout from: {cmd}"));
                },
            }
        }
        if start_time.elapsed().as_secs() > max_seconds {
            return Err(anyhow!(
                "clock not synchronized after {max_seconds} seconds"
            ));
        }
        warn!("waiting for clock synchronization");
        thread::sleep(Duration::new(10, 0));
    }
}

pub fn addr_port_str(ip_addr: &str, port: &str) -> Result<String> {
    match IpAddr::from_str(ip_addr) {
        Err(e) => Err(Error::new(e).context(ip_addr.to_owned())),
        Ok(IpAddr::V4(_)) => Ok([ip_addr, port].join(":")),
        Ok(IpAddr::V6(_)) => {
            // See RFC2732, example [2001:db8:4006:812::200e]:8080
            Ok([["[", ip_addr, "]"].join("").as_str(), port].join(":"))
        },
    }
}

pub fn udp_bind(local_addr_port: &str) -> Result<UdpSocket> {
    UdpSocket::bind(local_addr_port).with_context(|| format!("bind for {local_addr_port}"))
}

#[allow(clippy::all)] // See https://github.com/rust-lang/rust-clippy/issues/4033 and 4453
fn same_prefix_bits<const N: usize>(a1: [u8; N], a2: [u8; N], num_bits: u64) -> bool {
    // check octets of ip addresses for equality of prefix bits.
    let mut unchecked_bits = num_bits;
    if num_bits == 0 {
        true
    } else if num_bits as usize > N * (u8::BITS as usize) {
        false
    } else {
        a1.into_iter()
            .zip(a2)
            .take(((num_bits as usize - 1) / (u8::BITS as usize)) + 1)
            .find(|(b1, b2)| {
                // look for first unequal byte pair, check fewer bits when unchecked_bits < 8
                if unchecked_bits >= 8 {
                    if b1 == b2 {
                        unchecked_bits -= 8;
                        false
                    } else {
                        true
                    }
                } else {
                    (b1 ^ b2) >> (8 - unchecked_bits) != 0
                }
            })
            .is_none()
    }
}

pub fn ip_addr_in_wg_intf(ip_addr_str: &str) -> Result<bool> {
    let ip_addr = match IpAddr::from_str(ip_addr_str) {
        Err(e) => return Err(anyhow!("{e:?} for input address {ip_addr_str}")),
        Ok(ia) => ia,
    };
    let cmd = "ip -json -details addr show"; // no need for --pretty
    let stdout = bash_cmd_stdout(cmd)?;
    // Partial output of:
    // ip --json --pretty -d addr show

    // [
    // ...
    //  {
    // ...
    //     "ifname": "wgenmprv0",
    // ...
    //     "linkinfo": {
    //         "info_kind": "wireguard"
    //     },
    // ...
    //     "addr_info": [ {
    //             "family": "inet",
    //             "local": "10.1.0.1",
    //             "prefixlen": 32,
    // ...
    //         },{
    //             "family": "inet6",
    //             "local": "fde2:8b02:e204::a01:1",
    //             "prefixlen": 128,
    // ...
    //         } ]
    // } ]
    type JsonValue = serde_json::Value;
    let ip_json: JsonValue = match serde_json::from_str(&stdout) {
        Ok(json) => json,
        Err(e) => {
            return Err(anyhow!(
                "{e:?} for serde_json::from_str for output of {cmd}"
            ));
        },
    };
    let JsonValue::Array(intfs_array) = ip_json else {
        return Err(anyhow!("expected json array, got {ip_json:?}"));
    };
    for intf_value in &intfs_array {
        let JsonValue::Object(intf_object) = intf_value else {
            return Err(anyhow!(
                "expected json object, got {intf_value:?} for {intfs_array:?}"
            ));
        };
        // if let Some(JsonValue::String(if_name)) = intf_object.get("ifname") {
        //     dbg!(if_name);
        // }
        let Some(JsonValue::Object(link_info_object)) = intf_object.get("linkinfo") else {
            continue;
        };
        if Some(&JsonValue::String("wireguard".to_string())) != link_info_object.get("info_kind") {
            continue;
        }
        let Some(JsonValue::Array(addr_infos_array)) = intf_object.get("addr_info") else {
            warn!("no addr_info on wireguard interface {intf_object:?}");
            continue;
        };
        for addr_info_value in addr_infos_array {
            let JsonValue::Object(addr_info_object) = addr_info_value else {
                return Err(anyhow!(
                    "expected json Object, got {addr_info_value:?} in {addr_infos_array:?}"
                ));
            };
            let Some(JsonValue::String(family)) = addr_info_object.get("family") else {
                warn!("no address family for {addr_info_object:?} in {intf_object:?}");
                continue;
            };
            let Some(JsonValue::String(local_addr_string)) = addr_info_object.get("local") else {
                warn!("no local address for {addr_info_object:?} in {intf_object:?}");
                continue;
            };
            let local_addr = match IpAddr::from_str(local_addr_string) {
                Err(e) => {
                    return Err(anyhow!(
                        "{e:?} for {local_addr_string:?} in {addr_info_object:?}"
                    ));
                },
                Ok(ip_addr) => ip_addr,
            };
            let prefix_len_key = "prefixlen";
            let Some(JsonValue::Number(prefix_len_number)) = addr_info_object.get(prefix_len_key)
            else {
                return Err(anyhow!(
                    "no Number for {prefix_len_key} in {addr_info_object:?}"
                ));
            };
            let Some(prefix_len) = prefix_len_number.as_u64() else {
                return Err(anyhow!(
                    "{prefix_len_number:?} not u64 for {prefix_len_key} in {addr_info_object:?}"
                ));
            };
            match (family.as_str(), ip_addr, local_addr) {
                ("inet", IpAddr::V4(given_v4), IpAddr::V4(addr_v4))
                    if same_prefix_bits(given_v4.octets(), addr_v4.octets(), prefix_len) =>
                {
                    return Ok(true);
                },
                ("inet6", IpAddr::V6(given_v6), IpAddr::V6(addr_v6))
                    if same_prefix_bits(given_v6.octets(), addr_v6.octets(), prefix_len) =>
                {
                    return Ok(true);
                },
                ("inet" | "inet6", _, _) => {}, // ipv4/ipv6 or ip6/ipv4
                _ => {
                    trace!("ignoring family {family:?}, local_address {local_addr:?}, and {prefix_len_key} {prefix_len:?} for ip_addr {ip_addr:?}");
                },
            }
        }
    }
    Ok(false)
}

pub fn wait_ip_addr_in_wg_intf(ip_addr: &str) -> Result<()> {
    let start_time = Instant::now();
    let max_seconds = 60;
    loop {
        match ip_addr_in_wg_intf(ip_addr) {
            Ok(true) => return Ok(()),
            Ok(false) => {
                if start_time.elapsed().as_secs() > max_seconds {
                    return Err(anyhow!(
                        "no wg interface at {ip_addr} after {max_seconds} seconds"
                    ));
                }
            },
            Err(e) => {
                return Err(e.context(format!("ip_addr_in_wg_intf {ip_addr}")));
            },
        }
        warn!("waiting for wg interface at {ip_addr}");
        thread::sleep(Duration::new(10, 0));
    }
}

pub fn tcp_stream_in_wg_intf(socket_addr: &SocketAddr, time_out_secs: u64) -> Result<TcpStream> {
    let timeout = Duration::new(time_out_secs, 0);
    match TcpStream::connect_timeout(socket_addr, timeout) {
        Err(e) => Err(Error::new(e).context(format!("TcpStream::connect_timeout {socket_addr:?}"))),
        Ok(tcp_stream) => match tcp_stream.local_addr() {
            Err(e) => Err(Error::new(e).context("tcp_stream.local_addr")),
            Ok(local_addr_port) => {
                let local_ip_addr_str = local_addr_port.ip().to_string();
                match ip_addr_in_wg_intf(&local_ip_addr_str) {
                    Err(e) => Err(e.context(format!("ip_addr_in_wg_intf {local_ip_addr_str}"))),
                    Ok(res) => {
                        if !res {
                            warn!(
                                "no wireguard ip interface for local address {local_ip_addr_str}"
                            );
                        }
                        Ok(tcp_stream)
                    },
                }
            },
        },
    }
}

pub fn send_version_size_message(message: &[u8], tcp_stream: &mut TcpStream) -> Result<()> {
    let mes_size = message.len();
    if mes_size > u16::MAX.into() {
        Err(anyhow!("message too long: {mes_size}"))
    } else {
        tcp_stream
            .set_nonblocking(false)
            .context("set_nonblocking")?;
        // prefix 2 zero version bytes and 2 length bytes
        let mut version_size: [u8; 4] = [0; 4];
        version_size[2] = mes_size as u8;
        version_size[3] = (mes_size >> 8) as u8;
        tcp_stream
            .write_all(&version_size)
            .context("write_all version_size")?;
        tcp_stream.write_all(message).context("write_all message")?;
        Ok(())
    }
}

pub fn receive_version_size_message(
    tcp_stream: &mut TcpStream,
    read_timeout_seconds: u64,
) -> Result<Vec<u8>> {
    if let Err(e) = tcp_stream.set_nonblocking(false) {
        return Err(Error::new(e).context("set_nonblocking"));
    }
    let receive_timeout = Duration::new(read_timeout_seconds, 0);
    if let Err(e) = tcp_stream.set_read_timeout(Some(receive_timeout)) {
        return Err(Error::new(e).context("set_read_timeout"));
    }
    let mut version_size: [u8; 4] = [0; 4];
    if let Err(e) = tcp_stream.read_exact(&mut version_size) {
        return Err(Error::new(e).context("read_exact version_size"));
    }
    if version_size[0] != 0 || version_size[1] != 0 {
        return Err(anyhow!("unexpected version bytes {:?}", &version_size[..2]));
    }
    let message_size = version_size[2] as usize + ((version_size[3] as usize) << 8);
    trace!("receive_version_size_message size {message_size}");
    let mut message = vec![0; message_size];
    if let Err(e) = tcp_stream.read_exact(&mut message) {
        Err(Error::new(e).context("read_exact"))
    } else {
        Ok(message)
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct DurationConfig {
    // Non negative duration in seconds. Only u16 and u8 fields, default 0.
    weeks: Option<u16>, // about 1260 years
    days: Option<u8>,
    hours: Option<u8>,
    minutes: Option<u8>,
    seconds: Option<u8>,
}

impl Display for DurationConfig {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut rv = Vec::<String>::new();
        if self.weeks() > 0 {
            rv.push(format!("weeks = {}", self.weeks()));
        }
        if self.days() > 0 {
            rv.push(format!("days = {}", self.days()));
        }
        if self.hours() > 0 {
            rv.push(format!("hours = {}", self.hours()));
        }
        if self.minutes() > 0 {
            rv.push(format!("minutes = {}", self.minutes()));
        }
        if self.seconds() > 0 {
            rv.push(format!("seconds = {}", self.seconds()));
        }
        if rv.is_empty() {
            rv.push("seconds = 0".to_owned());
        }
        write!(f, "{{{}}}", rv.join(", "))
    }
}

impl DurationConfig {
    fn weeks(&self) -> u16 {
        self.weeks.unwrap_or(0)
    }

    fn days(&self) -> u8 {
        self.days.unwrap_or(0)
    }

    fn hours(&self) -> u8 {
        self.hours.unwrap_or(0)
    }

    fn minutes(&self) -> u8 {
        self.minutes.unwrap_or(0)
    }

    fn seconds(&self) -> u8 {
        self.seconds.unwrap_or(0)
    }

    #[must_use]
    pub fn std_duration(&self) -> Duration {
        // max Duration is u64::MAX seconds, about 584542 million years.
        Duration::from_secs(
            self.seconds() as u64
                + 60 * (self.minutes() as u64
                    + 60 * (self.hours() as u64
                        + 24 * (self.days() as u64 + 7 * self.weeks() as u64))),
        )
    }

    #[must_use]
    pub fn chrono_duration(&self) -> ChronoDuration {
        // ChronoDuration::MAX is i64::MAX milliseconds, more than 292 million years.
        ChronoDuration::weeks(self.weeks().into())
            + ChronoDuration::days(self.days().into())
            + ChronoDuration::hours(self.hours().into())
            + ChronoDuration::minutes(self.minutes().into())
            + ChronoDuration::seconds(self.seconds().into())
    }
}

#[must_use]
pub fn time_stamp_milli_secs() -> DateTime<Utc> {
    let now = SystemTime::now();
    let now_utc = DateTime::<Utc>::from(now);
    let date = now_utc.naive_utc().date();
    let time = now_utc.time();
    let millis = time.nanosecond() / 1_000_000;
    let nanos = millis * 1_000_000;
    if let Some(naive_time) = time.with_nanosecond(nanos) {
        DateTime::from_naive_utc_and_offset(NaiveDateTime::new(date, naive_time), Utc)
    } else {
        error!("time_stamp_milli_secs no NaiveTime in milli secs from {now_utc:?}");
        now_utc
    }
}

pub fn time_stamp_to_string(time_stamp: &DateTime<Utc>) -> String {
    // As RFC 3339, always including milliseconds, even when zero, and with a trailing Z:
    let use_z: bool = true;
    time_stamp.to_rfc3339_opts(SecondsFormat::Millis, use_z)
}

pub fn time_stamp_from_str(s: &str) -> Result<DateTime<Utc>> {
    match NaiveDateTime::parse_from_str(s, "%Y-%m-%dT%H:%M:%S%.3fZ") {
        Ok(ndt) => Ok(DateTime::<Utc>::from_naive_utc_and_offset(ndt, Utc)),
        Err(e) => Err(Error::new(e).context(format!("NaiveDateTime::parse_from_str {s}"))),
    }
}

#[derive(Debug)]
pub struct CtrlC {}

impl Display for CtrlC {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        f.write_str("ctrl-C")
    }
}

#[derive(Clone)]
pub struct RunState(Arc<AtomicBool>);

impl RunState {
    pub fn new() -> Result<Self> {
        let res = RunState(Arc::new(AtomicBool::new(true)));
        let r = res.0.clone();
        match ctrlc::set_handler(move || {
            r.store(false, Ordering::SeqCst);
        }) {
            Err(e) => Err(Error::new(e).context("set_handler")),
            Ok(_) => Ok(res),
        }
    }

    pub fn is_running(&self) -> Result<()> {
        if self.0.load(Ordering::SeqCst) {
            Ok(())
        } else {
            Err(Error::msg("terminated").context(CtrlC {}))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_ts(ts: &str) {
        let dt = time_stamp_from_str(ts).unwrap();
        let dts = time_stamp_to_string(&dt);
        assert_eq!(ts, dts);
        let dtst = time_stamp_from_str(&dts).unwrap();
        assert_eq!(dt, dtst);
    }

    #[test]
    fn test_time_stamp_string() {
        test_ts("2023-03-25T01:01:03.999Z");
        test_ts("2023-03-25T01:02:03.000Z"); // this used to fail before 2023, suppressing the .000
        test_ts("2023-03-25T01:02:03.001Z");
    }

    #[test]
    fn test_same_prefix_bits() {
        assert!(same_prefix_bits([0], [0], 8));
        assert!(!same_prefix_bits([0], [1], 8));
        assert!(same_prefix_bits([0], [1], 7));
        assert!(!same_prefix_bits([0, 0], [0, 3], 16));
        assert!(same_prefix_bits([0, 0], [0, 3], 14));
    }

    fn test_ok_bool(r: Result<bool>, b: bool) -> bool {
        r.map_or(false, |rb| rb == b)
    }

    #[test]
    #[ignore]
    fn test_ip_addr_in_wg_intf() {
        // Only run this with a wg link with local addresses 10.1.0.1 and fde2:8b02:e204::a01:1
        assert!(test_ok_bool(ip_addr_in_wg_intf("10.1.0.1"), true));
        assert!(test_ok_bool(ip_addr_in_wg_intf("10.1.0.2"), false));
        assert!(test_ok_bool(
            ip_addr_in_wg_intf("fde2:8b02:e204::a01:1"),
            true
        ));
        assert!(test_ok_bool(
            ip_addr_in_wg_intf("fde2:8b02:e204::a01:0"),
            false
        ));
    }
}
