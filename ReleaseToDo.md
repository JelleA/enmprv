# Making a release

A release consists of three binary programs built from the source code as available in a public git repository. The three programs are:
- a p1 encryptor, p1encrypt-rs,
- a key adder, keyadder-rs, and
- an aggregator, aggrmeters-rs.

The release also provides a starting point for configuration of these programs.
This configuration consists of two files per binary: 
a .toml file with runtime parameters, and
a .yaml for to define the log file formats.


# Develop/Test

Test the three binaries in a development environment.

This development environment consists of two computers: 
- a local machine (laptop/desktop) with the development git repository
to change the programs,
- a raspberry pi to run the p1encrypt-rs program. 
  This raspberry is connected to a P1 port of a smart meter.
  This raspberry is accessed by ssh from the local machine.
- a wireguard tunnel between the local machine and the raspberry.

The p1encrypt-rs program is built and started on the raspberry by using the
test/p1encrypt-rs.sh shell script without a command line argument.
This will copy the latest rust sources and configuration files for the p1encrypt-rs program to
the raspberry into workspace enmprv, and build or run it with cargo build or cargo run.

The unit tests for p1encrypt-rs can be run by running test/p1encrypt-rs test.

The keyadder-rs and aggrmeters-rs programs do not have automated tests.
Test these by running their latest versions manually on the local machine
while they are used by one or more running p1encrypt-rs programs.

These latest versions are made by cargo build, not by cargo build --release.

# Increase the version number

Commit the latest changes in the test repository on the master branch.

Update the version mentioned in these files to the new release version number k.l.m:
- Cargo.toml, remove -dev from k.l.m-dev here,
- p1encrypt-rs/p1encrypt-rs.Cargo.toml, remove -dev from k.l.m-dev here,
- p1encrypt-rs/test/p1encrypt-rs.sh, update from the previous release version.

Do a p1encryptor build/run from directory ~/gitrepos/enmprv/p1encrypt-rs
- test/p1encrypt-rs.sh

This build/run normally ends with an early exit in case the (previous) p1encrypt-rs service is running.

Add a release commit on the master branch.
This will involve the above files and the Cargo.lock files changed by the build/run.

Add a local git tag on this commit with a release tag (like v0.1.12)
and a short message for the release.

Push the master branch to the public repository.

Push the release tag to the public repository.

In directory ~/gitrepos/enmprv/p1encrypt-rs run
- test/p1encrypt-rs.sh release

This will:
- checkout the release tag from the public repository 
  into a separate rust workspace enmprv-rel on the raspberry.
- cargo check and cargo build --release the release binaries in the enmprv-rel/target/release/ directory.
  When the Cargo.lock dependencies and/or the rust compiler have been updated since the previous release,
  these cargo steps will take some time.
- run the p1encrypt-rs release binary.
  This run normally ends with an early exit in case the previous p1encrypt-rs service is running.

After the checkout of the release tag and the build on the raspberry,
change the new version number from k.l.m to k.l.(m+1)-dev in these files:
- Cargo.toml, 
- p1encrypt-rs/p1encrypt-rs.Cargo.toml
and commit this on master branch to continue development.

The release binaries are now available on the raspberry in:
- enmprv-rel/target/release/p1encrypt-rs
- enmprv-rel/target/release/keyadder-rs
- enmprv-rel/target/release/aggrmeters-rs

To check the versions of the binaries, ssh into the raspberry
and run each of these binaries with argument -v.

The basic configuration .toml and .yaml files are here:
- enmprv-rel/p1encrypt-rs/p1encrypt-rs.toml
- enmprv-rel/p1encrypt-rs/p1enbglog4rs.yaml
- enmprv-rel/keyadder-rs/keyadder.toml
- enmprv-rel/keyadder-rs/keyaddbglog4rs.yaml
- enmprv-rel/aggrmeters-rs/aggrmeters.toml
- enmprv-rel/aggrmeters-rs/agrmbglog4rs.toml

These configurations are used for development.

To communicate with each other these programs use wireguard tunnels.
The network addresses on these tunnels are defined in the .toml configuration files.

These network addresses will need to be adapted to the tunnel network addresses
of the machines on which these programs are run.

The wireguard tunnels are currently set up manually.
