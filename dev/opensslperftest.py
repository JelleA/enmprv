import subprocess
import datetime

# key_pass_phrase = "asdf"

def time_command(cmd):
    start_time = datetime.datetime.now()
    # print("time_command: "+ cmd)
    completion = subprocess.run(cmd, shell=True)
    run_time = (datetime.datetime.now() - start_time).total_seconds()
    print("time_command " + str(run_time) + " secs: " + cmd)
    if completion.returncode != 0:
        raise Exception(str(completion))

def gen_private_key_command(key_size):
    private_key_file = "private" + str(key_size) + ".pem"
    # return " ".join(["openssl genrsa -aes128 -out", private_key_file, ("-passout pass:" + key_pass_phrase), str(key_size)])
    # unencrypted private key, no pass phrase:
    return " ".join(["openssl genrsa -out", private_key_file, str(key_size)])

def create_data_command():
    data_file = "data.txt"
    return "echo abcd > " + data_file

def sign_data_command(key_size):
    private_key_file = "private" + str(key_size) + ".pem"
    data_file = "data.txt"
    signature_file = "sign.sha256"
    # return " " .join(["openssl dgst -sha256 -sign", private_key_file, ("-passin pass:" + key_pass_phrase), "-out", signature_file, data_file])
    return " " .join(["openssl dgst -sha256 -sign", private_key_file, "-out", signature_file, data_file])

def gen_public_key_command(key_size):
    private_key_file = "private" + str(key_size) + ".pem"
    public_key_file = "public" + str(key_size) + ".pem"
    # return " ".join(["openssl rsa -in", private_key_file, ("-passin pass:" + key_pass_phrase), "-pubout -out", public_key_file])
    return " ".join(["openssl rsa -in", private_key_file, "-pubout -out", public_key_file])

def verify_data_command(key_size):
    public_key_file = "public" + str(key_size) + ".pem"
    signature_file = "sign.sha256"
    data_file = "data.txt"
    return " " .join(["openssl dgst -sha256 -verify", public_key_file, "-signature", signature_file, data_file])


def time_pk_commands(key_size):
    time_command(gen_private_key_command(key_size))
    time_command(gen_public_key_command(key_size))
    time_command(create_data_command())
    time_command(sign_data_command(key_size))
    time_command(verify_data_command(key_size))

if __name__ == "__main__":
    time_pk_commands(key_size = 512)
    time_pk_commands(key_size = 1024)
    time_pk_commands(key_size = 2048)
    time_pk_commands(key_size = 4096)
    time_pk_commands(key_size = 8192)
