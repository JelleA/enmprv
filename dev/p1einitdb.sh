#! /bin/bash -ev
set -u

# Start a new postgress user and db for the p1encryptor on the current host.
# The current user must a sudoer for users postgres and mmenc.
# Postgres should be installed and started.
# This will create a postgres user with $USER name from the environment,
# and a postgress database p1edb owned by this user.

dbname=p1edb
pg_user=postgres
mmenc_user=mmenc

sudo -u $pg_user psql << EOFUD
  CREATE USER $mmenc_user LOGIN PASSWORD NULL;
  CREATE DATABASE $dbname WITH OWNER $mmenc_user;
EOFUD
