#! /usr/bin/python3

"""
Show P1 measurements as present in a postgres db.
"""

from enum import Enum

import datetime
import matplotlib.pyplot as plt

import warnings
import argparse

import psycopg2
from psycopg2 import sql

from itertools import pairwise  # python 3.10

one_second_delta = datetime.timedelta(seconds=1)
five_second_delta = datetime.timedelta(seconds=5)
ten_second_delta = datetime.timedelta(seconds=10)
fifteen_second_delta = datetime.timedelta(seconds=15)
one_minute_delta = datetime.timedelta(minutes=1)
one_quarter_delta = datetime.timedelta(minutes=15)
one_hour_delta = datetime.timedelta(hours=1)
one_day_delta = datetime.timedelta(days=1)
one_month_delta = datetime.timedelta(days=30, hours=11)  # not exact
one_year_delta = datetime.timedelta(days=365, hours=6)  # not exact

# for kW scale on Y axis: msrmnt values are in kWh and m3.
secs_per_hour = 3600
kWh_per_m3 = 9.769
hours_per_day = 24


class MeasurementType(Enum):
    # See rust bincoded::MeasurementType
    # The MeasurementType attribute is stored with each measurement
    El_kWh = 0    # Electric, kWh, cumulative, El_kWh_to minus El_kWh_by
    El_kW = 1     # Electric, kW, non cumulative, CHECKME: instantaneous ?
    NgNl_m3 = 2   # Natural Gas, NL, cumulative, 9.769 kWh per normalized m3.
    El_kWh_to = 3  # cumulative, delivered to the meter from the grid
    El_kWh_by = 4  # cumulative, delivered by meter to the grid
    El_kW_to = 5  # delivered to the meter from the grid
    El_kW_by = 6  # delivered by meter to the grid

    def from_value(value):
        return MeasurementType.by_value[value]


MeasurementType.by_value = dict()  # cannot assign when defined in class scope.
# need defined class to iterate for initialisation:
for msrmnt_type in MeasurementType:
    MeasurementType.by_value[msrmnt_type.value] = msrmnt_type

CumulativeMeasurementTypes = set()
CumulativeMeasurementTypes.add(MeasurementType.El_kWh)
CumulativeMeasurementTypes.add(MeasurementType.NgNl_m3)
CumulativeMeasurementTypes.add(MeasurementType.El_kWh_to)
CumulativeMeasurementTypes.add(MeasurementType.El_kWh_by)


class IntervalBorder(Enum):
    # See rust p1db::IntervalType
    # This attribute is stored with each pair of consecutive measurements
    # to indicate that the interval between this pair contains
    # the border of the longest such interval:
    YearUtc = 0
    YearLocal = 1
    MonthUtc = 2
    MonthLocal = 3
    DayUtc = 4
    DayLocal = 5
    Hour = 6
    Quarter = 7
    Minute = 8
    TenSecond = 9
    Second = 10

    def from_value(value):
        return IntervalBorder.by_value[value]


IntervalBorder.by_value = dict()  # cannot assign when defined in class scope.
# need defined class to iterate for initialisation:
for interval_border in IntervalBorder:
    IntervalBorder.by_value[interval_border.value] = interval_border


p1edb_connect_request = "postgresql://localhost/p1edb?port=5432"


def filter_meter_readings(dt_value_pairs, max_interval_border):
    if (len(dt_value_pairs) <= 2) or (max_interval_border == IntervalBorder.Second):
        return dt_value_pairs

    # for intervals bigger than TenSecond, remove the msrmnts starting the next interval
    min_delta = {
        IntervalBorder.YearUtc: one_hour_delta,
        IntervalBorder.YearLocal: one_hour_delta,
        IntervalBorder.MonthUtc: one_hour_delta,
        IntervalBorder.MonthLocal: one_hour_delta,
        IntervalBorder.DayUtc: one_hour_delta,
        IntervalBorder.DayLocal: one_hour_delta,
        IntervalBorder.Hour: one_hour_delta - fifteen_second_delta,
        IntervalBorder.Quarter: fifteen_second_delta,
        IntervalBorder.Minute: fifteen_second_delta,
        IntervalBorder.TenSecond: five_second_delta,
        IntervalBorder.Second: one_second_delta,
    }[max_interval_border]
    res = dt_value_pairs[0:1]  # keep first msrmnt
    prev_dt = dt_value_pairs[0][0]
    for (dt, val) in dt_value_pairs[1:-1]:
        if (dt - prev_dt) > min_delta:
            res.append((dt, val))
            prev_dt = dt
    res.append(dt_value_pairs[-1])  # keep last msrmnt
    print("Filtered out {} interval start measurements".format(
        len(dt_value_pairs) - len(res)))
    return res


def get_meter_readings_by_types(msrmnt_type_equipment_combis, max_interval_border, start_dt, end_dt):
    # for (msrmnt_type, equipment_ean_combi) in msrmnt_type_equipment_combis:
    #     res[msrmnt_type] = get_meter_readings(equipment_ean_combi, msrmnt_type, max_interval_border, start_dt, end_dt)
    # return res
    start_query_dt = datetime.datetime.now()
    msrmnt_type_values = {msrmnt_type.value for (
        msrmnt_type, _) in msrmnt_type_equipment_combis}
    equipment_ean_combis = {equipment_ean_combi for (
        _, equipment_ean_combi) in msrmnt_type_equipment_combis}
    with psycopg2.connect(p1edb_connect_request) as p1edb_conn:
        with p1edb_conn.cursor() as p1edb_cursor:
            # Possibly also treat start_dt and end_dt as interval borders here.
            # To do this, add min and max available msrmnt_dt_utc between start_dt and end_dt,
            # independent of the given max_interval_border.
            # This would need nested sql, so avoid for now.
            sql_query = sql.SQL(
                """SELECT msrmnt_type, msrmnt_dt_utc, msrmnt_value
FROM measurement
WHERE {start_dt} <= msrmnt_dt_utc AND msrmnt_dt_utc <= {end_dt}
  AND msrmnt_type IN ({msrmnt_type_values})
  AND equipment_ean_combi IN ({ean_combis})
  AND interval_border <= {max_interval_border};"""
            ).format(
                start_dt=sql.Literal(start_dt),
                end_dt=sql.Literal(end_dt),
                msrmnt_type_values=sql.SQL(', ').join(
                    map(sql.Literal, msrmnt_type_values)),
                ean_combis=sql.SQL(', ').join(
                    map(sql.Literal, equipment_ean_combis)),
                max_interval_border=sql.Literal(max_interval_border.value),
            )
            p1edb_cursor.execute(sql_query)
            type_dt_value_triples = p1edb_cursor.fetchall()
    end_query_dt = datetime.datetime.now()
    elapsed = (end_query_dt - start_query_dt).total_seconds()
    print("Selected {} measurements in {} secs".format(
        len(type_dt_value_triples), elapsed))
    print("in interval from {} to {} for {}.".format(
        start_dt, end_dt, max_interval_border))

    res = dict()
    for msrmnt_type_value, msrmnt_dt_utc, msrmnt_value in type_dt_value_triples:
        msrmnt_type = MeasurementType.from_value(msrmnt_type_value)
        dt_value = (msrmnt_dt_utc, msrmnt_value)
        if msrmnt_type in res:
            res[msrmnt_type].append(dt_value)
        else:
            res[msrmnt_type] = [dt_value]

    for msrmnt_type, dt_value_pairs in res.items():
        print("{} {} measurements".format(
            len(dt_value_pairs), msrmnt_type.name))
        dt_value_pairs.sort()
        res[msrmnt_type] = filter_meter_readings(
            dt_value_pairs, max_interval_border)

    return res


def get_msrmnt_count_interval(start_dt, end_dt):
    start_query_dt = datetime.datetime.now()
    with psycopg2.connect(p1edb_connect_request) as p1edb_conn:
        with p1edb_conn.cursor() as p1edb_cursor:
            # Possibly also treat start_dt and end_dt as interval borders here.
            # To do this, add min and max available msrmnt_dt_utc between start_dt and end_dt,
            # independent of the given max_interval_border.
            # This would need nested sql, so avoid for now.
            sql_query = sql.SQL(
                """SELECT COUNT(*)
                FROM measurement
                WHERE {start_dt} <= msrmnt_dt_utc AND msrmnt_dt_utc < {end_dt}
                """
            ).format(
                start_dt=sql.Literal(start_dt),
                end_dt=sql.Literal(end_dt),
            )
            p1edb_cursor.execute(sql_query)
            num_msrmnts = p1edb_cursor.fetchone()[0]
    end_query_dt = datetime.datetime.now()
    elapsed = (end_query_dt - start_query_dt).total_seconds()
    print("get_msrmnt_count_interval from {} to {} num_msrmnts {}, elapsed {}".format(
        start_dt, end_dt, num_msrmnts, elapsed))


def get_msrmnt_counts(interval_border, start_dt, end_dt):
    # return dt_value_pairs
    max_intervals = 100
    total_delta = end_dt - start_dt
    while True:
        delta = {
            IntervalBorder.YearUtc: one_year_delta,
            IntervalBorder.YearLocal: one_year_delta,
            IntervalBorder.MonthUtc: one_month_delta,
            IntervalBorder.MonthLocal: one_month_delta,
            IntervalBorder.DayUtc: one_day_delta,
            IntervalBorder.DayLocal: one_day_delta,
            IntervalBorder.Hour: one_hour_delta,
            IntervalBorder.Quarter: one_quarter_delta,
            IntervalBorder.Minute: one_minute_delta,
            IntervalBorder.TenSecond: ten_second_delta,
            IntervalBorder.Second: one_second_delta,
        }[interval_border]
        num_intervals = total_delta / delta
        print("get_msrmnt_counts num_intervals {} border {}".format(
            num_intervals, interval_border.name))
        if num_intervals <= max_intervals:
            break
        if interval_border.value <= IntervalBorder.YearUtc.value:
            break
        # try longer interval
        interval_border = IntervalBorder.from_value(interval_border.value - 1)

    res = []
    while start_dt < end_dt:
        next_end_dt = start_dt + delta
        res.append((start_dt, get_msrmnt_count_interval(start_dt, next_end_dt)))
        start_dt = next_end_dt
    return res


def plot_values(meter_readings, ax1, legends, scale, color, legend):
    # non cumulative meter reading values
    if not meter_readings:
        print("plot_values: no meter_readings for {}".format(legend))
        return
    plot_points = [(dt, value * scale) for (dt, value) in meter_readings]
    (dts, values) = zip(*plot_points)
    ax1.plot(dts, values, color=color, linewidth=0.8)
    legends.append(legend)


def vertical_lines(horizontal_intervals):
    vert_lines = []
    for (_prev_start_dt, _prev_end_dt, prev_value), (start_dt, _end_dt, value) in pairwise(horizontal_intervals):
        vert_lines.append((start_dt, prev_value, value))
    return vert_lines


def plot_decumul_values(meter_readings_cumul, ax1, legends, value_scale, color, legend):
    # meter reading values are cumulative
    if not meter_readings_cumul:
        print("plot_cumul_values: no meter_readings_cumul for {}".format(legend))
        return

    total_av_W = None
    if len(meter_readings_cumul) >= 2:
        # Report totals, compute avarages
        total_start_dt, total_start_val = meter_readings_cumul[0]
        total_end_dt, total_end_val = meter_readings_cumul[-1]
        total_secs = (total_end_dt - total_start_dt).total_seconds()
        total_val = float(total_end_val - total_start_val) * value_scale
        if total_secs > 0:
            total_av_kW = float(total_val / total_secs)
            total_av_W = round(total_av_kW * 1000)
            print("Plot avarage {} W {} from {} to {} UTC, total {} kWh".format(
                total_av_W, legend, total_start_dt, total_end_dt, round(total_val / 3600)))
            ax1.hlines((total_av_kW,), (total_start_dt,),
                       (total_end_dt,), color=color, linestyles='dotted')
            legends.append("_")  # no legend for avarages

    plot_lines = []
    for (prev_start_dt, prev_cumul_value), (start_dt, cumul_value) in pairwise(meter_readings_cumul):
        duration = (start_dt - prev_start_dt).total_seconds()
        value_period = float(cumul_value - prev_cumul_value) * \
            value_scale  # decumulate
        av_value = value_period / duration
        plot_lines.append((prev_start_dt, start_dt, av_value))

    if plot_lines:
        (start_dts, end_dts, av_values) = zip(*plot_lines)
        ax1.hlines(av_values, start_dts, end_dts, color=color)
        if total_av_W is None:
            legends.append(legend)
        elif legend == MeasurementType.NgNl_m3.name:
            legends.append("{} (avarage {} m3/day)".format(legend,
                           round(total_av_W * hours_per_day / kWh_per_m3) / 1000))
        else:
            legends.append("{} (avarage {} W)".format(legend, total_av_W))
        vert_lines = vertical_lines(plot_lines)
        if vert_lines:
            (start_dts, prev_values, values) = zip(*vert_lines)
            ax1.vlines(start_dts, prev_values, values,
                       color=color, linewidth=0.3)
            legends.append("_")  # no legend for vertical lines


def show_dt(dt):
    return " ".join(dt.isoformat().split("T"))


def plot_xy_info(value_name, value_unit, start_dt_utc, end_dt_utc, plt, ax1, legends, max_interval_border):
    plt.title("{} from P1 port, {} intervals".format(
        value_name, max_interval_border.name))
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        ax1.legend(legends, ncol=1+len(legends)//2, loc="best")
    ax1.grid(axis="y", linestyle="dotted")
    plt.ylabel(value_unit)
    ax1.xaxis.grid(linestyle="dotted")
    plt.xlabel("From " + show_dt(start_dt_utc) +
               " UTC to " + show_dt(end_dt_utc) + " UTC")

    ax2 = ax1.twinx()
    ax2.set_ylabel("m3/day")
    y1, y2 = ax1.get_ylim()
    yfac = hours_per_day / kWh_per_m3
    ax2.set_ylim(y1 * yfac, y2 * yfac)


def plot_meter_readings_types(combis_types_scales_colours, max_interval_border, start_dt, end_dt, ax1, legends):
    msrmnt_type_equipment_combis = [(msrmnt_type, equipment_ean_combi)
                                    for (equipment_ean_combi, msrmnt_type, scale, colour)
                                    in combis_types_scales_colours]
    meterreadings_by_type = get_meter_readings_by_types(
        msrmnt_type_equipment_combis, max_interval_border, start_dt, end_dt)

    for (equipment_ean_combi, msrmnt_type, scale, colour) in combis_types_scales_colours:
        if not msrmnt_type in meterreadings_by_type:
            print("plot_meter_readings_types: no meterreadings_by_type for", msrmnt_type)
            continue
        if msrmnt_type in CumulativeMeasurementTypes:
            value_plot_fn = plot_decumul_values
        else:
            value_plot_fn = plot_values
        value_plot_fn(
            meterreadings_by_type[msrmnt_type], ax1, legends, scale, colour, msrmnt_type.name)


def show_msmrnt_counts(max_interval_border, start_dt, end_dt, ax1, legends):
    counts = get_msrmnt_counts(max_interval_border, start_dt, end_dt)
    # plot_values(counts), ax1, legends, scale, colour, "counts")


def do_main():
    parser = argparse.ArgumentParser(description='Show meter plots.')
    parser.add_argument('-n', dest='non_blocking',
                        action="store_const", const=True, default=False,
                        help='close plots immediately')
    args = parser.parse_args()

    # max_interval_border = IntervalBorder.Second # not needed, interval between P1 telegrams is just over 10 secs.

    # show kWh quantization
    plot_last_days, plot_last_hours, max_interval_border = 0, 5, IntervalBorder.TenSecond
    # last days in minutes, some quantization
    # plot_last_days, plot_last_hours, max_interval_border = 1, None, IntervalBorder.Minute
    # last week in quarters
    plot_last_days, plot_last_hours, max_interval_border = 7, None, IntervalBorder.Quarter
    # last month in hours
    # plot_last_days, plot_last_hours, max_interval_border = 32, None, IntervalBorder.Hour
    # last year in days
    # plot_last_days, plot_last_hours, max_interval_border = 366, None, IntervalBorder.DayUtc
    # last 2 years in months
    # plot_last_days, plot_last_hours, max_interval_border = 510, None, IntervalBorder.MonthUtc

    end_dt = (datetime.datetime.utcnow() + datetime.timedelta(minutes=1)
              ).replace(second=0, microsecond=0)  # whole minute

    # fixed end date 2022 Oct 1
    # end_dt = datetime.datetime(year=2022, month=10, day=1, hour=0)
    # 2022 august + september
    # plot_last_days, plot_last_hours, max_interval_border = 62, None, IntervalBorder.DayUtc
    # 2022 september
    # plot_last_days, plot_last_hours, max_interval_border = 30, None, IntervalBorder.DayUtc

    # fixed end date 2022 Nov 1, local time
    # end_dt = datetime.datetime(year=2022, month=10, day=31, hour=23)
    # plot_last_days, plot_last_hours, max_interval_border = 31, 0, IntervalBorder.Hour  # 2022 october

    # fixed end date 2022 Dec 1, local time
    # end_dt = datetime.datetime(year=2022, month=11, day=30, hour=23)
    # 2022 November (exactly as billed)
    # plot_last_days, plot_last_hours, max_interval_border = 30, 0, IntervalBorder.DayLocal

    # fixed end date 24 April 2023, local time, one day, all values
    # end_dt = datetime.datetime(year=2023, month=4, day=23, hour=19)
    # plot_last_days, plot_last_hours, max_interval_border = 2, 20, IntervalBorder.TenSecond

    if plot_last_hours is not None:
        plot_total_time = datetime.timedelta(
            days=plot_last_days, hours=plot_last_hours)
        start_dt = end_dt - plot_total_time
    else:
        plot_total_time = datetime.timedelta(days=plot_last_days)
        start_dt = end_dt - plot_total_time
        start_dt = datetime.datetime(
            year=start_dt.year, month=start_dt.month, day=start_dt.day)  # whole days

    # blue, green, red, cyan, magenta, yellow, black, white, grey
    colours = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w', 'grey']

    legends = []

    el_equipment_ean_combi = 33  # CHECKME: retrieve by EAN ?
    gas_equipment_ean_combi = 35

    fig1, ax1 = plt.subplots()

    combis_types_scales_clrs = [
        (gas_equipment_ean_combi, MeasurementType.NgNl_m3,
         kWh_per_m3 * secs_per_hour, colours[2]),
        (el_equipment_ean_combi, MeasurementType.El_kWh_to,
         secs_per_hour, colours[0]),
        # El_kWh_by has many zero values, plot colour overwrites zeros mostly from NgNl_m3 above.
        (el_equipment_ean_combi, MeasurementType.El_kWh_by, \
         -secs_per_hour, colours[1]),
    ]
    if start_dt < datetime.datetime(year=2022, month=3, day=1):
        # El_kWh only available in jan and feb 2022, and only partially
        combis_types_scales_clrs.append(
            (el_equipment_ean_combi, MeasurementType.El_kWh, secs_per_hour, colours[4]))

    # interval of ten seconds or less, i.e. all P1 telegrams
    if max_interval_border.value >= IntervalBorder.TenSecond.value:
        combis_types_scales_clrs.extend((
            (el_equipment_ean_combi, MeasurementType.El_kW, 1, colours[6]),
            (el_equipment_ean_combi, MeasurementType.El_kW_to, 1, colours[5]),
            (el_equipment_ean_combi, MeasurementType.El_kW_by, -1, colours[5]),
        ))

    plot_meter_readings_types(
        combis_types_scales_clrs, max_interval_border, start_dt, end_dt, ax1, legends)

    # show_msmrnt_counts(max_interval_border, start_dt, end_dt, ax1, legends)

    plot_xy_info("Electricity and gas", "kW (dotted: avarages)",
                 start_dt, end_dt, plt, ax1, legends, max_interval_border)
    # see also matplotlib Figure.set_size_inches(w, h) or ...((w,h))
    fig1.set_figwidth(13.5)
    try:
        plt.show(block=not args.non_blocking)
    except KeyboardInterrupt:  # avoid traceback
        print("")  # end line with ^C


if __name__ == "__main__":
    import subprocess
    import sys
    try:
        do_main()
    except psycopg2.OperationalError:
        cmd = "/bin/bash -c /home/ype/gitrepos/enmprv/dev/p1edbtunnel.sh"
        print(cmd)
        cmd_process = subprocess.Popen(
            cmd.split(), stdout=sys.stdout, stderr=sys.stderr)
        cmd_process.wait()
        if cmd_process.returncode != 0:
            raise Exception("command {} non zero return code {}".format(
                cmd, cmd_process.returncode))
        plt.close("all")
        do_main()
