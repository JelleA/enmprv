# Compute 56 bits in hex for global and interface id in ipv6 local unique addresses
l = list(ord(c) for c in 'privacysamen')
lmin = min(l)
f = max(l) - lmin + 1
num = 0
for n in l:
    num = num * f + (n - lmin)
# num %= 2**40 # 40 bits instead of 56
r = ''
while num > 0:
    r = format(num % 16, 'x') + r
    num = num // 16
print(len(r) * 4, r)
