use std::io;
use std::iter::FromIterator;
use std::net::{TcpListener, TcpStream};
use std::sync::mpsc::{self, SyncSender};
use std::time::{Duration, Instant};
use std::{
    collections::{
        hash_map::Entry::{Occupied, Vacant},
        HashMap, HashSet,
    },
    thread,
};

use anyhow::{anyhow, Context, Error, Result};
use bincode::Options;
use enmplib_rs::util::RunState;
use log::{error, info, log, warn, Level};
use num::{pow, BigInt, FromPrimitive};
use polling::{Event, Poller};

use enmplib_rs::{
    bigintpowten::BigIntPowTen,
    bincoded::{
        get_bincode_options, slice_u8_to_string, EncKeyAddReply, EncKeyAddRequest,
        EncryptionResult, IncrementalRequest, MeasurementType, MeasurementTypeTotalEncKey,
    },
    encryptionseed::{encryption_key, SharedSeeds},
    openssl::OpenSslSigner,
    util::{
        addr_port_str, receive_version_size_message, send_version_size_message,
        time_stamp_milli_secs, CtrlC,
    },
};

use crate::{KeyAdderConfig, KeyAdderQueuedMessage};

fn receive_key_add_requests_for_interval(
    aggr_meters_tcp_stream: &mut TcpStream,
    read_timeout_seconds: u64,
    run_state: &RunState,
    key_add_sender: &mpsc::SyncSender<KeyAdderQueuedMessage>,
) -> Result<()> {
    let bincode_options = get_bincode_options();
    loop {
        let key_add_request: EncKeyAddRequest = {
            let buf: Vec<u8> =
                match receive_version_size_message(aggr_meters_tcp_stream, read_timeout_seconds) {
                    Err(e) => {
                        return if format!("{e:?}").contains("failed to fill whole buffer") {
                            Ok(())
                        } else {
                            Err(e.context("receive_version_size_message"))
                        };
                    },
                    Ok(b) => b,
                };
            bincode_options
                .deserialize::<EncKeyAddRequest>(&buf)
                .context("deserialize EncKeyAddRequest")?
        };

        key_add_sender
            .send(KeyAdderQueuedMessage::EncKeyAddRequest(key_add_request))
            .context("key_add_sender.send")?;

        run_state
            .is_running()
            .context("receive_key_add_requests_for_interval")?;
    }
}

pub(crate) fn connect_from_aggregator(
    config: &KeyAdderConfig,
    run_state: &RunState,
    key_add_sender: &SyncSender<KeyAdderQueuedMessage>,
) -> Result<()> {
    const AGGR_KEY: usize = 7;
    let local_addr_port = addr_port_str(
        &config.bind_address_aggrmeters,
        &config.serve_port_key_add.to_string(),
    )?;
    let accept_timeout_seconds = config.accept_timeout_seconds;
    let read_timeout_seconds = config.read_timeout_seconds;

    loop {
        run_state.is_running()?;
        let listener: TcpListener = match TcpListener::bind(local_addr_port.clone()) {
            Err(e) => {
                error!("{e:?} at bind local_addr_port: {local_addr_port}");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                continue; // next listener
            },
            Ok(l) => l,
        };
        if let Err(e) = listener.set_nonblocking(true) {
            error!("{e:?} at set_nonblocking listener {listener:?}");
            thread::sleep(Duration::from_secs(accept_timeout_seconds));
            continue; // next listener
        }
        let mut start_instant = Instant::now();

        let poller = match Poller::new() {
            Ok(p) => p,
            Err(e) => {
                error!("{e:?} at Poller::new");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                continue; // next listener
            },
        };

        // Set interest in the first events.
        if let Err(e) = poller.add(&listener, Event::readable(AGGR_KEY)) {
            error!("{e:?} poller.add()");
            thread::sleep(Duration::from_secs(accept_timeout_seconds));
            continue; // next listener
        }

        loop {
            let mut events = Vec::new();
            if let Err(e) = poller.wait(&mut events, Some(Duration::new(accept_timeout_seconds, 0)))
            {
                error!("{e:?} at poller.wait");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                break; // next listener
            }
            if events.is_empty() {
                error!("no event from {listener:?} via {poller:?}");
            } else {
                for ev in &events {
                    if ev.key != AGGR_KEY {
                        error!("poller.wait() unknown event key in {ev:?}");
                        thread::sleep(Duration::from_secs(accept_timeout_seconds));
                        break; // next listener
                    }
                    match listener.accept() {
                        Ok((mut interval_stream, _peer_addr)) => {
                            let interval_stream_clone = match interval_stream.try_clone() {
                                Ok(stream) => stream,
                                Err(e) => {
                                    return Err(Error::new(e).context("try_clone"));
                                },
                            };
                            match key_add_sender
                                .send(KeyAdderQueuedMessage::TcpStream(interval_stream_clone))
                            {
                                Ok(()) => {},
                                Err(e) => {
                                    error!("{e:?}");
                                },
                            }

                            if let Err(e) = receive_key_add_requests_for_interval(
                                &mut interval_stream,
                                read_timeout_seconds,
                                run_state,
                                key_add_sender,
                            ) {
                                if e.downcast_ref::<CtrlC>().is_some() {
                                    return Err(e);
                                }
                                log!(
                                    if let Some(ioe) = e.downcast_ref::<io::ErrorKind>() {
                                        match ioe {
                                            io::ErrorKind::UnexpectedEof => Level::Trace,
                                            _ => Level::Warn,
                                        }
                                    } else {
                                        Level::Warn
                                    },
                                    "{e:#}"
                                );
                            }
                        },
                        Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                            if start_instant.elapsed() > Duration::new(accept_timeout_seconds, 0) {
                                info!("time out on {listener:?}");
                                start_instant = Instant::now();
                            }
                        },
                        Err(e) => {
                            error!("listener.accept() {e:?}");
                            thread::sleep(Duration::from_secs(accept_timeout_seconds));
                            break; // next listener
                        },
                    }
                }
            }
            // Set interest in the next events.
            if let Err(e) = poller.modify(&listener, Event::readable(AGGR_KEY)) {
                error!("poller.modify() {e:?}");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                break; // next listener
            }
            run_state.is_running()?;
        }
    }
}

// For building up a MeasurementTypeTotalEncKey from partial results per eq_id/ms_dt for a single measurement type.
// This is initialized and maintained per aggregation interval
// The total_encryption_key is maintained also for ean's that are initially refused as long there are not enough
// ean's available in the interval.
// The enc_key_by_grid_conn_ean map is maintained to allow a later measurement to be replaced lateron
// in the same interval.
struct PartialEncKey {
    total_encryption_key: BigIntPowTen,
    total_measurements: u64,
    // maximum decimal digits encountered in encryption infos used in this interval.
    // used for formatting the encryption key in the reply.
    max_decimal_digits: u64,
    refused_grid_conn_ean_vec: Vec<Vec<u8>>, // Clear this when total ean's exceed the configured minimum.
    // Allow a later received msrmnt for the same grid connection ean to replace an earlier msrmnt:
    enc_key_by_grid_conn_ean: HashMap<Vec<u8>, BigIntPowTen>, // Last encryption key by grid connection ean, added into total_encryption_key,
}

// Aggregate encryption keys for an interval
struct IntervalKeysAggr {
    min_grid_connection_eans: u64,
    partial_enc_key_by_msrmnt_type: HashMap<MeasurementType, PartialEncKey>,
    grid_conn_eans_no_encryption_info: HashSet<Vec<u8>>,
}

impl IntervalKeysAggr {
    fn new(min_grid_connection_eans: u64) -> Self {
        IntervalKeysAggr {
            min_grid_connection_eans,
            partial_enc_key_by_msrmnt_type: HashMap::new(),
            grid_conn_eans_no_encryption_info: HashSet::new(),
        }
    }
    fn earlier_missing_encryption_info(&self, grid_ean: &Vec<u8>) -> bool {
        self.grid_conn_eans_no_encryption_info.contains(grid_ean)
    }

    fn missing_ean_encryption_info(&mut self, grid_ean: &Vec<u8>) {
        self.grid_conn_eans_no_encryption_info
            .insert(grid_ean.clone());
        // In this interval the grid_connection_ean may have had an earlier measurement with encryption info available,
        // Remove this ean/msrmnt_type from partial results for all msmrnt types
        for (msrmnt_type, partial_enc_key) in &mut self.partial_enc_key_by_msrmnt_type {
            if let Some(enc_key) = partial_enc_key.enc_key_by_grid_conn_ean.remove(grid_ean) {
                partial_enc_key.total_encryption_key -= enc_key;
                warn!(
                    "removing earlier encryption key for network ean {} and {msrmnt_type:?}",
                    slice_u8_to_string(grid_ean)
                );
            }
        }
    }

    fn add_encryption_key(
        &mut self,
        grid_ean: &[u8],
        msrmnt_type: MeasurementType,
        enc_key: BigIntPowTen,
        max_decimal_digits: u64,
    ) {
        match self.partial_enc_key_by_msrmnt_type.entry(msrmnt_type) {
            Vacant(entry) => {
                // first measurement of this type
                let partial_enc_key = PartialEncKey {
                    total_encryption_key: enc_key.clone(),
                    max_decimal_digits,
                    total_measurements: 1,
                    refused_grid_conn_ean_vec: if self.min_grid_connection_eans > 1 {
                        vec![grid_ean.to_vec()]
                    } else {
                        // no meter value privacy
                        Vec::new()
                    },
                    enc_key_by_grid_conn_ean: HashMap::from_iter([(grid_ean.to_vec(), enc_key)]),
                };
                entry.insert(partial_enc_key);
            },
            Occupied(mut entry) => {
                // later measurement of this type
                let partial_enc_key = entry.get_mut();
                match partial_enc_key
                    .enc_key_by_grid_conn_ean
                    .entry(grid_ean.to_vec())
                {
                    Vacant(entry) => {
                        // first measurement of this type for grid connection ean
                        entry.insert(enc_key.clone());
                        partial_enc_key.total_encryption_key += enc_key;
                        if max_decimal_digits > partial_enc_key.max_decimal_digits {
                            partial_enc_key.max_decimal_digits = max_decimal_digits;
                        }
                        partial_enc_key.total_measurements += 1;
                        if partial_enc_key.total_measurements < self.min_grid_connection_eans {
                            partial_enc_key
                                .refused_grid_conn_ean_vec
                                .push(grid_ean.to_vec());
                        } else {
                            partial_enc_key.refused_grid_conn_ean_vec.clear();
                        }
                    },
                    Occupied(mut entry) => {
                        // later received measurement of this type for this grid connection ean, discard earlier
                        let enc_key_eqid_ref = entry.get_mut();
                        let prev_enc_key = enc_key_eqid_ref.clone();
                        *enc_key_eqid_ref = enc_key.clone();
                        partial_enc_key.total_encryption_key -= prev_enc_key;
                        partial_enc_key.total_encryption_key += enc_key;
                        // same grid connection ean: leave partial_enc_key.total_measurements unchanged
                    },
                }
            },
        }
    }

    fn total_key_by_msrmnt_type(
        &self,
        msrmnt_type: MeasurementType,
        min_eq_ids_decrypt: u64,
    ) -> Option<MeasurementTypeTotalEncKey> {
        if let Some(partial_enc_key) = self.partial_enc_key_by_msrmnt_type.get(&msrmnt_type) {
            let total_encryption_key = if partial_enc_key.total_measurements < min_eq_ids_decrypt {
                info!(
                    "need measurements from at least {} grid connections, only {} available for {msrmnt_type:?}",
                    min_eq_ids_decrypt, partial_enc_key.total_measurements
                );
                EncryptionResult::PrivacyViolation
            } else {
                EncryptionResult::TotalEncryptionKey(
                    partial_enc_key
                        .total_encryption_key
                        .to_string(partial_enc_key.max_decimal_digits),
                )
            };

            for refused_ean in &partial_enc_key.refused_grid_conn_ean_vec {
                info!(
                    "refused grid connection ean {} for {msrmnt_type:?}",
                    slice_u8_to_string(refused_ean)
                );
            }

            Some(MeasurementTypeTotalEncKey {
                msrmnt_type,
                total_encryption_key,
                total_measurements: partial_enc_key.total_measurements,
                refused_grid_conn_ean_vec: partial_enc_key.refused_grid_conn_ean_vec.clone(),
            })
        } else {
            None
        }
    }
}

fn send_key_add_reply_for_interval(
    signer: &OpenSslSigner,
    config: &KeyAdderConfig,
    run_state: &RunState,
    key_add_receiver: &mpsc::Receiver<KeyAdderQueuedMessage>,
    shared_seeds: &mut SharedSeeds,
) -> Result<()> {
    let bincode_options = get_bincode_options();

    let Some(ten_bi) = BigInt::from_usize(10) else {
        return Err(anyhow!("BigInt::from_usize(10)"));
    };

    let mut aggr_meters_tcp_stream_opt: Option<TcpStream> = None; // use the last one from the queue.
    let mut key_add_request_opt: Option<EncKeyAddRequest> = None; // use the last one from the queue.

    let mut interval_keys_aggr = IntervalKeysAggr::new(config.min_grid_connection_eans_decrypt);

    let shared_seeds_keep_duration = config.shared_seeds_keep_duration.chrono_duration();

    loop {
        let request = key_add_receiver.recv().context("recv")?;

        match request {
            KeyAdderQueuedMessage::TcpStream(s) => {
                // for sending the key add reply
                aggr_meters_tcp_stream_opt = Some(s);
            },
            KeyAdderQueuedMessage::EncKeyAddRequest(kar) => {
                key_add_request_opt = Some(kar);
            },
            KeyAdderQueuedMessage::ShareSeedRequest(share_seed_request) => {
                let current_time_stamp = time_stamp_milli_secs();
                let keep_oldest_dt = current_time_stamp - shared_seeds_keep_duration;
                match shared_seeds.add_share_seed_request(
                    &share_seed_request,
                    &keep_oldest_dt,
                    &current_time_stamp,
                ) {
                    Ok(shared_seed_add_result) => {
                        info!("add_share_seed_request {shared_seed_add_result:?}");
                    },
                    Err(e) => {
                        error!("{e:#} at add_share_seed_request");
                    },
                }
            },
        }

        let Some(ref key_add_request) = key_add_request_opt else {
            continue;
        };

        // Check the measurement types in the key_add_request
        for gc_ean_meas_dt_types in &key_add_request.gc_ean_meas_dt_types_vec {
            let msrmnt_type_bits = &gc_ean_meas_dt_types.msrmnt_types_bit_vec;
            if !msrmnt_type_bits.any() {
                return Err(anyhow!(
                    "no requested measurement types in {msrmnt_type_bits:?}"
                ));
            }
            match msrmnt_type_bits.last_set_bit() {
                Some(pos) => {
                    if pos >= MeasurementType::VARIANT_COUNT {
                        return Err(anyhow!(
                        "requested measurement type for {pos} out of available range {}: {msrmnt_type_bits:?}",
                        MeasurementType::VARIANT_COUNT
                    ));
                    }
                },
                None => {
                    return Err(anyhow!(
                        "inconsistent: no last_set_bit() in {msrmnt_type_bits:?}"
                    ))
                },
            }
        }

        for gce_mt in &key_add_request.gc_ean_meas_dt_types_vec {
            match shared_seeds.get_encryption_info(&gce_mt.grid_connection_ean, &gce_mt.meas_dt) {
                Err(e) => {
                    warn!("{e:#}, no encryption info for {gce_mt}");
                    interval_keys_aggr.missing_ean_encryption_info(&gce_mt.grid_connection_ean);
                },
                Ok(encryption_info) => {
                    if interval_keys_aggr
                        .earlier_missing_encryption_info(&gce_mt.grid_connection_ean)
                    {
                        warn!(
                            "earlier measurement for {} had no encryption info available",
                            slice_u8_to_string(&gce_mt.grid_connection_ean)
                        );
                    } else {
                        let enc_modulo_digits =
                            match usize::try_from(encryption_info.ten_pow_modulus) {
                                Ok(d) => d,
                                Err(e) => {
                                    return Err(Error::new(e).context(format!(
                                        "usize::try_from {}",
                                        encryption_info.ten_pow_modulus
                                    )));
                                },
                            };

                        let encryption_modulus = pow(ten_bi.clone(), enc_modulo_digits);
                        let ten_pow_divisor = encryption_info.ten_pow_divisor;

                        let max_decimal_digits = u64::from(ten_pow_divisor);

                        let msrmnt_type_bits = &gce_mt.msrmnt_types_bit_vec;
                        for msrmnt_type in MeasurementType::iter() {
                            if msrmnt_type_bits.get_bit(*msrmnt_type)? {
                                let enc_key_bi = encryption_key(
                                    &encryption_info.seed,
                                    &gce_mt.grid_connection_ean,
                                    &gce_mt.meas_dt,
                                    *msrmnt_type,
                                    &encryption_modulus,
                                );
                                // divide by 10 ^ ten_pow_divisor as dependent on grid connection ean and meas_dt.
                                let minus_ten_pow_divisor = -i32::from(ten_pow_divisor); // cannot fail for u16
                                let enc_key = BigIntPowTen::new(enc_key_bi, minus_ten_pow_divisor);

                                interval_keys_aggr.add_encryption_key(
                                    &gce_mt.grid_connection_ean,
                                    *msrmnt_type,
                                    enc_key,
                                    max_decimal_digits,
                                );
                            }
                        }
                    }
                },
            }
        }

        match key_add_request.request_stage {
            IncrementalRequest::Partial => {
                run_state.is_running()?;
                continue;
            },
            IncrementalRequest::Final => break,
        }
    }

    // avoid HashMap random order
    let msrmnt_type_total_key_vec = MeasurementType::iter()
        .filter_map(|msrmnt_type| {
            interval_keys_aggr
                .total_key_by_msrmnt_type(*msrmnt_type, config.min_grid_connection_eans_decrypt)
        })
        .collect::<Vec<MeasurementTypeTotalEncKey>>();

    let mut eans_wo_ei =
        Vec::<Vec<u8>>::from_iter(interval_keys_aggr.grid_conn_eans_no_encryption_info);
    eans_wo_ei.sort();

    let key_add_reply = bincode_options
        .serialize(&EncKeyAddReply {
            msrmnt_type_total_key_vec: msrmnt_type_total_key_vec.clone(), // clone for reporting below
            grid_conn_eans_no_encryption_info: eans_wo_ei,
        })
        .context("serialize EncKeyAddReply")?;

    let Some(mut aggr_meters_tcp_stream) = aggr_meters_tcp_stream_opt else {
        return Err(anyhow!(
            "send_key_add_replies_for_interval: no tcp stream to answer"
        ));
    };

    // send key_add_reply before generating its signature: allow parallelism
    send_version_size_message(&key_add_reply, &mut aggr_meters_tcp_stream)
        .context("send_version_size_message key_add_reply")?;
    let signature = signer
        .generate_signature(&key_add_reply)
        .context("generate_signature")?;

    send_version_size_message(&signature, &mut aggr_meters_tcp_stream)
        .context("send_version_size_message signature")?;

    for mttk in &msrmnt_type_total_key_vec {
        match &mttk.total_encryption_key {
            EncryptionResult::PrivacyViolation => {
                info!(
                    "{} {:>10} msrmnt(s), privacy violation",
                    mttk.total_measurements,
                    mttk.msrmnt_type.to_str(),
                );
            },
            EncryptionResult::TotalEncryptionKey(decryption_key) => {
                info!(
                    "{} {:>10} msrmnts, decryption key {:>18}",
                    mttk.total_measurements,
                    mttk.msrmnt_type.to_str(),
                    decryption_key,
                );
            },
        }
    }
    Ok(())
}

pub(crate) fn send_key_add_replies_for_intervals(
    signer: &OpenSslSigner,
    config: &KeyAdderConfig,
    run_state: &RunState,
    key_add_receiver: &mpsc::Receiver<KeyAdderQueuedMessage>,
) -> Result<()> {
    let mut shared_seeds = SharedSeeds::new();
    loop {
        send_key_add_reply_for_interval(
            signer,
            config,
            run_state,
            key_add_receiver,
            &mut shared_seeds,
        )
        .context("send_key_add_reply_for_interval")?;
    }
}
