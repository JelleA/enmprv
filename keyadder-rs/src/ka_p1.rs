use std::collections::HashMap;
use std::net::{TcpListener, TcpStream};
use std::rc::Rc;
use std::sync::mpsc::SyncSender;
use std::time::{Duration, Instant};
use std::{io, thread};

use anyhow::{anyhow, Context, Result};
use bincode::Options;
use enmplib_rs::util::RunState;
use log::{error, info, log, trace, warn, Level};
use polling::{Event, Poller};

use enmplib_rs::{
    bincoded::{get_bincode_options, slice_u8_to_string, ShareSeedReply, ShareSeedRequest},
    openssl::{sig_verifier_by_grid_connection_ean_from_config, OpenSslSigVerifier, OpenSslSigner},
    util::{addr_port_str, receive_version_size_message, send_version_size_message},
};

use crate::{KeyAdderConfig, KeyAdderQueuedMessage};

pub(crate) fn listen_seed_share_requests(
    config: &KeyAdderConfig,
    key_adder_signer: &OpenSslSigner,
    run_state: &RunState,
    share_seed_sender: &SyncSender<KeyAdderQueuedMessage>,
) -> Result<()> {
    const P1_KEY: usize = 9;
    let sig_verifier_by_grid_connection_ean = sig_verifier_by_grid_connection_ean_from_config(
        "grid_connection_eans_by_public_keys",
        &config.grid_connection_eans_by_public_keys,
        &config.temp_dir,
    )?;

    let local_addr_port = addr_port_str(
        &config.bind_address_p1_encryptors,
        &config.serve_port_share_seed.to_string(),
    )?;
    let accept_timeout_seconds = config.accept_timeout_seconds;
    let read_timeout_seconds = config.read_timeout_seconds;
    loop {
        run_state.is_running()?;
        let listener: TcpListener = match TcpListener::bind(local_addr_port.clone()) {
            Err(e) => {
                error!("{e:?} at bind local_addr_port: {local_addr_port}");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                continue; // new listener
            },
            Ok(l) => l,
        };
        if let Err(e) = listener.set_nonblocking(true) {
            error!("{e:?} at set_nonblocking listener {listener:?}");
            thread::sleep(Duration::from_secs(accept_timeout_seconds));
            continue; // new listener
        }
        let mut start_instant = Instant::now();

        let poller = match Poller::new() {
            Ok(p) => p,
            Err(e) => {
                error!("{e:?} at Poller::new");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                continue; // new listener
            },
        };
        // CHECKME: merging this poller with the one in ka_aggr might save one thread.

        // Set interest in the first events.
        if let Err(e) = poller.add(&listener, Event::readable(P1_KEY)) {
            error!("{e:?} at poller.add");
            thread::sleep(Duration::from_secs(accept_timeout_seconds));
            continue; // new listener
        }

        loop {
            let mut events = Vec::new();
            if let Err(e) = poller.wait(&mut events, Some(Duration::new(accept_timeout_seconds, 0)))
            {
                error!("{e:?} at poller.wait");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                break; // new listener
            }
            if events.is_empty() {
                trace!("no event from {listener:?} via {poller:?}");
            } else {
                for ev in &events {
                    if ev.key != P1_KEY {
                        error!("poller.wait() unknown event key in {ev:?}");
                        thread::sleep(Duration::from_secs(accept_timeout_seconds));
                        break; // poll again.
                    }
                    match listener.accept() {
                        Ok((mut seed_share_stream, _peer_addr)) => {
                            if let Err(e) = serve_seed_share_requests(
                                &mut seed_share_stream,
                                read_timeout_seconds,
                                &sig_verifier_by_grid_connection_ean,
                                key_adder_signer,
                                run_state,
                                share_seed_sender,
                            ) {
                                let mut level: Level = Level::Warn;
                                for cause in e.chain() {
                                    if let Some(ioe) = cause.downcast_ref::<io::Error>() {
                                        if ioe.kind() == io::ErrorKind::UnexpectedEof {
                                            level = Level::Trace;
                                            break;
                                        }
                                    }
                                }
                                log!(level, "{e:#}");
                            }
                        },
                        Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                            if start_instant.elapsed() > Duration::new(accept_timeout_seconds, 0) {
                                info!("time out on {listener:?}");
                                start_instant = Instant::now();
                            }
                        },
                        Err(e) => {
                            error!("{e:?} at listener.accept");
                            thread::sleep(Duration::from_secs(accept_timeout_seconds));
                            break; // new listener
                        },
                    }
                }
            }
            // Set interest in the next events.
            // From the docs: interest in I/O events needs to be re-enabled using modify() again
            // after an event is delivered if we’re interested in the next event of the same kind.
            if let Err(e) = poller.modify(&listener, Event::readable(P1_KEY)) {
                error!("{e:?} at poller.modify");
                thread::sleep(Duration::from_secs(accept_timeout_seconds));
                break; // new listener
            }
            run_state.is_running()?;
        }
    }
}

fn serve_seed_share_requests(
    seed_share_stream: &mut TcpStream,
    read_timeout_seconds: u64,
    sig_verifier_by_grid_connection_ean: &HashMap<Vec<u8>, Rc<OpenSslSigVerifier>>,
    key_adder_signer: &OpenSslSigner,
    run_state: &RunState,
    share_seed_sender: &SyncSender<KeyAdderQueuedMessage>,
) -> Result<()> {
    let bincode_options = get_bincode_options();
    loop {
        let share_seed_request_res: Result<ShareSeedRequest, String> = {
            match receive_version_size_message(seed_share_stream, read_timeout_seconds) {
                Err(e) => {
                    for cause in e.chain() {
                        if let Some(ioe) = cause.downcast_ref::<io::Error>() {
                            if ioe.kind() == io::ErrorKind::UnexpectedEof {
                                // no more requests
                                return Ok(());
                            }
                        }
                    }
                    Err(format!("{e:?} at receive_version_size_message"))
                },
                Ok(seed_share_request_buf) => {
                    let signature =
                        receive_version_size_message(seed_share_stream, read_timeout_seconds)
                            .context("receive_version_size_message signature")?;

                    let seed_share_request = bincode_options
                        .deserialize::<ShareSeedRequest>(&seed_share_request_buf)
                        .context("deserialize seed_share_request_buf")?;

                    if seed_share_request.encryption_info.hash_function_name
                        != "blake2s256".as_bytes()
                    {
                        return Err(anyhow!(
                            "unknown hash_function {}",
                            slice_u8_to_string(
                                &seed_share_request.encryption_info.hash_function_name
                            )
                        ));
                    }
                    if seed_share_request.interval.not_before_dt_vec
                        > seed_share_request.interval.not_after_dt_vec
                    {
                        return Err(anyhow!(
                            "inconsistent interval {}",
                            seed_share_request.interval
                        ));
                    };

                    let sig_verifier = sig_verifier_by_grid_connection_ean
                        .get(&seed_share_request.grid_connection_ean)
                        .with_context(|| {
                            format!(
                                "no signature verifier for grid_connection_ean {}",
                                slice_u8_to_string(&seed_share_request.grid_connection_ean)
                            )
                        })?;

                    if sig_verifier
                        .verify_signature(&seed_share_request_buf, &signature)
                        .context("verify_signature")?
                    {
                        Ok(seed_share_request)
                    } else {
                        Err("verify_signature failed".to_string())
                    }
                },
            }
        };

        let share_seed_reply = ShareSeedReply {
            message: match share_seed_request_res {
                Err(e) => Some(format!("{e:#} in shared_seed_request_res")),
                Ok(share_seed_request) => {
                    trace!("received {share_seed_request}");
                    match share_seed_sender
                        .send(KeyAdderQueuedMessage::ShareSeedRequest(share_seed_request))
                    {
                        Err(e) => Some(format!(" {e:?} at SyncSender send(share_seed_request)")),
                        Ok(()) => None,
                    }
                },
            },
        };

        if let Some(ref mes) = share_seed_reply.message {
            warn!("shared_seed_reply.message {mes}");
        }

        match bincode_options.serialize::<ShareSeedReply>(&share_seed_reply) {
            Err(e) => error!("{e:?} at serialize shared_seed_reply"),
            Ok(buf) => {
                send_version_size_message(&buf, seed_share_stream)
                    .context("send_version_size_message shared_seed_reply")?;

                let sig_buf = key_adder_signer
                    .generate_signature(&buf)
                    .context("generate_signature")?;

                send_version_size_message(&sig_buf, seed_share_stream)
                    .context("send_version_size_message signature")?;
            },
        }

        run_state.is_running()?;
    }
}
