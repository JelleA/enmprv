use std::fs::File;
use std::io::Read;
use std::net::TcpStream;
use std::process::exit;
use std::sync::mpsc;
use std::sync::Arc;
use std::{env, thread};

use anyhow::Error;
use enmplib_rs::util::wait_clock_synchronized;
use enmplib_rs::util::RunState;
use log::{error, info, warn};
use serde::Deserialize;

use enmplib_rs::{
    bincoded::{EncKeyAddRequest, ShareSeedRequest},
    openssl::OpenSslSignerFile,
    util::{ip_addr_in_wg_intf, DurationConfig},
};

mod ka_aggr;
mod ka_p1;

enum KeyAdderQueuedMessage {
    TcpStream(TcpStream),
    EncKeyAddRequest(EncKeyAddRequest),
    ShareSeedRequest(ShareSeedRequest),
}

const CONFIG_FILE_NAME: &str = "keyadder.toml";
const LOG_CONFIG_FILE_NAME: &str = "keyaddlog4rs.yaml";

#[derive(Deserialize, Debug)]
pub(crate) struct KeyAdderConfig {
    wait_clock_sync: DurationConfig,
    min_grid_connection_eans_decrypt: u64,
    accept_timeout_seconds: u64,
    read_timeout_seconds: u64,
    sync_queue_size: u64,
    bind_address_aggrmeters: String,
    serve_port_key_add: u16,
    bind_address_p1_encryptors: String,
    serve_port_share_seed: u16,
    shared_seeds_keep_duration: DurationConfig,
    temp_dir: String,
    grid_connection_eans_by_public_keys: toml::Value,
    ssl_signer_file: OpenSslSignerFile,
}

fn usage_exit(exit_code: i32) -> ! {
    eprintln!("argument: -v");
    eprintln!("  show invocation and version number");
    eprintln!("arguments: [config_file [log_config_file]]");
    eprintln!("  config_file:     default {CONFIG_FILE_NAME}");
    eprintln!("  log_config_file: default {LOG_CONFIG_FILE_NAME}");
    exit(exit_code);
}

fn error_exit(e: Error) -> ! {
    error!("{e:#}, exiting");
    exit(1)
}

fn main() {
    let mut args = env::args();
    let binary_name = args.next().unwrap_or("key adder".to_string());
    let config_file_name = args.next().unwrap_or_else(|| CONFIG_FILE_NAME.to_string());
    let version = env!("CARGO_PKG_VERSION");
    if config_file_name == "-v" {
        println!("{binary_name} version {version}");
        exit(0);
    }
    if config_file_name == "-h" {
        usage_exit(0);
    }
    let log_config_file_name = args
        .next()
        .unwrap_or_else(|| LOG_CONFIG_FILE_NAME.to_string());
    if args.next().is_some() {
        usage_exit(1);
    }
    log4rs::init_file(
        &log_config_file_name,
        log4rs::config::Deserializers::default(),
    )
    .unwrap_or_else(|e| {
        eprintln!("{log_config_file_name}: {e:?} at log4rs::init_file , exiting",);
        exit(1);
    });
    info!(
        "starting key adder version {version} as {binary_name} user {} pid {}",
        std::env::var("USER").unwrap_or("".to_owned()),
        std::process::id()
    );

    let run_state = match RunState::new() {
        Err(e) => error_exit(e.context("RunState::new()")),
        Ok(r) => r,
    };

    let mut toml_buf = Vec::<u8>::new();
    {
        let mut config_file = File::open(&config_file_name).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(config_file_name.to_owned()));
        });
        let _ = config_file.read_to_end(&mut toml_buf).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(format!("read_to_end {config_file_name}")));
        });
    }
    let toml_str = std::str::from_utf8(&toml_buf).unwrap_or_else(|e| {
        error_exit(Error::new(e).context(config_file_name.to_owned()));
    });

    let config = Arc::new(
        toml::from_str::<KeyAdderConfig>(toml_str).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(format!("toml::from_str {config_file_name}")));
        }),
    );

    if let Err(e) = wait_clock_synchronized(config.wait_clock_sync.std_duration().as_secs()) {
        error_exit(e.context("wait_clock_synchronized"));
    }

    match ip_addr_in_wg_intf(&config.bind_address_aggrmeters) {
        Err(e) => error_exit(e.context(format!(
            "ip_addr_in_wg_intf ip interface {}",
            &config.bind_address_aggrmeters
        ))),
        Ok(res) => {
            if !res {
                warn!(
                    "no wireguard ip interface for {}",
                    &config.bind_address_aggrmeters
                );
            }
        },
    }

    match ip_addr_in_wg_intf(&config.bind_address_p1_encryptors) {
        Err(e) => error_exit(e.context(format!(
            "ip_addr_in_wg_intf ip interface {}",
            &config.bind_address_p1_encryptors
        ))),
        Ok(res) => {
            if !res {
                warn!(
                    "no wireguard ip interface for {}",
                    &config.bind_address_p1_encryptors
                );
            }
        },
    }

    let queue_size = config.sync_queue_size as usize;
    let (key_add_sender, key_add_receiver) =
        mpsc::sync_channel::<KeyAdderQueuedMessage>(queue_size);

    // start threads
    let signer = Arc::new(
        config
            .ssl_signer_file
            .open_ssl_signer(&config.temp_dir)
            .unwrap_or_else(|e| {
                error_exit(e.context("OpenSslSigner::new()"));
            }),
    );

    let signer_clone = Arc::clone(&signer);
    let config_clone = Arc::clone(&config);
    let run_state_clone = run_state.clone();
    let _key_add_reply_sender_thread = thread::spawn({
        move || loop {
            if let Err(e) = ka_aggr::send_key_add_replies_for_intervals(
                &signer_clone,
                &config_clone,
                &run_state_clone,
                &key_add_receiver,
            ) {
                error!("send_key_add_replies_for_intervals() {e:?}");
            }
        }
    });

    let share_seed_sender = key_add_sender.clone(); // extra queue producer

    let run_state_clone = run_state.clone();
    let config_clone = Arc::clone(&config);
    let _share_seed_thread = thread::spawn({
        move || match ka_p1::listen_seed_share_requests(
            &config_clone,
            &signer,
            &run_state_clone,
            &share_seed_sender,
        ) {
            Err(e) => error_exit(e.context("listen_seed_share_requests")),
            Ok(()) => {
                error!("serve_seed_share_requests thread stopped");
            },
        }
    });

    match ka_aggr::connect_from_aggregator(&config, &run_state, &key_add_sender) {
        Ok(()) => {
            info!("connect_from_aggregator returned, exiting");
        },
        Err(e) => {
            error_exit(e.context("connect_from_aggregator"));
        },
    }
}
