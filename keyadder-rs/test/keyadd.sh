#! /bin/bash -ev
set -u 
# Run the key adder for development testing
cd ~/gitrepos/enmprv
clear
cargo clippy
cargo build --package keyadder-rs
# reconnect with p1encryptors and aggregator by using  wg tunnels endpoints
ping -c 2 -w 10 10.1.0.3
ping -c 2 -w 10 10.1.0.5
time cargo run --package keyadder-rs keyadder-rs/keyadder.toml keyadder-rs/keyaddlog4rs.yaml
