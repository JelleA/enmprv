#! /bin/bash -ev
set -u
# Copy a p1encryptor program from one remote device to another, and run the program on the target.

# Remote p1encryptors as ssh hosts:
from_host=raspy2h # development machine with a.o. a release binary of the p1encrypt-rs program.
to_host=raspy6 # reachable from from_host as sudoer, since rsync cannot work with two remote hosts.
# to_host is also used in openssl key file names, log file names and on log lines.

# Config that varies with to_host:
grid_connection_ean_el='90163456789523456789'
grid_connection_ean_gas='90263456789523456789'
to_host_wg_address='10.1.0.6' # for bind_address_aggr_meters p1e config
p1_terminal="/dev/ttyUSB0" # to receive p1 telegrams on to_host, 

ssl_key_size=2048 # bit size for generated openssl rsa keys, use 3072 from 2030.

# The target device still needs:
# - a user mmenc (measurements encryptor) to run the p1encrypt-rs program.
# - wireguard tunnel(s) to a key adder and an aggregator,
# - a openssl rsa key pair to sign its measurements,
#   and the key adder and aggregator should have this public key added to their configs.

installmode=0
uninstallmode=0
runmode=0
if [ "$#" = 0 ]; then
    installmode=1
elif [ "$1" = 'u' ]; then
    uninstallmode=1 # uninstall all
elif [ "$1" = 'r' ]; then
    runmode=1
elif [ "$1" != '' ]; then
    echo "$0: unknown argument $1"
    exit 1
fi

# Via from_host, create user mmenc on to_host.
# Use:
# sudo adduser mmenc
# avoid: sudo adduser mmenc sudo, i.e. do not add user mmenc to the sudo group.
# sudo adduser mmenc dialout # allow access to /dev/ttyUSB*, otherwise sudo needed for ssty, or
# sudo adduser mmenc tty # inc case /dev/ttyUSB* is in group tty.
mmenc_user="mmenc" # measurements encryptor user, will run the p1encrypt-rs program, 
# should be in the same user group as p1_terminal

wg_intf="wgenmprv0"

if [[ $uninstallmode -eq 1 ]]; then
    echo Killing processes of user $mmenc_user
    ssh $from_host ssh $to_host sudo killall -SIGKILL --user $mmenc_user || true
    # ssh $from_host ssh $to_host sudo deluser $mmenc_user
    # --remove-all-files takes some time.
    echo Removing user $mmenc_user
    ssh $from_host ssh $to_host sudo deluser --remove-all-files $mmenc_user
    # check for running wg interface:
    wg_link_line=$( (ssh $from_host ssh $to_host ip -brief link show | grep $wg_intf) || true)
    if [[ $wg_link_line != '' ]]; then
        # delete the wg interface
        cmd="ssh $from_host ssh $to_host sudo ip link delete $wg_intf"
        echo $cmd
        $cmd
    fi
    # FIXME: delete the wgconf directory of the sudo user on to_host
    # FIXME: delele the to_host public key file in keys directory on localhost
    exit
fi

# files to be copied from home dir of ssh user at from_host
from_working_space_dir="enmprv-rel" # in $from_host home dir
p1e_binary_file_name="p1encrypt-rs"
p1e_binary_path="$from_working_space_dir/target/release/$p1e_binary_file_name"

p1e_config_name="p1encrypt.toml" 
p1e_config_path="$from_working_space_dir/$p1e_config_name" 

keys_dir="keys" # in home dir

# on to_host, config info
private_key_file_name="${to_host}-private${ssl_key_size}.pem"
private_key_path="$keys_dir/$private_key_file_name"
public_key_file="$keys_dir/${to_host}-public${ssl_key_size}.pem"
# aggr_public_key_file="$keys_dir/aggr-public${ssl_key_size}.pem" # unused
# keyadd_public_key_file_name="$keys_dir/keyadd-public${ssl_key_size}.pem"

# take the log config file from the from_host git repo of the rust working space:
log_config_file_name="p1elog4rs.yaml"
log_config_path="$from_working_space_dir/p1encrypt-rs/$log_config_file_name"
log_dir="log" # in home directory of mmenc_user

# on to_host, in suduer user home via ssh $from_host ssh $to_host
wgconf_dir="wgconf" # in sudouser user home
wg_config_file_name="$wg_intf.conf"
wg_pubkey_file_name="wgpubkey"

target_dir="p1encrypt-rs" # install binary and config files, run from there.

if [[ $installmode -eq 1 ]]; then
    # Check for existing $mmenc_user with working ssh access:
    mmenc_home_dir=$(ssh $from_host ssh $mmenc_user@$to_host pwd || true)
    if [[ $mmenc_home_dir == '' ]]; then
        echo Home dir for user $mmenc_user at @to_host does not exist.
        # Create user mmenc in group mmenc:
        # - no login, i.e. need su or sudo to run as mmenc_user. 
        # - create a home directory for mmenc_user : /home/mmenc.
        # no tty input to adduser by redirecting from /dev/null.
        # ignore exit 1 when user already exists by suffix || true.
        (ssh $from_host ssh $to_host sudo adduser --disabled-login $mmenc_user < /dev/null) || true
        # Get the group of /dev/ttyUSB0 on to_host :
        ls_out=($(ssh $from_host ssh $to_host ls -l $p1_terminal))
        tty_group=${ls_out[3]}
        echo $p1_terminal tty_group: "$tty_group"
        # add to group to read from p1_terminal, this also creates a group with the same name
        ssh $from_host ssh $to_host sudo adduser $mmenc_user "$tty_group"

        # Allow user mmenc to ssh login by public key on to_host from from_host:
        # create the .ssh directory on $to_host with mode 700
        (ssh $from_host ssh $to_host sudo "mkdir --mode=700 --parents /home/$mmenc_user/.ssh" ) || true
        ssh $from_host ssh $to_host sudo "chown $mmenc_user /home/$mmenc_user/.ssh"
        ssh $from_host ssh $to_host sudo "chgrp $mmenc_user /home/$mmenc_user/.ssh"
        ssh $from_host rsync .ssh/id_rsa.pub $to_host: # into home of ssh to_host user
        ssh $from_host ssh $to_host sudo mv id_rsa.pub /home/$mmenc_user/.ssh/authorized_keys
        ssh $from_host ssh $to_host sudo "chown $mmenc_user /home/$mmenc_user/.ssh/authorized_keys"
        ssh $from_host ssh $to_host sudo "chgrp $mmenc_user /home/$mmenc_user/.ssh/authorized_keys"
        ssh $from_host ssh $mmenc_user@$to_host ls -l .ssh/authorized_keys

        # create default mode directories on to_host in mmenc home for mmenc user
        for dir in \
            $target_dir \
            $log_dir
        do
            echo Create dir $dir on $to_host for $mmenc_user
            (ssh $from_host ssh $mmenc_user@$to_host /usr/bin/mkdir --parents $dir ) || true
        done
        # create mode 700 directories on to_host in mmenc home for mmenc user
        for dir in \
            $keys_dir
        do
            echo Create dir $dir on $to_host for $mmenc_user with mode 700
            (ssh $from_host ssh $mmenc_user@$to_host /usr/bin/mkdir --mode=700 --parents $dir ) || true
        done
        # create mode 700 directories on to_host in mmenc home for default ssh user:
        for dir in \
            $wgconf_dir
        do
            echo Create dir $dir on $to_host with mode 700
            (ssh $from_host ssh $to_host /usr/bin/mkdir --mode=700 --parents $dir ) || true
        done
    fi

    # update p1e_config_path for to_host
    tmp_p1e_config_path=/tmp/$p1e_config_name # same file name to ease rsync below.
    sed_script_path=/tmp/sed_script
    ssh $from_host "cat - > $sed_script_path" << SED_SCRIPT_EOF1
{
    # use the to_host grid connection eans
    /grid_connection_ean_el/s/=.*/= \'$grid_connection_ean_el\'/
    /grid_connection_ean_gas/s/=.*/= \'$grid_connection_ean_gas\'/
    # to_host udp tunnel endpoint to aggregator:
    /bind_address_aggr_meters/s/=.*/= \'$to_host_wg_address\'/
    # replace the private key file name with the one generated here
    s/p1encrypt-private2048.pem/$private_key_file_name/
    # remove the line for private key passphrase
    /key_pass_phrase/d 
    # remove all lines from the [p1_split] too the next empty line,
    # no programmatic telegram splitting on to_host:
    /\[p1_split\]/,//d
}
SED_SCRIPT_EOF1

    echo Sed script $sed_script_path:
    ssh $from_host cat $sed_script_path
    ssh $from_host "sed -f $sed_script_path $p1e_config_path > $tmp_p1e_config_path"
    echo Patched config file $tmp_p1e_config_path:
    ssh $from_host cat $tmp_p1e_config_path

    # Patch the log config file on from_host to use the correct log file paths and host name
    ssh $from_host "cat - > $sed_script_path" << SED_SCRIPT_EOF2
{
    s/p1enc-user/$mmenc_user/g
    s/p1enc-host/$to_host/g
}
SED_SCRIPT_EOF2

    tmp_log_config_file=/tmp/$log_config_file_name # same file name to ease rsync below.
    ssh $from_host "sed -f $sed_script_path $log_config_path > $tmp_log_config_file"
    echo Patched log config file $tmp_log_config_file:
    ssh $from_host cat $tmp_log_config_file

    install_files=("$p1e_binary_path" "$tmp_p1e_config_path" "$tmp_log_config_file")
    echo install_files: "${install_files[@]}"
    rsyncopts=(--verbose --times) # avoid --relative to ignore source directory.
    time ssh $from_host rsync "${rsyncopts[@]}" "${install_files[@]}" $mmenc_user@$to_host":"$target_dir
    ssh $from_host ssh $mmenc_user@$to_host ls -la $target_dir

    # generate openssl keys on to_host, and copy back the public key to local host:
    if test ! -f "$HOME/$public_key_file"; then
        # generate private and public openssl keys on to_host
        gen_private_key_command="openssl genrsa -out $private_key_path ${ssl_key_size}"
        cmd="ssh $from_host ssh $mmenc_user@$to_host time $gen_private_key_command"
        echo $cmd
        $cmd
        gen_public_key_command="openssl rsa -in $private_key_path -pubout -out $public_key_file"
        cmd="ssh $from_host ssh $mmenc_user@$to_host $gen_public_key_command"
        echo $cmd
        $cmd

        # copy public key via from_host to the local machine (that runs aggregator and key adder):
        from_host_home=$(ssh $from_host pwd)
        cmd="ssh $from_host rsync --relative --times $mmenc_user@$to_host:$public_key_file $from_host_home"
        echo $cmd
        $cmd
        ssh $from_host ls -la $from_host_home/$public_key_file

        cmd="rsync --relative --times $from_host:$public_key_file $HOME"
        echo $cmd
        $cmd
        ls -lt $HOME/$public_key_file
    fi

    # copy public key of key adder from from_host to to_host, suppress this for raspy6.
    # cmd="ssh $from_host rsync --relative --times $keyadd_public_key_file_name $mmenc_user@$to_host:"
    # echo $cmd
    # $cmd
    # ssh $from_host ssh $mmenc_user@$to_host ls -lt $keys_dir

fi

if [[ $runmode -eq 1 ]]; then
    # install wireguard on to_host
    which_wg=$(ssh $from_host ssh $mmenc_user@$to_host which wg || true)
    if [[ "$which_wg" == '' ]]; then
        echo Installing wireguard on $to_host
        ssh $to_host sudo apt --assume-yes install wireguard
    else
        echo wireguard already installed on $to_host $which_wg
    fi

    wg_conf_to_host_path=$(ssh $from_host ssh $to_host ls $wgconf_dir/$wg_config_file_name || true)
    echo wg_conf_to_host_path $wg_conf_to_host_path

    wg_listen_port="51289" # on to_host

    if [[ $wg_conf_to_host_path == '' ]]; then
        # generate wireguard config on to_host
        wg_priv_key=$(ssh $from_host ssh $mmenc_user@$to_host wg genkey) # FIXME: this should run on to_host
        ssh $from_host ssh $to_host " \" echo $wg_priv_key | wg pubkey > $wgconf_dir/$wg_pubkey_file_name \" "
        # get the first public key at from_host, which is an existing peer of localhost:
        local_host_wg_public_key=$(ssh $from_host cat wgconf/wgenmprv0.conf \
            | grep "PublicKey =" \
            | head -1 \
            | cut --delimiter=' ' --fields=3)
        if [[ $local_host_wg_public_key == '' ]]; then
            echo No local_host_wg_public_key found at $from_host in wgconf/wgenmprv0.conf
            exit 1
        fi
        echo local_host_wg_public_key $local_host_wg_public_key
        local_host_wg_address="10.1.0.1"
        # create temp wg config file locally:
        echo "[Interface]
Address = $to_host_wg_address/32
PrivateKey = $wg_priv_key
ListenPort = $wg_listen_port
[Peer]
PublicKey = $local_host_wg_public_key
AllowedIps= $local_host_wg_address/32
" > /tmp/$wg_config_file_name
        echo /tmp/$wg_config_file_name
        cat /tmp/$wg_config_file_name
        # copy wg config to from_host, FIXME: use /tmp subdir on from_host for this.
        cmd="rsync /tmp/$wg_config_file_name $from_host:$wg_config_file_name"
        echo $cmd
        $cmd
        # and to to_host
        cmd="ssh $from_host rsync $wg_config_file_name $to_host:$wgconf_dir/$wg_config_file_name"
        echo $cmd
        $cmd
        # Remove intermediate files containing to_host wg private key:
        rm /tmp/$wg_config_file_name
        ssh $from_host rm $wg_config_file_name
    fi
    cmd="ssh $from_host ssh $to_host cat $wgconf_dir/$wg_config_file_name"
    echo $cmd
    $cmd

    # setup the wg tunnel on to_host to connect to the localhost v110:
    # check for running wg interface:
    wg_link_line=$( (ssh $from_host ssh $to_host ip -brief link show | grep $wg_intf) || true)
    if [[ $wg_link_line != '' ]]; then
        # delete the interface
        cmd="ssh $from_host ssh $to_host sudo ip link delete $wg_intf"
        echo $cmd
        $cmd
    fi
    # start the wg interface until shutdown
    cmd="ssh $from_host ssh $to_host \" ( cd $wgconf_dir; sudo wg-quick up ./$wg_config_file_name ) \" "
    echo $cmd
    $cmd

    # FIXME: use $wg_pubkey_file_name public key to add a wg peer to localhost.

    # run on target in foreground, via ssh -t terminals:
    td=$target_dir
    cmd="ssh -t $from_host ssh -t $mmenc_user@$to_host $td/$p1e_binary_file_name $td/$p1e_config_name $td/$log_config_file_name"
    echo $cmd
    $cmd
fi

# FIXME: enable the wg tunnel as systemctl service on $to_host

# FIXME: run on $to_host as systemctl service
