#! /bin/bash -ev
set -u

# On a remote p1encryptor, control:
# - building, testing and running a development version, and
# - building and running a release version from a git repo on the remote p1encryptor, and 
# - enabling and disabling the release version as systemctl service.
#
# When disabling the remote systemctl service:
# - stop the service and disable it for next reboot.
# Otherwise, the target directories for rsync copying should exist:
# - copy rust code for p1encrypt-rs, and some test configuration, from the local source workspace
#   into the remote workspace ~/enmprv/ on the target raspberry
# - cargo check on the target
# - copy back the Cargo.lock file
# - cargo clippy
# Finally, one of:
# - build debug target, and cargo run or test
# - build and cargo run the release target
# - start and enable the release build target as systemd service


# Function chosen by 1st arg:
# By default do a normal run without the tests.
testmode=0 # cargo test
releasemode=0 # cargo build --release, and run this in foreground.
enablemode=0 # cargo build --release, and systemctl start/enable.
disablemode=0 # systemctl stop/disable
statusmode=0 # show systemctl status
if [ "$#" == 0 ]; then
    echo -n '' 
elif [ "$1" = 'test' ]; then
    testmode=1 # run the rust tests.
elif [ "$1" = 'release' ]; then
    releasemode=1 # build a release, and run the release binary in foreground.
elif [ "$1" = 'enable' ]; then
    enablemode=1 # systemctl
elif [ "$1" = 'disable' ]; then
    disablemode=1 # systemctl
elif [ "$1" = 'status' ]; then
    statusmode=1 # systemctl
elif [ "$1" != '' ]; then
    echo "$0: unknown argument $1"
    exit 1
fi

release_tag="v0.1.15" # check out on branch master, for a release build on remote p1encryptor

# Remote p1encryptor as ssh host:
#raspy=raspy2
raspy=raspy2h
#raspy=p1e3

ssh_sudo_args=(-t "$raspy" sudo --prompt=sudo-passwd-"$raspy":)

service_name="p1encrypt-rs" # prefix of .service file
# pipe systemctl status through cat to avoid systemctl using less on the terminal stdout
systemctl_status_cmd="systemctl status $service_name | cat"

if [[ $statusmode -eq 1 ]]; then
    # show full systemctl status
    ssh "${ssh_sudo_args[@]}" "$systemctl_status_cmd"
    exit
fi


if [[ $disablemode -eq 1 ]]; then
    # Check for a remote running p1encrypt-rs process:
    remote_binary="p1encrypt-rs" # binary name showing up in ps ax, as started by systemctl
    grep_exit=0
    (ssh $raspy "ps ax" | grep $remote_binary) || grep_exit=$?
    # show full systemctl status anyway
    ssh "${ssh_sudo_args[@]}" "$systemctl_status_cmd"
    if [[ $grep_exit -ne 0 ]]; then
        # grep exit non zero, no process running
        echo "no $remote_binary remote process running to be disabled"
        exit 1
    fi
    echo "Stop $service_name service"
    ssh "${ssh_sudo_args[@]}" systemctl stop $service_name
    echo "Do not start $service_name at next boot"
    ssh "${ssh_sudo_args[@]}" systemctl disable $service_name
    ssh "${ssh_sudo_args[@]}" "$systemctl_status_cmd"
    exit
fi

# Local source code repository directory:
local_workspace=~/gitrepos/enmprv
rsyncopts=(--relative --times)
target_workspace=enmprv # in remote home directory
release_repo_dir=enmprv-rel # in remote home directory

service_dir=$service_name # in remote home directory, for running as systemctl service.

# Create remote directories for rust workspace, openssl keys and wg tunnel configs:
for dir in \
    $target_workspace/p1encrypt-rs/src \
    $target_workspace/enmplib-rs/src \
    keys \
    wgtest \
    $service_dir
do
    (ssh $raspy "mkdir --parents $dir" ) || true
done

builddevmode=0
if [[ $enablemode -eq 0 ]]; then
    if [[ $releasemode -eq 0 ]]; then
        builddevmode=1
    fi
fi

database_url="postgres:///p1edb" # for development compilation and for test runs.
p1e_config_file="p1encrypt.toml"

if [[ $builddevmode -eq 1 ]]; then
    # Copy development files, system service files, and rust sources to raspy
    cd $local_workspace/p1encrypt-rs
    rsync "${rsyncopts[@]}" $p1e_config_file p1etestlog4rs.yaml "$raspy":"$target_workspace"
    # copy Cargo.toml and Cargo.lock to the remote p1encrypt-rs package:
    rsync "${rsyncopts[@]}" Cargo.toml Cargo.lock "$raspy":"$target_workspace"/p1encrypt-rs
    # workspace file for the remote package p1encrypt-rs, using ../enmplib-rs:
    rsync "${rsyncopts[@]}" p1encrypt-rs.Cargo.toml "$raspy":"$target_workspace"/Cargo.toml
    # rust sources for p1encrypt-rs
    cd $local_workspace/p1encrypt-rs/src
    rust_files=(main.rs p1input.rs p1db.rs p1split.rs p1aggr.rs p1ka.rs)
    rsync "${rsyncopts[@]}" "${rust_files[@]}" "$raspy":"$target_workspace"/p1encrypt-rs/src
    # Cargo.toml for enmplib-rs
    cd $local_workspace/enmplib-rs
    rsync "${rsyncopts[@]}" Cargo.toml "$raspy":"$target_workspace"/enmplib-rs
    # rust sources for enmplib-rs
    cd $local_workspace/enmplib-rs/src
    rust_enmplib_files=(lib.rs bigintpowten.rs bincoded.rs encryptionseed.rs measurement.rs openssl.rs util.rs)
    rsync "${rsyncopts[@]}" "${rust_enmplib_files[@]}" "$raspy":"$target_workspace"/enmplib-rs/src

    # Copy openssl keys to raspy
    cd ~/keys
    # allow aggrmeters to verify p1 datagrams, and verify key adder replies to seed share requests:
    key_files=(p1encrypt-private2048.pem keyadd-public2048.pem)
    rsync "${rsyncopts[@]}" "${key_files[@]}" "$raspy":keys

    # Copy wireguard tunnel config to raspy
    cd ~/wgtest
    if [[ $raspy == raspy || $raspy == raspy2 ]]; then
        rsync "${rsyncopts[@]}" wg1/wg1.conf "$raspy":wgtest
        rsync "${rsyncopts[@]}" demowg1/demowg1.conf "$raspy":wgtest
    elif [[ $raspy == p1e3 ]]; then
        rsync "${rsyncopts[@]}" wg3/wg3.conf "$raspy":wgtest
    fi

    # cargo check step:
    if [ $testmode = 1 ]; then
        cargo_args="check --tests"
    else
        cargo_args="check"
    fi
    echo "$cargo_args"
    time ssh -t $raspy "(cd $target_workspace; DATABASE_URL=$database_url cargo $cargo_args)"

    # Cargo.lock may have changed on the raspberry, get it back to the local repo, with another name
    cd $local_workspace
    rsync --times "$raspy":"$target_workspace"/Cargo.lock p1encrypt-rs.Cargo.lock

    # cargo clippy step
    if [ $testmode = 1 ]; then
        cargo_args="clippy --tests"
    else
        cargo_args="clippy"
    fi
    echo "$cargo_args"
    time ssh -t $raspy "(cd $target_workspace; DATABASE_URL=$database_url cargo $cargo_args)"

fi # not enabling system service

# cargo test step
if [[ $testmode -eq 1 ]]; then
    ssh -t $raspy "(cd $target_workspace; DATABASE_URL=$database_url cargo test --no-run)"
    ssh -t $raspy "(cd $target_workspace; cargo test -- --nocapture)"
    exit
fi

raspy_user=$(ssh $raspy whoami)
echo User at $raspy is "$raspy_user"

release_build_target="target/release/p1encrypt-rs" # in $source_dir on $raspy

if [[ $enablemode -eq 0 ]]; then
    # development run, use debug or release target.

    # set terminal to incorrect parity for testing parity without reboot:
    # ssh $raspy stty -F /dev/ttyUSB0 -evenp

    # build p1encrypt-rs on raspy, and prepare cargo run command:
    if [[ $releasemode -eq 0 ]]; then
        ssh -t $raspy "(cd $target_workspace;DATABASE_URL=$database_url cargo build)"
        cargo_run_cmd=run
        source_dir=$target_workspace
    else
        # check for presence of enmprv-rel git repo on raspy
        # build/run release from release_repo_dir
        dir_present=$(ssh $raspy ls -d $release_repo_dir || true)
        repo_url="https://gitlab.com/enmprivaggr/enmprv.git" # reading only
        if [[ $dir_present == '' ]]; then
           echo $release_repo_dir on $raspy
           ssh $raspy git clone $repo_url $release_repo_dir
        fi
        echo Checking out $release_tag, should be at master branch.
        ssh $raspy "(cd $release_repo_dir; \
                     git fetch --all; \
                     git checkout master; \
                     git pull $repo_url master; \
                     git pull $repo_url $release_tag; \
                     git checkout $release_tag)"
        # release: clippy and build without database_url
        ssh -t $raspy "(cd $release_repo_dir; cargo clippy)"
        ssh -t $raspy "(cd $release_repo_dir; cargo build --release)"
        cargo_run_cmd="run --bin p1encrypt-rs --release"
        source_dir=$release_repo_dir
    fi

    log_config_name=p1elog4rs.yaml # foreground log config
    # in the log config change the host name marker p1enc-host to $raspy
    tmp_log_config_file=/tmp/$log_config_name
    cd $local_workspace/p1encrypt-rs
    sed --expression="{
        s/p1enc-user/$raspy_user/g
        s/p1enc-host/$raspy/g
    }" $log_config_name > $tmp_log_config_file
    # Copy test run and log config files to $raspy, avoid rsync --relative
    rsync --times $p1e_config_file $tmp_log_config_file "$raspy":"$source_dir"
    cargo_args="$cargo_run_cmd $p1e_config_file $log_config_name"
    cargo_cmd="DATABASE_URL=$database_url cargo $cargo_args"
    echo "$source_dir $cargo_cmd"
    ssh -t $raspy "(cd $source_dir; $cargo_cmd )"
    exit
fi

# enablemode=1

remote_home=$(ssh $raspy pwd)
echo remote_home "$remote_home"

echo "Check existence of release binary $release_repo_dir/$release_build_target"
ssh $raspy ls -l "$release_repo_dir/$release_build_target"

# link the release target into service_dir
ssh $raspy ln --force "$release_repo_dir/$release_build_target" "$remote_home/$service_dir"

cd $local_workspace/p1encrypt-rs
# copy the .service, the log config file and the p1e config file into service_dir:
p1e_service_name="$service_name".service
tmp_p1e_service_file=/tmp/$p1e_service_name
sed --expression="{
    s/p1enc-user/$raspy_user/g
}" $p1e_service_name > $tmp_p1e_service_file

log_config_name=p1ebglog4rs.yaml # background log config
tmp_log_config_file=/tmp/$log_config_name
# replace host name marker with $raspy in log config file:
sed --expression="{
    s/p1enc-user/$raspy_user/g
    s/p1enc-host/$raspy/g
}" $log_config_name > $tmp_log_config_file

p1e_service_start_files=(
    "$tmp_p1e_service_file"
    "$tmp_log_config_file"
    "$p1e_config_file"
)
# avoid rsync --relative because of $tmp_log_config_file:
rsync --times "${p1e_service_start_files[@]}" "$raspy":"$service_dir"
ssh $raspy ls -lt $service_dir

# Start as systemctl service:
ssh $raspy ls -lt "$target_workspace"
# remote symbolic link from /etc/systemd/system/ to absolute path service file:
remote_service_file="$remote_home"/"$service_dir"/"$p1e_service_name"

ssh "${ssh_sudo_args[@]}" ln --symbolic --force "$remote_service_file" /etc/systemd/system
ssh "${ssh_sudo_args[@]}" ls -lt /etc/systemd/system

# reload possibly changed p1encrypt.service
ssh "${ssh_sudo_args[@]}" systemctl daemon-reload
# start as systemd service
ssh "${ssh_sudo_args[@]}" systemctl start $service_name
# enable on next boot
ssh "${ssh_sudo_args[@]}" systemctl enable $service_name
# show status
ssh "${ssh_sudo_args[@]}" "$systemctl_status_cmd"
exit
