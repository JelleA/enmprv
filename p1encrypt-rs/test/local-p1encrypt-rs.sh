#! /bin/bash -ev
set -u

# Run a local version of p1encryptor-rs only to check for correct startup.
repodir=~/gitrepos/enmprv
cd $repodir

# use $USER name and $HOSTNAME in config files:
p1_config_name=p1encrypt.toml

log_config_name=p1elog4rs.yaml
tmp_log_config_file=/tmp/$log_config_name
script="{
s/p1enc-user/$USER/g
s/p1enc-host/$HOSTNAME/g
}"
sed --expression="$script" $repodir/p1encrypt-rs/$log_config_name > $tmp_log_config_file

# echo Patched log config $tmp_log_config_file:
# cat $tmp_log_config_file

database_url="postgresql://localhost/p1edb?port=5432"
DATABASE_URL=$database_url cargo run --package p1encrypt-rs $repodir/p1encrypt-rs/$p1_config_name $tmp_log_config_file