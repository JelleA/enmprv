use std::env;
use std::fs::File;
use std::io::Read;
use std::net::{IpAddr, SocketAddr, UdpSocket};
use std::process::exit;
use std::str::FromStr;
use std::sync::Arc;
use std::sync::{mpsc, Mutex};
use std::thread;

use anyhow::{anyhow, Error};
use enmplib_rs::util::RunState;
use log::{error, info};
use serde::Deserialize;
use tokio::{runtime::Builder as TokioRuntimeBuilder, sync::mpsc as tokio_mpsc};

use enmplib_rs::{
    bincoded::ShareSeedRequest,
    measurement::Measurement,
    openssl::OpenSslSignerFile,
    util::{wait_clock_synchronized, DurationConfig},
};

mod p1aggr;
mod p1db;
mod p1input;
mod p1ka;
mod p1split;

use crate::{
    p1db::MeasurementIntervalKeepTimesConfig, p1ka::SharedSeedsConfig, p1split::P1SplitConfig,
};

// P1 telegram as read from p1 terminal, and then queued
#[derive(Debug, Clone)]
pub struct P1Telegram {
    msrmnts: Vec<Measurement>,
}

const CONFIG_FILE_NAME: &str = "p1encrypt.toml";
const LOG_CONFIG_FILE_NAME: &str = "p1elog4rs.yaml";

#[derive(Debug)]
pub enum P1TelegramOrSharedSeedRequest {
    P1Telegram(P1Telegram),
    ShareSeedRequest(ShareSeedRequest),
}

#[derive(Deserialize, Debug)]
pub(crate) struct P1EncryptConfig {
    wait_clock_sync: DurationConfig,
    p1_terminal: String,
    p1_excl_bind_address: String,
    p1_excl_port: u16,
    baudrates: Vec<u64>,
    max_parity_changes: u64,
    min_p1_interval: DurationConfig,
    window_msrmnt_time_start: DurationConfig,
    window_msrmnt_time_end: DurationConfig,
    log_ignored_values_interval: DurationConfig,
    grid_connection_ean_el: String,
    grid_connection_ean_gas: String,
    bind_address_aggr_meters: String,
    bind_port_aggr_meters: u16,
    remote_address_aggr_meters: String,
    remote_port_aggr_meters: u16,
    aggr_meters_timeout: DurationConfig,
    remote_address_key_adder: String,
    remote_port_share_seed: u16,
    key_adder_timeout: DurationConfig,
    send_non_encrypted: Option<bool>,
    max_p1_queue_size: u64,
    shared_seeds: SharedSeedsConfig,
    temp_dir: String,
    key_adder_public_key: String,
    p1e_ssl_signer_file: OpenSslSignerFile,
    database_url: String,
    measurement_interval_keep_times: MeasurementIntervalKeepTimesConfig,
    p1_split: Option<P1SplitConfig>,
}

fn usage_exit(exit_code: i32) -> ! {
    eprintln!("argument: -v");
    eprintln!("  show invocation and version number");
    eprintln!("arguments: [config_file [log_config_file]]");
    eprintln!("  config_file:     default {CONFIG_FILE_NAME}");
    eprintln!("  log_config_file: default {LOG_CONFIG_FILE_NAME}");
    exit(exit_code);
}

fn error_exit(e: Error) -> ! {
    error!("{e:#}, exiting");
    exit(1);
}

fn main() {
    let mut args = env::args();
    let binary_name = args.next().unwrap_or("p1 encryptor".to_owned());
    let config_file_name = args.next().unwrap_or_else(|| CONFIG_FILE_NAME.to_string());
    let version = env!("CARGO_PKG_VERSION");
    if config_file_name == "-v" {
        println!("{binary_name} version {version}");
        exit(0);
    }
    if config_file_name == "-h" {
        usage_exit(0);
    }
    let log_config_file_name = args
        .next()
        .unwrap_or_else(|| LOG_CONFIG_FILE_NAME.to_string());
    if args.next().is_some() {
        usage_exit(1);
    }
    log4rs::init_file(
        &log_config_file_name,
        log4rs::config::Deserializers::default(),
    )
    .unwrap_or_else(|e| {
        eprintln!("{log_config_file_name}: {e:?} at log4rs::init_file, exiting",);
        exit(1);
    });
    info!(
        "starting p1 encryptor version {version} as {binary_name} user {} pid {}",
        std::env::var("USER").unwrap_or("".to_owned()),
        std::process::id()
    );

    let run_state = match RunState::new() {
        Err(e) => error_exit(e.context("Runstate::new()")),
        Ok(r) => r,
    };

    let mut toml_buf = Vec::<u8>::new();
    {
        let mut config_file = File::open(&config_file_name).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(config_file_name.to_owned()));
        });
        let _ = config_file.read_to_end(&mut toml_buf).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(config_file_name.to_owned()));
        });
    }
    let toml_str = std::str::from_utf8(&toml_buf).unwrap_or_else(|e| {
        error_exit(Error::new(e).context(config_file_name.to_owned()));
    });
    let cfn = config_file_name.clone();
    let config = Arc::new(
        toml::from_str::<P1EncryptConfig>(toml_str).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(cfn));
        }),
    );

    if !&config
        .grid_connection_ean_el
        .chars()
        .all(|c| c.is_ascii_digit())
    {
        error_exit(anyhow!(
            "{} grid_connection_ean_el not only 0-9: {}",
            &config_file_name,
            &config.grid_connection_ean_el
        ));
    };

    if !&config
        .grid_connection_ean_gas
        .chars()
        .all(|c| c.is_ascii_digit())
    {
        error_exit(anyhow!(
            "{config_file_name} grid_conn_ean_gas not only 0-9: {}",
            &config.grid_connection_ean_gas
        ));
    };

    if config
        .shared_seeds
        .validity_period
        .chrono_duration()
        .num_minutes()
        <= 0
    {
        error_exit(anyhow!(
            "{config_file_name} shared seed validity period less than 1 minute"
        ));
    }

    let shared_udp_socket: Arc<Mutex<Option<UdpSocket>>> = Arc::new(Mutex::new(None));

    let remote_aggr_meters_addr_port = SocketAddr::new(
        IpAddr::from_str(&config.remote_address_aggr_meters).unwrap_or_else(|e| {
            error_exit(Error::new(e).context(format!(
                "for aggr_meters_address {:?}",
                &config.bind_address_aggr_meters
            )));
        }),
        config.remote_port_aggr_meters,
    );

    let key_adder_socket_addr = match IpAddr::from_str(&config.remote_address_key_adder) {
        Err(e) => {
            error_exit(Error::new(e).context(format!(
                "remote_address_key_adder {:?}",
                &config.remote_address_key_adder
            )));
        },
        Ok(ip_addr) => SocketAddr::new(ip_addr, config.remote_port_share_seed),
    };

    if let Err(e) = wait_clock_synchronized(config.wait_clock_sync.std_duration().as_secs()) {
        error_exit(e.context("wait_clock_synchronized"));
    }

    // Start threads, and connect channels, in reverse order of data flow:

    let replies_shared_socket = Arc::clone(&shared_udp_socket);
    let config_clone = Arc::clone(&config);

    let _show_udp_replies_thread = thread::spawn(move || {
        p1aggr::show_udp_replies(
            &replies_shared_socket,
            remote_aggr_meters_addr_port,
            &config_clone,
        );
    });

    let max_p1_queue_size = match usize::try_from(config.max_p1_queue_size) {
        Ok(m) => m,
        Err(e) => {
            error_exit(Error::new(e).context(format!(
                "usize::try_from max_p1_queue_size {}",
                config.max_p1_queue_size
            )));
        },
    };

    // channel to encryption thread
    let (to_encrypt_chan, from_encrypt_chan) =
        mpsc::sync_channel::<P1TelegramOrSharedSeedRequest>(max_p1_queue_size);

    let encrypt_config = Arc::clone(&config);
    let encrypt_shared_udp_socket = Arc::clone(&shared_udp_socket);

    let _encryptor_thread = thread::spawn(move || {
        if let Err(e) = p1aggr::encrypt_p1_telegrams_to_udp(
            &from_encrypt_chan,
            &encrypt_shared_udp_socket,
            remote_aggr_meters_addr_port,
            &encrypt_config,
        ) {
            error_exit(e.context("encrypt_p1_telegrams_to_udp"));
        }
    });

    let (to_share_seeds_chan, mut from_share_seeds_chan) =
        tokio_mpsc::channel::<P1Telegram>(max_p1_queue_size);

    let config_clone = Arc::clone(&config);
    let run_state_clone = run_state.clone();

    let _share_seeds_thread = thread::spawn(move || {
        if let Err(e) = p1ka::share_seeds(
            &config_clone,
            &mut from_share_seeds_chan,
            &key_adder_socket_addr,
            &to_encrypt_chan,
            &run_state_clone,
        ) {
            error_exit(e.context("share_seeds"));
        }
    });

    let config_clone = Arc::clone(&config);
    let run_state_clone = run_state.clone();

    let to_share_seeds_or_split_chan: tokio_mpsc::Sender<_> =
        if let Some(p1split_config) = &config_clone.p1_split {
            if p1split_config.extra_p1_copies > 0 {
                // channel to p1 splitting thread: ports
                let (to_split_chan, mut from_split_chan) =
                    tokio_mpsc::channel::<P1Telegram>(max_p1_queue_size);
                let _p1_split_thread = thread::spawn(move || {
                    if let Err(e) = p1split::split_p1_telegrams(
                        &config_clone,
                        &mut from_split_chan,
                        &to_share_seeds_chan,
                        &run_state_clone,
                    ) {
                        error_exit(e.context("split_p1_telegrams"));
                    }
                });
                to_split_chan
            } else {
                to_share_seeds_chan
            }
        } else {
            to_share_seeds_chan
        };

    // channel to insert p1 into db thread: ports
    let config_clone = Arc::clone(&config);
    let run_state_clone = run_state.clone();
    let (to_insert_db_chan, mut from_insert_db_chan) =
        tokio_mpsc::channel::<P1Telegram>(max_p1_queue_size);
    // See https://tokio.rs/tokio/topics/bridging
    let _p1_insert_db_thread = thread::spawn(move || {
        // Use only the current thread to implement async/await
        let runtime = TokioRuntimeBuilder::new_current_thread()
            .enable_all()
            .build()
            .unwrap_or_else(|e| {
                error_exit(Error::new(e).context("tokio failed to build runtime"));
            });
        runtime.block_on(async {
            if let Err(e) = p1db::p1_telegrams_id_to_grid_ean_and_into_db(
                &config_clone,
                &mut from_insert_db_chan,
                &to_share_seeds_or_split_chan,
                &run_state_clone,
            )
            .await
            {
                error_exit(e.context("insert_p1_telegrams_into_db"));
            }
        });
    });

    let config_clone = Arc::clone(&config);
    if let Err(e) =
        p1input::read_p1_telegrams_changing_parity(&config_clone, &to_insert_db_chan, &run_state)
    {
        error_exit(e.context("read_p1_telegrams_changing_parity"));
    }
    info!("normal exit");
    exit(0);
}
