use std::thread;

use anyhow::{anyhow, Error, Result};
use enmplib_rs::util::{time_stamp_milli_secs, RunState};
use log::{info, warn};
use serde::Deserialize;
use tokio::sync::mpsc as tokio_mpsc;

use enmplib_rs::{bincoded::MeasurementType, util::DurationConfig};

use crate::{P1EncryptConfig, P1Telegram};

#[derive(Deserialize, Debug)]
pub(crate) struct P1SplitConfig {
    pub extra_p1_copies: u64,
    copies_interval: DurationConfig,
}

pub(crate) fn split_p1_telegrams(
    config: &P1EncryptConfig,
    p1_source: &mut tokio_mpsc::Receiver<P1Telegram>,
    p1_target: &tokio_mpsc::Sender<P1Telegram>,
    run_state: &RunState,
) -> Result<()> {
    let Some(p1split_config) = &config.p1_split else {
        return Err(anyhow!("missing p1split configuration"));
    };
    let extra_p1_copies = p1split_config.extra_p1_copies;
    if extra_p1_copies == 0 {
        return Err(anyhow!("zero extra_p1_copies"));
    }
    let copies_interval = p1split_config.copies_interval.chrono_duration();
    let copy_interval = copies_interval / (extra_p1_copies as i32);
    if copy_interval.num_milliseconds() <= 10 {
        return Err(anyhow!("copy_interval {copy_interval} is too short"));
    }
    let sleep_duration = std::time::Duration::from_millis(copy_interval.num_milliseconds() as u64);
    loop {
        run_state.is_running()?;
        let Some(p1_telegram) = p1_source.blocking_recv() else {
            return Err(anyhow!("closed at blocking_recv"));
        };
        info!("sending original");
        if let Err(queue_send_err) = p1_target.try_send(p1_telegram.clone()) {
            match queue_send_err {
                tokio_mpsc::error::TrySendError::Full(_p1t) => {
                    warn!("p1_target queue full, ignored p1_telegram");
                },
                tokio_mpsc::error::TrySendError::Closed(ref _p1t) => {
                    return Err(Error::new(queue_send_err));
                },
            }
        }
        for i in 1..=extra_p1_copies {
            thread::sleep(sleep_duration);
            let now_dt = time_stamp_milli_secs();
            let mut p1_clone = p1_telegram.clone();
            p1_clone.msrmnts.iter_mut().for_each(|msrmnt| {
                msrmnt.ref_id = format!("{}-{i}", msrmnt.ref_id);
                if msrmnt.unit != MeasurementType::NgNl_m3 {
                    // avoid now for gas msrmnts with long delays
                    msrmnt.utc_time_stamp = now_dt;
                }
            });
            info!("sending copy {}", i);
            if let Err(queue_send_err) = p1_target.try_send(p1_clone) {
                match queue_send_err {
                    tokio_mpsc::error::TrySendError::Full(_p1t) => {
                        warn!("p1_target queue full, ignored p1_clone");
                    },
                    tokio_mpsc::error::TrySendError::Closed(ref _p1t) => {
                        return Err(Error::new(queue_send_err));
                    },
                }
            }
        }
    }
}
