//! p1encryptor interface to key adder.

//  To share the seed, choose a validity interval length and starting moment.
//  Choose an initial starting time (not_before_dt), this should:
//    - not be in the past for consistency with unknown earlier share seeds.
//    - be usable in a single testing period of say 3 minutes.
//    try initial starting time at one minute after the current minute ends.
//  The initial starting time is implemented here as one minute after the current minute ends.

//  Choose validity interval length to avoid leap seconds outside validity intervals,
//  so this should be in whole minutes when starting at a minute boundary.
//  For testing, 1 minute is practical.
//  For development, a number of whole hours or days.
//  In real life, a larger number of whole weeks.

//  To avoid many p1encryptors connecting to the key adder at the same time after a power up,
//  use a random wait period between 1 and 3 minutes.
//  after failing to connect to the key adder to share a seed.
//  When sharing a new seed, generate a new one each time a connection is tried.

//  At startup of the p1encryptor, share the currently valid seed with the key adder,
//  or choose an initial seed as above.
//  For this it will be necessary to store the current seed for later use after shutdown.
//  Initially during the development, use the shared seed from the config file for this.
//  Lateron add the shared seeds into a db table.

//  When the key adder goes down it may need the currently valid shared seed,
//  this will be indicated by the aggregator reply to a measurement message.
//  Choose minimum time before end of current validity interval, choose a new seed and share it.

use std::{
    cmp::Ordering,
    collections::{
        hash_map::Entry::{Occupied, Vacant},
        HashMap,
    },
    iter::repeat_with,
    net::SocketAddr,
    sync::mpsc::{self, SyncSender},
    time::{Duration, SystemTime},
};

use anyhow::{anyhow, Context, Error, Result};
use bincode::Options;
use chrono::{DateTime, Duration as ChronoDuration, NaiveDateTime, NaiveTime, Timelike, Utc};
use log::{error, info, trace, warn};
use rand::random;
use serde::Deserialize;
use tokio::sync::mpsc as tokio_mpsc;

use enmplib_rs::{
    bincoded::{
        get_bincode_options, slice_u8_to_string, ShareSeedEncryptionInfo, ShareSeedInterval,
        ShareSeedReply, ShareSeedRequest, INTERVAL_SEPARATOR_MILLIS,
    },
    openssl::{OpenSslSigVerifier, OpenSslSigner},
    util::{
        receive_version_size_message, send_version_size_message, tcp_stream_in_wg_intf,
        time_stamp_from_str, time_stamp_to_string, DurationConfig, RunState,
    },
};

use crate::{P1EncryptConfig, P1Telegram, P1TelegramOrSharedSeedRequest};

#[derive(Deserialize, Debug, Clone)]
pub(crate) struct SharedSeedsConfig {
    random_wait: DurationConfig,
    repeat_interval: DurationConfig,
    pub keep_interval: DurationConfig, // how long to keep a shared seed for encrypting measurements from the past.
    replace_old_seeds_interval: DurationConfig,
    hash_function: String,
    seed_size: u16,
    pub validity_period: DurationConfig,
    pub before_next_period: DurationConfig, // provide new share seed before current one expires.
    pub ten_pow_modulus: u16,
    pub ten_pow_divisor: u16,
}

// for a grid_ean/meas_dt pair determine whether
// - to use a shared seed from the cache, or
// - to provide the starting time of a possibly validity interval, and a new random seed.
fn new_interval_not_before_dt_opt(
    shared_seeds_status_cache: &SharedSeedsStatusCache,
    grid_ean: &str,
    meas_dt: &DateTime<Utc>,
    seed_size: usize,
) -> Option<(Vec<u8>, DateTime<Utc>)> {
    if let Some((_sent_to_ka, earlier_share_seed_request)) = shared_seeds_status_cache.get(grid_ean)
    {
        // check the validity period of the entry against the msrmnt time.
        let msrmnt_dt_vec = time_stamp_to_string(meas_dt).as_bytes().to_vec();
        if msrmnt_dt_vec.cmp(&earlier_share_seed_request.interval.not_after_dt_vec)
            == Ordering::Greater
        {
            trace!(
                "{grid_ean} {} msmrnt is after earlier: {earlier_share_seed_request}",
                slice_u8_to_string(&msrmnt_dt_vec)
            );
            let new_seed = random_seed(seed_size); // this may take some time
            Some((new_seed, *meas_dt))
        } else if msrmnt_dt_vec.cmp(&earlier_share_seed_request.interval.not_before_dt_vec)
            == Ordering::Less
        {
            warn!(
                "{grid_ean} {} msmrnt is before interval: {}",
                slice_u8_to_string(&msrmnt_dt_vec),
                earlier_share_seed_request.interval,
            );
            None
        } else {
            // shared seed available, new new one random seed needed.
            None
        }
    } else {
        // this might take some time
        let new_seed = random_seed(seed_size);
        // avoid meas_dt, it might be too early or too late.
        let now = DateTime::<Utc>::from(SystemTime::now());
        if *meas_dt > now {
            error!("meas_dt {meas_dt} is in the future");
            None
        } else {
            Some((new_seed, now))
        }
    }
}

fn random_seed(num_bytes: usize) -> Vec<u8> {
    // random::<u8>() may involve a delay until randomness is available.
    repeat_with(random).take(num_bytes).collect()
}

// for a given grid_ean and a starting time of validity interval, provide a new share seed request
fn new_share_seed_request(
    grid_ean: &str,
    new_seed: Vec<u8>,
    not_before_dt: &DateTime<Utc>,
    config: &P1EncryptConfig,
    seed_validity_period: &ChronoDuration,
) -> Result<ShareSeedRequest> {
    let two_minutes_after = *not_before_dt + ChronoDuration::minutes(2);
    let date_after = two_minutes_after.date_naive();
    let time_after = two_minutes_after.time();
    let Some(whole_minute_after) =
        NaiveTime::from_hms_opt(time_after.hour(), time_after.minute(), 0)
    else {
        return Err(anyhow!("no whole minute for {time_after:?}"));
    };
    let new_start_dt: DateTime<Utc> = DateTime::from_naive_utc_and_offset(
        NaiveDateTime::new(date_after, whole_minute_after),
        Utc,
    );
    let new_not_before_dt = new_start_dt + ChronoDuration::milliseconds(INTERVAL_SEPARATOR_MILLIS);
    let new_not_before_dt_vec = time_stamp_to_string(&new_not_before_dt).as_bytes().to_vec();
    let new_not_after_dt = new_start_dt + *seed_validity_period;
    let new_not_after_dt_vec = time_stamp_to_string(&new_not_after_dt).as_bytes().to_vec();

    Ok(ShareSeedRequest {
        grid_connection_ean: grid_ean.as_bytes().to_vec(),
        encryption_info: ShareSeedEncryptionInfo {
            hash_function_name: config.shared_seeds.hash_function.as_bytes().to_vec(),
            seed: new_seed,
            ten_pow_modulus: config.shared_seeds.ten_pow_modulus,
            ten_pow_divisor: config.shared_seeds.ten_pow_divisor,
        },
        interval: ShareSeedInterval {
            not_before_dt_vec: new_not_before_dt_vec,
            not_after_dt_vec: new_not_after_dt_vec,
        },
    })
}

enum SharedSeedState {
    New,                // when created, or with new interval and random seed.
    ReceivedByKeyAdder, // when the key adder confirmed reception of SharedSeedRequest before interval.not_before_dt .
    SentToEncryptor,    // when put on the queue to module p1aggr.
}

type SharedSeedsStatusCache = HashMap<String, (SharedSeedState, ShareSeedRequest)>;

// replace shared seeds that are in their before_next_period at the end of the validity period.
fn replace_old_shared_seeds(
    shared_seeds_status_cache: &mut SharedSeedsStatusCache,
    before_next_period: &ChronoDuration,
    seed_validity_period: &ChronoDuration,
    seed_size: usize,
    run_state: &RunState,
) -> Result<()> {
    let latest_validity = DateTime::<Utc>::from(SystemTime::now()) + *before_next_period;
    let latest_validity_vec = time_stamp_to_string(&latest_validity).as_bytes().to_vec();
    trace!("replacement latest_validity {latest_validity}");

    for (shared_seed_state, ssr) in shared_seeds_status_cache.values_mut() {
        if ssr.interval.not_after_dt_vec < latest_validity_vec {
            // replace ssr with validity period just after ssr.interval and new key.
            let mut prev_not_after_dt =
                match String::from_utf8(ssr.interval.not_after_dt_vec.clone()) {
                    Err(utf8_error) => {
                        error!(
                            "{utf8_error} at from_utf8 not_after_dt_vec {:?}",
                            ssr.interval.not_after_dt_vec
                        );
                        continue; // next ssr
                    },
                    Ok(s) => match time_stamp_from_str(&s) {
                        Err(e) => {
                            error!("{e:#} at time_stamp_from_str not_after_dt_vec {s}");
                            continue; // next ssr
                        },
                        Ok(dt) => dt,
                    },
                };

            let mut next_not_after_dt;
            loop {
                run_state.is_running()?;
                // Try next interval until at latest_validity.
                // this loop can take long when the p1 stream was interrupted and this program continues,
                // therefore there is timeout on the input queue per hour
                // that calls this replace_old_shared_seeds().
                next_not_after_dt = prev_not_after_dt + *seed_validity_period;
                if next_not_after_dt > latest_validity {
                    break;
                }
                prev_not_after_dt = next_not_after_dt;
            }

            let next_not_before_dt =
                prev_not_after_dt + ChronoDuration::milliseconds(INTERVAL_SEPARATOR_MILLIS);
            let next_not_before_dt_vec = time_stamp_to_string(&next_not_before_dt)
                .as_bytes()
                .to_owned();
            let next_not_after_dt_vec = time_stamp_to_string(&next_not_after_dt)
                .as_bytes()
                .to_owned();

            let mut new_ssr = ssr.clone();
            new_ssr.interval.not_before_dt_vec = next_not_before_dt_vec;
            new_ssr.interval.not_after_dt_vec = next_not_after_dt_vec;
            new_ssr.encryption_info.seed = random_seed(seed_size);

            *ssr = new_ssr;
            *shared_seed_state = SharedSeedState::New;
        }
    }
    Ok(())
}

fn share_seed(
    share_seed_req: &ShareSeedRequest,
    key_adder_socket_addr: &SocketAddr,
    key_adder_timeout: ChronoDuration,
    read_timeout_seconds: u64,
    p1encryptor_signer: &OpenSslSigner,
    keyadder_sig_verifier: &OpenSslSigVerifier,
) -> Result<()> {
    let time_out_seconds = match u64::try_from(key_adder_timeout.num_seconds()) {
        Ok(s) => s,
        Err(e) => {
            return Err(Error::new(e).context(format!(
                "key_adder_timeout.num_seconds() {key_adder_timeout}"
            )))
        },
    };
    let key_adder_tcp_stream = &mut tcp_stream_in_wg_intf(key_adder_socket_addr, time_out_seconds)
        .context("tcp_stream_in_wg_intf")?;

    let bincode_options = get_bincode_options();
    // serialize shared_seed_req, sign it, and send both to the key adder.
    let request_bytes = bincode_options
        .serialize::<ShareSeedRequest>(share_seed_req)
        .context("serialize share_seed_req")?;
    send_version_size_message(&request_bytes, key_adder_tcp_stream)
        .context("send_version_size_message share_seed_req")?;
    trace!("sent to key adder: {share_seed_req}");
    let signature = p1encryptor_signer
        .generate_signature(&request_bytes)
        .context("generate_signature")?;

    send_version_size_message(&signature, key_adder_tcp_stream)
        .context("send_version_size_message signature")?;
    trace!("sent {share_seed_req}");

    let reply_buf = receive_version_size_message(key_adder_tcp_stream, read_timeout_seconds)
        .context("receive_version_size_message reply_buf")?;

    // verify the signature of the reply
    let signature = receive_version_size_message(key_adder_tcp_stream, read_timeout_seconds)
        .context("receive_version_size_message shared_seed_reply signature")?;

    let sig_ok = keyadder_sig_verifier
        .verify_signature(&reply_buf, &signature)
        .context("verify_signature shared_seed_reply")?;
    if !sig_ok {
        return Err(anyhow!("verify_signature failed"));
    }

    let shared_seed_reply = bincode_options
        .deserialize::<ShareSeedReply>(&reply_buf)
        .context("deserialize ShareSeedReply")?;

    if let Some(mes) = shared_seed_reply.message {
        trace!("message from key adder: {mes}");
        Err(anyhow!("{mes} in ShareSeedReply"))
    } else {
        trace!("received from key adder: ok");
        Ok(())
    }
}

// long running thread, sending SharedSeedEncryptionInfo on a channel and ShareSeedsRequest's to key adder.
pub(crate) fn share_seeds(
    config: &P1EncryptConfig,
    from_share_keys_chan: &mut tokio_mpsc::Receiver<P1Telegram>,
    key_adder_socket_addr: &SocketAddr,
    to_encrypt_chan: &SyncSender<P1TelegramOrSharedSeedRequest>,
    run_state: &RunState,
) -> Result<()> {
    let p1e_ssl_signer = config
        .p1e_ssl_signer_file
        .open_ssl_signer(&config.temp_dir)?;
    let key_adder_sig_verifier =
        OpenSslSigVerifier::new(&config.key_adder_public_key, &config.temp_dir)?;

    let key_adder_timeout = config.key_adder_timeout.chrono_duration();
    let read_timeout_seconds = u64::try_from(key_adder_timeout.num_seconds())
        .with_context(|| format!("u64::from num_seconds {key_adder_timeout}"))?;

    info!("random_wait_period {}", config.shared_seeds.random_wait);
    let random_wait_period = config.shared_seeds.random_wait.chrono_duration();
    info!("repeat_interval {}", config.shared_seeds.repeat_interval);
    let repeat_interval = config.shared_seeds.repeat_interval.chrono_duration();

    let seed_validity_period: ChronoDuration =
        config.shared_seeds.validity_period.chrono_duration();
    if seed_validity_period.num_minutes() <= 0 {
        return Err(anyhow!(
            "cannot use shared seeds seed_validity_period less than 1 minute"
        ));
    }
    let before_next_period = config.shared_seeds.before_next_period.chrono_duration();
    if before_next_period.num_minutes() <= 0 {
        return Err(anyhow!(
            "cannot use shared seeds before_next_period less than 1 minute"
        ));
    }
    if seed_validity_period <= before_next_period {
        return Err(anyhow!(
            "before_next_period {} {} seed_validity_period {}",
            before_next_period,
            "must be smaller than",
            seed_validity_period
        ));
    }

    // generate a new seed at start up and make it valid for the configured validity period.
    let mut shared_seeds_status_cache = SharedSeedsStatusCache::new();

    let mut next_repeat_interval_start = DateTime::<Utc>::from(SystemTime::now())
        + ChronoDuration::seconds(
            random_wait_period.num_seconds() * i64::from(rand::random::<u8>()) / i64::from(u8::MAX),
        );
    info!("next_repeat_interval_start {next_repeat_interval_start}");

    let mut next_replace_old_seeds = next_repeat_interval_start;

    let seed_size = config.shared_seeds.seed_size as usize;
    loop {
        run_state.is_running()?;
        let p1_telegram = match from_share_keys_chan.try_recv() {
            Err(tokio_mpsc::error::TryRecvError::Disconnected) => {
                return Err(anyhow!(
                    "recv_timeout, p1_telegram queue no longer available"
                ));
            },
            Err(tokio_mpsc::error::TryRecvError::Empty) => {
                if DateTime::<Utc>::from(SystemTime::now()) > next_replace_old_seeds {
                    info!("timeout on queue, replace_old_shared_seeds");
                    // avoid too long loop in replace_old_shared_seeds():
                    if let Err(e) = replace_old_shared_seeds(
                        &mut shared_seeds_status_cache,
                        &before_next_period,
                        &seed_validity_period,
                        seed_size,
                        run_state,
                    ) {
                        error!("{e:#} at replace_old_shared_seeds");
                    }
                    next_replace_old_seeds = DateTime::<Utc>::from(SystemTime::now())
                        + config
                            .shared_seeds
                            .replace_old_seeds_interval
                            .chrono_duration();
                } else {
                    std::thread::sleep(Duration::from_secs(1));
                }
                continue;
            },
            Ok(p1t) => p1t,
        };

        // immediately pass on the p1_telegram for encryption to aggregator:
        let share_seeds_msrmnts = p1_telegram.msrmnts.clone();
        if let Err(queue_send_err) =
            to_encrypt_chan.try_send(P1TelegramOrSharedSeedRequest::P1Telegram(p1_telegram))
        {
            match queue_send_err {
                mpsc::TrySendError::Full(_full_error) => {
                    warn!("to_encrypt_chan queue full, ignored p1_telegram");
                },
                mpsc::TrySendError::Disconnected(ref _option_mkvs) => {
                    return Err(Error::new(queue_send_err));
                },
            }
        }

        if let Err(e) = replace_old_shared_seeds(
            &mut shared_seeds_status_cache,
            &before_next_period,
            &seed_validity_period,
            seed_size,
            run_state,
        ) {
            error!("{e:#} at replace_old_shared_seeds");
        };

        // determine earliest utc_time_stamp by grid ean in p1 telegram:
        let mut meas_dt_by_grid_ean = HashMap::new();
        for msrmnt in share_seeds_msrmnts {
            match meas_dt_by_grid_ean.entry(msrmnt.ref_id) {
                Vacant(ve) => {
                    ve.insert(msrmnt.utc_time_stamp);
                },
                Occupied(mut oe) => {
                    if oe.get() > &msrmnt.utc_time_stamp {
                        oe.insert(msrmnt.utc_time_stamp);
                    }
                },
            }
        }

        for (grid_ean, meas_dt) in meas_dt_by_grid_ean {
            trace!(
                "from p1_telegram: {grid_ean} {}",
                time_stamp_to_string(&meas_dt)
            );
            if let Some((new_seed, not_before_dt)) = new_interval_not_before_dt_opt(
                &shared_seeds_status_cache,
                &grid_ean,
                &meas_dt,
                seed_size,
            ) {
                let share_seed_req = match new_share_seed_request(
                    &grid_ean,
                    new_seed,
                    &not_before_dt,
                    config,
                    &seed_validity_period,
                ) {
                    Err(e) => {
                        error!("{e:#} at new_share_seed_request");
                        continue; // next grid_ean_meas_dts
                    },
                    Ok(ssr) => ssr,
                };

                trace!("new {share_seed_req}"); // CHECKME: never seen, 20230911

                shared_seeds_status_cache
                    .insert(grid_ean.to_owned(), (SharedSeedState::New, share_seed_req));
            }
            if next_repeat_interval_start > DateTime::<Utc>::from(SystemTime::now()) {
                trace!("next_repeat_interval_start: {next_repeat_interval_start}");
            } else {
                info!("time is after next_repeat_interval_start: {next_repeat_interval_start}");
                // in repeat interval, send all available shared seeds again for the case that the key adder has restarted.
                for (shared_seed_state, ssr) in shared_seeds_status_cache.values_mut() {
                    if let Err(e) = share_seed(
                        ssr,
                        key_adder_socket_addr,
                        key_adder_timeout,
                        read_timeout_seconds,
                        &p1e_ssl_signer,
                        &key_adder_sig_verifier,
                    ) {
                        error!("{e:#} at share_seed");
                        break; // no need to try more TCP
                    }
                    match *shared_seed_state {
                        SharedSeedState::New => {
                            // CHECKME: should the key adder reply be before the end of the interval?
                            *shared_seed_state = SharedSeedState::ReceivedByKeyAdder;
                        },
                        SharedSeedState::ReceivedByKeyAdder | SharedSeedState::SentToEncryptor => {
                        },
                    }
                }

                next_repeat_interval_start = DateTime::<Utc>::from(SystemTime::now())
                    + repeat_interval
                    + ChronoDuration::seconds(
                        (random_wait_period.num_minutes() * i64::from(rand::random::<u8>())) * 60
                            / i64::from(u8::MAX),
                    );
                info!("new next_repeat_interval_start: {next_repeat_interval_start}");
            }

            for (shared_seed_state, ssr) in shared_seeds_status_cache.values_mut() {
                match *shared_seed_state {
                    SharedSeedState::New | SharedSeedState::SentToEncryptor => {},
                    SharedSeedState::ReceivedByKeyAdder => {
                        // send share_seed_request to p1aggr for encryption:
                        if let Err(queue_send_err) = to_encrypt_chan
                            .try_send(P1TelegramOrSharedSeedRequest::ShareSeedRequest(ssr.clone()))
                        {
                            match queue_send_err {
                                mpsc::TrySendError::Full(_full_error) => {
                                    warn!("queue full, ignored shared seed");
                                    continue; // try again at next p1_telegram
                                },
                                mpsc::TrySendError::Disconnected(_option_mkvs) => {
                                    return Err(anyhow!(
                                        "P1TelegramOrSharedSeedRequest queue disconnected"
                                    ));
                                },
                            }
                        } else {
                            *shared_seed_state = SharedSeedState::SentToEncryptor;
                        }
                    },
                }
            }
        }
    }
}
