use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::Read;
use std::os::unix::io::AsRawFd;
use std::str::FromStr;
use std::thread;
use std::time::Duration;

use anyhow::{anyhow, Context, Error, Result};
use chrono::{
    DateTime, Duration as ChronoDuration, Local, LocalResult, NaiveDate, NaiveDateTime, NaiveTime,
    TimeZone, Utc,
};
use log::{debug, error, info, trace, warn};
use nix::sys::epoll::{epoll_create, epoll_ctl, epoll_wait, EpollEvent, EpollFlags, EpollOp};
use tokio::sync::mpsc as tokio_mpsc;

use enmplib_rs::bigintpowten::BigIntPowTen;
use enmplib_rs::bincoded::MeasurementType;
use enmplib_rs::util::{
    addr_port_str, bash_cmd_output, bash_cmd_stdout, time_stamp_milli_secs, time_stamp_to_string,
    udp_bind, RunState,
};

use crate::{Measurement, P1EncryptConfig, P1Telegram};

// P1 telegram as read from p1 terminal, and then queued
#[derive(Debug, Clone)]
pub struct P1TelegramBuilder {
    utc_time_stamp: DateTime<Utc>,
    used_obis_keys: HashSet<String>,
    obis_kf: ObisKeysFunctions,
    last_logged_keys_values: HashMap<String, String>,
    crc16: u16,
    input_errors: u64,
    identity_line_opt: Option<String>,
    last_time_stamp_opt: Option<DateTime<Utc>>,
    last_msrmnt_type_opt: Option<MeasurementType>,
    last_equipment_id_opt: Option<String>,
    msrmnts: Vec<Measurement>,
}

type AddToP1TelegramFromObisKeyValue = fn(&mut P1TelegramBuilder, &str, &str) -> Result<()>;

type ObisKeysFunctions = HashMap<&'static str, AddToP1TelegramFromObisKeyValue>;

fn new_obis_kf() -> ObisKeysFunctions {
    // Actually encountered obis keys, ordered by key value:
    let empty_key = ""; // for gas m3
    let power_time_stamp = "0-0:1.0.0";
    let equipment_id = "0-0:96.1.1"; // Power meter serial number
    let long_power_failures = "0-0:96.7.9";
    let power_failures = "0-0:96.7.21";
    let text_message_0 = "0-0:96.13.0";
    let text_message_1 = "0-0:96.13.1";
    let tariff_el = "0-0:96.14.0"; // tariff electricity
    let device_type_channel_1 = "0-1:24.1.0";
    let channel0_time_and_unit = "0-1:24.3.0";
    // Encountered in telegram lines:
    //     0-1:96.1.0(3338303034303038323431313331363132)
    //     0-1:24.1.0(03)
    //     0-1:24.3.0(230827170000)(08)(60)(1)(0-1:24.2.0)(m3)
    //     (20204.264)
    // Parse these into a Measurement by collecting:
    //   device id (1st line)
    //   gas type (2nd line)
    //   time stamp (3rd line), verify m3 for gas type,
    //   value (4th line)
    let channel1_time_and_value = "0-1:24.2.1";
    // 0-n:24.2.1.255, Last 5-minute Meter reading Heat or Cold in 0,01 GJ and capture time
    // Encountered in telegram lines:
    //     0-1:24.1.0(003)
    //     0-1:96.1.0(4730303233353631323236303936333134)
    //     0-1:24.2.1(230826000000S)(09100.175*m3)
    // The m3 is against the spec of 0,01 GJ, just assume it is NL gas in m3.
    // Parse these into a Measurement by collecting:
    //   gas type (1st line)
    //   device id (2nd line)
    //   time stamp and value (3rd line), verify m3 for gas type
    let gas_equipment_id = "0-1:96.1.0"; // Gas meter serial number
    let el_kw_delivered = "1-0:1.7.0"; // Actual import kW
    let el_kwh_to_tariff_1 = "1-0:1.8.1"; // Import tariff 1
    let el_kwh_to_tariff_2 = "1-0:1.8.2"; // Import tariff 2
    let el_kw_received = "1-0:2.7.0"; // Actual export kW
    let el_kwh_by_tariff_1 = "1-0:2.8.1"; // Export tariff 1
    let el_kwh_by_tariff_2 = "1-0:2.8.2"; // Export tariff 2
    let version_key = "1-3:0.2.8";
    let power_failure_event_log = "1-0:99.97.0";
    let num_voltage_sags_l1 = "1-0:32.32.0"; // in phase L1
    let num_voltage_sags_l2 = "1-0:52.32.0"; // in phase L2, etc
    let num_voltage_sags_l3 = "1-0:72.32.0";
    let num_voltage_swells_l1 = "1-0:32.36.0";
    let num_voltage_swells_l2 = "1-0:52.36.0";
    let num_voltage_swells_l3 = "1-0:72.36.0";
    let instant_current_l1 = "1-0:31.7.0"; // Instantaneous current L1, etc.
    let instant_current_l2 = "1-0:51.7.0";
    let instant_current_l3 = "1-0:71.7.0";
    let instant_active_pos_power_l1 = "1-0:21.7.0"; // Instantaneous active power L1 (+P)
    let instant_active_pos_power_l2 = "1-0:41.7.0";
    let instant_active_pos_power_l3 = "1-0:61.7.0";
    let instant_active_neg_power_l1 = "1-0:22.7.0"; // Instantaneous active power L1 (-P)
    let instant_active_neg_power_l2 = "1-0:42.7.0";
    let instant_active_neg_power_l3 = "1-0:62.7.0";

    // Parsing routines, adding parsed into to a P1TelegramBuilder
    type Akv = AddToP1TelegramFromObisKeyValue;
    let add_version = P1TelegramBuilder::add_version as Akv;
    let add_power_time_stamp = P1TelegramBuilder::add_power_time_stamp as Akv;
    let add_equipment_id = P1TelegramBuilder::add_equipment_id as Akv;
    let add_kwh_msrmnt_to = P1TelegramBuilder::add_kwh_msrmnt_to as Akv;
    let add_kwh_msrmnt_by = P1TelegramBuilder::add_kwh_msrmnt_by as Akv;
    let add_kw_msrmnt_to = P1TelegramBuilder::add_kw_msrmnt_to as Akv;
    let add_kw_msrmnt_by = P1TelegramBuilder::add_kw_msrmnt_by as Akv;
    let add_tariff = P1TelegramBuilder::add_tariff as Akv;
    let add_text_message = P1TelegramBuilder::add_text_message as Akv;
    let add_device_type = P1TelegramBuilder::add_device_type as Akv;
    let add_time_and_unit = P1TelegramBuilder::add_time_and_unit as Akv;
    let add_time_and_value = P1TelegramBuilder::add_time_and_value as Akv;
    let add_empty_key = P1TelegramBuilder::add_empty_key as Akv;
    let add_power_failures = P1TelegramBuilder::add_power_failures as Akv;
    let add_power_failure_event_log = P1TelegramBuilder::add_power_failure_event_log as Akv;
    let add_voltage_sags_swells_line = P1TelegramBuilder::add_voltage_sags_swells_line as Akv;
    let add_instant_current_line = P1TelegramBuilder::add_instant_current_line as Akv;
    let add_kw_msrmnt_line = P1TelegramBuilder::add_kw_msrmnt_line as Akv;

    // associate obis keys with parsing routines:
    HashMap::from_iter([
        (version_key, add_version),
        (power_time_stamp, add_power_time_stamp),
        (equipment_id, add_equipment_id),
        (gas_equipment_id, add_equipment_id),
        (el_kwh_to_tariff_1, add_kwh_msrmnt_to),
        (el_kwh_to_tariff_2, add_kwh_msrmnt_to),
        (el_kwh_by_tariff_1, add_kwh_msrmnt_by),
        (el_kwh_by_tariff_2, add_kwh_msrmnt_by),
        (el_kw_delivered, add_kw_msrmnt_to),
        (instant_active_pos_power_l1, add_kw_msrmnt_line), // duplicate of el_kw_delivered on single phase
        (instant_active_pos_power_l2, add_kw_msrmnt_line),
        (instant_active_pos_power_l3, add_kw_msrmnt_line),
        (el_kw_received, add_kw_msrmnt_by),
        (instant_active_neg_power_l1, add_kw_msrmnt_line), // duplicate of el_kw_received on single phase
        (instant_active_neg_power_l2, add_kw_msrmnt_line),
        (instant_active_neg_power_l3, add_kw_msrmnt_line),
        (tariff_el, add_tariff),
        (text_message_0, add_text_message),
        (text_message_1, add_text_message),
        (device_type_channel_1, add_device_type),
        (channel0_time_and_unit, add_time_and_unit),
        (channel1_time_and_value, add_time_and_value),
        (empty_key, add_empty_key),
        (power_failures, add_power_failures),
        (long_power_failures, add_power_failures),
        (power_failure_event_log, add_power_failure_event_log),
        (num_voltage_sags_l1, add_voltage_sags_swells_line),
        (num_voltage_sags_l2, add_voltage_sags_swells_line),
        (num_voltage_sags_l3, add_voltage_sags_swells_line),
        (num_voltage_swells_l1, add_voltage_sags_swells_line),
        (num_voltage_swells_l2, add_voltage_sags_swells_line),
        (num_voltage_swells_l3, add_voltage_sags_swells_line),
        (instant_current_l1, add_instant_current_line),
        (instant_current_l2, add_instant_current_line),
        (instant_current_l3, add_instant_current_line),
    ])
}

fn utc_from_msrmnt_time_str(msrmnt_local_time: &str, now: DateTime<Utc>) -> Result<DateTime<Utc>> {
    // determine utc from msrmnt_time_str in local time
    // Examples 210925140000, 170108161107
    if !msrmnt_local_time.chars().all(|c| c.is_ascii_digit()) {
        return Err(anyhow!("not all digits"));
    }
    let u32_from_str = {
        |s| match u32::from_str(s) {
            Ok(u) => Ok(u),
            Err(e) => Err(anyhow!("{e:?} at u32::from_str {s}")),
        }
    };
    let input_len = msrmnt_local_time.len();
    if input_len < 12 {
        return Err(anyhow!("time stamp {msrmnt_local_time} shorter than 12"));
    };
    let yy = u32_from_str(&msrmnt_local_time[..input_len - 10])?;
    let mo = u32_from_str(&msrmnt_local_time[input_len - 10..input_len - 8])?;
    let dd = u32_from_str(&msrmnt_local_time[input_len - 8..input_len - 6])?;
    let hh = u32_from_str(&msrmnt_local_time[input_len - 6..input_len - 4])?;
    let mm = u32_from_str(&msrmnt_local_time[input_len - 4..input_len - 2])?;
    let ss = u32_from_str(&msrmnt_local_time[input_len - 2..])?;

    let yyi32 = yy as i32
        + if yy >= 2000 {
            0
        } else {
            // Y2100 Y21C Y21K
            if yy >= 100 {
                return Err(anyhow!("no century for year {yy} in {msrmnt_local_time}"));
            }
            // determine century for 2 digit year from current date
            let y2k_i32: i32 = 2000;
            let y2k_dt = NaiveDate::from_ymd_opt(y2k_i32, 1, 1).ok_or(anyhow!("y2k_dt"))?;
            let yyyy_since_y2k = now
                .date_naive()
                .years_since(y2k_dt)
                .ok_or(anyhow!("yyyy_since_y2k"))? as i32;
            let cc_since_y2k = yyyy_since_y2k / 100;
            let yy_since_y2k = (yyyy_since_y2k % 100) as u32;
            y2k_i32
                + 100
                    * (cc_since_y2k
                        + if yy == 99 && yy_since_y2k == 00 {
                            // yy from previous century
                            -1
                        } else if yy == 0 && yy_since_y2k == 99 {
                            // yy from next century
                            1
                        } else {
                            0
                        })
        };
    let Some(naive_date) = NaiveDate::from_ymd_opt(yyi32, mo, dd) else {
        return Err(anyhow!("no NaiveDate from {yyi32} {mo} {dd}"));
    };
    let Some(naive_time) = NaiveTime::from_hms_opt(hh, mm, ss) else {
        return Err(anyhow!("no NaiveTime from {hh} {mm} {ss}"));
    };
    let naive_date_time = NaiveDateTime::new(naive_date, naive_time);
    let local_dt = match Local.from_local_datetime(&naive_date_time) {
        LocalResult::None => {
            return Err(anyhow!("no local time zone for {naive_date_time}"));
        },
        LocalResult::Single(ldt) => ldt,
        LocalResult::Ambiguous(ldt1, ldt2) => {
            warn!("local measurement time {msrmnt_local_time} has ambiguous utc, ignoring {ldt1}, using {ldt2}");
            ldt2
        },
    };
    Ok(local_dt.with_timezone(&Utc))
}

impl P1Telegram {
    fn log_msrmnts(&self) {
        for m in &self.msrmnts {
            info!(
                "{:>10} {:>9} at {}",
                m.value.to_string(3),
                m.unit.to_str(),
                time_stamp_to_string(&m.utc_time_stamp)
            );
        }
    }
}

fn strip_optional_suffix_from_time_stamp(stamp: &str) -> &str {
    if let Some(stripped) = stamp.strip_suffix('W') {
        stripped
    } else if let Some(stripped) = stamp.strip_suffix('S') {
        stripped
    } else {
        stamp
    }
}

impl P1TelegramBuilder {
    fn new(last_logged_keys_values: &HashMap<String, String>) -> Self {
        P1TelegramBuilder {
            used_obis_keys: HashSet::new(),
            obis_kf: new_obis_kf(),
            last_logged_keys_values: last_logged_keys_values.clone(),
            crc16: 0,
            input_errors: 0,
            identity_line_opt: None,
            utc_time_stamp: time_stamp_milli_secs(),
            last_time_stamp_opt: None,
            last_equipment_id_opt: None,
            last_msrmnt_type_opt: None,
            msrmnts: Vec::new(),
        }
    }

    fn build(
        &self,
        window_msrmnt_time_start: &ChronoDuration,
        window_msrmnt_time_end: &ChronoDuration,
    ) -> Result<P1Telegram> {
        if self.identity_line_opt.is_none() {
            Err(anyhow!("no identity line"))
        } else {
            // verify that each measurement type occurs only once:
            let mut msrmnt_types = HashSet::<MeasurementType>::new();
            for msrmnt in &self.msrmnts {
                if !msrmnt_types.insert(msrmnt.unit) {
                    return Err(anyhow!("unit more than once: {:?}", msrmnt.unit));
                }
                // Ensure that the msrmnt.mrmnt_dt_utc is not too far into in the future.
                // Otherwise the check lateron that a later measurement comes after this one
                // might prevent use of only later measurements for a long time.
                if msrmnt.utc_time_stamp + *window_msrmnt_time_start < self.utc_time_stamp {
                    return Err(anyhow!("measurement too early: {msrmnt}, window_msrmnt_time_start {window_msrmnt_time_start}"));
                }
                // Gas measurements can be 5 quarters of an hour late, allow a maximum delay.
                if msrmnt.utc_time_stamp - *window_msrmnt_time_end > self.utc_time_stamp {
                    return Err(anyhow!("measurement too late: {msrmnt}, window_msrmnt_time_end {window_msrmnt_time_end}"));
                }
            }
            Ok(P1Telegram {
                msrmnts: self.msrmnts.to_vec(),
            })
        }
    }

    fn add_line(&mut self, line: &str) {
        if line.is_empty() {
            return;
        }
        if self.identity_line_opt.is_none() {
            // first line
            self.identity_line_opt = Some(line.to_string());
        } else if line.starts_with('!') {
            // last telegram line
            if line.len() == 5 {
                warn!("last line {line} expecting crc {:04X}", self.crc16);
            }
        } else if let Err(e) = self.add_obis_line(line) {
            self.input_errors += 1;
            error!("{e:#} for line: {line}");
        }
    }

    fn add_crc(&mut self, b: u8) {
        // See https://www.netbeheernederland.nl/_upload/Files/Slimme_meter_15_2bff566986.pdf
        // section 5.2:
        // CRC is a CRC16 value calculated over the preceding characters in the data message
        // (from “/” to “!” using the polynomial: x16+x15+x2+1).
        // CRC16 uses no XOR in, no XOR out and is computed with least significant bit first.
        // The value is represented as 4 hexadecimal characters (MSB first).
        self.crc16 ^= b as u16;
        for _ in 0..u8::BITS {
            if self.crc16 & 1 == 0 {
                self.crc16 >>= 1;
            } else {
                self.crc16 >>= 1;
                self.crc16 ^= 0xA001;
            }
        }
    }

    fn add_obis_line(&mut self, line: &str) -> Result<()> {
        // line with obis key and value, e.g. 0-0:96.1.1(31333631323039342020202020202020)
        // values can be repeated on the same line by appending (value) brackets. A value may be empty ().
        let Some(first_open_bracket_pos) = line.find('(') else {
            self.input_errors += 1;
            return Err(anyhow!("missing open bracket"));
        };
        if !line.ends_with(')') {
            self.input_errors += 1;
            return Err(anyhow!("missing end closing bracket"));
        }
        let obis_key = &line[..first_open_bracket_pos];
        let obis_value = &line[first_open_bracket_pos + 1..line.len() - 1];

        if !self.used_obis_keys.insert(obis_key.to_string()) {
            self.input_errors += 1;
            return Err(anyhow!("duplicate key {obis_key}"));
        }

        let Some(p1t_from_key_value) = self.obis_kf.get(obis_key) else {
            self.input_errors += 1;
            return Err(anyhow!("unknown key {obis_key}"));
        };
        // trace!("obis key: {obis_key} value: {obis_value}");
        p1t_from_key_value(self, obis_key, obis_value)
    }

    fn add_version(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_new_value_once("add_version", obis_key, obis_value)
    }

    fn log_key_new_value_once(
        &mut self,
        message: &str,
        obis_key: &str,
        obis_value: &str,
    ) -> Result<()> {
        if self
            .last_logged_keys_values
            .get(obis_key)
            .map_or(true, |last_value| last_value != obis_value)
        {
            debug!("{message}, key: \"{obis_key}\", new value: \"{obis_value}\"");
            self.last_logged_keys_values
                .insert(obis_key.to_owned(), obis_value.to_owned());
        }
        Ok(())
    }

    fn add_power_time_stamp(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        let mlt = strip_optional_suffix_from_time_stamp(obis_value);
        match utc_from_msrmnt_time_str(mlt, self.utc_time_stamp) {
            Ok(utc_time_stamp) => {
                trace!("power_time_stamp utc {utc_time_stamp:?} ");
                self.last_time_stamp_opt = Some(utc_time_stamp);
                Ok(())
            },
            Err(e) => {
                self.input_errors += 1;
                Err(e.context(format!(
                    "add_power_time_stamp key {obis_key} value: {obis_value}"
                )))
            },
        }
    }

    fn add_equipment_id(&mut self, _obis_key: &str, obis_value: &str) -> Result<()> {
        self.last_equipment_id_opt = Some(obis_value.to_string());
        Ok(())
    }

    fn add_kwh_msrmnt_to(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.kwh_msrmnt_type(obis_key, obis_value, MeasurementType::El_kWh_to)
    }

    fn add_kwh_msrmnt_by(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.kwh_msrmnt_type(obis_key, obis_value, MeasurementType::El_kWh_by)
    }

    fn kwh_msrmnt_type(
        &mut self,
        obis_key: &str,
        obis_value: &str,
        msrmnt_type: MeasurementType,
    ) -> Result<()> {
        let Some(msrmnt_val_str) = obis_value.strip_suffix("*kWh") else {
            self.input_errors += 1;
            return Err(anyhow!("no *kWh at end of {obis_value} for key {obis_key}"));
        };
        let msrmnt_val = match BigIntPowTen::from_str(msrmnt_val_str) {
            Ok(bipt) => bipt,
            Err(e) => {
                return Err(e.context(format!(
                    "{msrmnt_val_str} in {obis_value} for key {obis_key}"
                )))
            },
        };
        if let Some(msrmnt) = self.msrmnts.iter_mut().find(|m| m.unit == msrmnt_type) {
            msrmnt.value += msrmnt_val;
        } else {
            let Some(device_id) = &self.last_equipment_id_opt else {
                self.input_errors += 1;
                return Err(anyhow!(
                    "no earlier device id for key {obis_key} value {obis_value} type {msrmnt_type:?}"
                ));
            };
            let time_stamp = if let Some(pts) = self.last_time_stamp_opt {
                pts
            } else {
                self.utc_time_stamp
            };
            self.msrmnts.push(Measurement {
                ref_id: device_id.to_string(),
                unit: msrmnt_type,
                utc_time_stamp: time_stamp,
                value: msrmnt_val,
            });
        }
        Ok(())
    }

    fn add_kw_msrmnt_to(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.kw_msrmnt_type(obis_key, obis_value, MeasurementType::El_kW_to)
    }

    fn add_kw_msrmnt_by(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.kw_msrmnt_type(obis_key, obis_value, MeasurementType::El_kW_by)
    }

    fn kw_msrmnt_type(
        &mut self,
        obis_key: &str,
        obis_value: &str,
        msrmnt_type: MeasurementType,
    ) -> Result<()> {
        let Some(device_id) = &self.last_equipment_id_opt else {
            self.input_errors += 1;
            return Err(anyhow!(
                "no earlier device id for key {obis_key} value {obis_value}"
            ));
        };
        let Some(msrmnt_val_str) = obis_value.strip_suffix("*kW") else {
            return Err(anyhow!("no *kW at end of {obis_value} for key {obis_key}"));
        };
        let msrmnt_val = match BigIntPowTen::from_str(msrmnt_val_str) {
            Ok(bipt) => bipt,
            Err(e) => {
                self.input_errors += 1;
                return Err(e.context(format!(
                    "{msrmnt_val_str} in {obis_value} for key {obis_key}"
                )));
            },
        };
        let time_stamp = if let Some(pts) = self.last_time_stamp_opt {
            pts
        } else {
            self.utc_time_stamp
        };
        self.msrmnts.push(Measurement {
            ref_id: device_id.to_string(),
            unit: msrmnt_type,
            utc_time_stamp: time_stamp,
            value: msrmnt_val,
        });
        Ok(())
    }

    fn add_kw_msrmnt_line(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_new_value_once("instantaneous power l1/l2/l3", obis_key, obis_value)
    }

    fn add_tariff(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_new_value_once("tariff", obis_key, obis_value)
    }

    fn add_text_message(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_new_value_once("text message", obis_key, obis_value)
    }

    fn add_power_failures(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_new_value_once("(long) power failures", obis_key, obis_value)
    }

    fn add_power_failure_event_log(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_new_value_once("power failure event log", obis_key, obis_value)
    }

    fn add_voltage_sags_swells_line(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_new_value_once("voltage sags/swells l1/l2/l3", obis_key, obis_value)
    }

    fn add_instant_current_line(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        self.log_key_new_value_once("instantaneous current l1/l2/l3", obis_key, obis_value)
    }

    fn add_device_type(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        if obis_value != "03" && obis_value != "003" {
            self.input_errors += 1;
            // encountered gas device types: 03 and 003
            return Err(anyhow!(
                "{obis_value} not a gas device type, key {obis_key}"
            ));
        }
        let Some(device_id) = &self.last_equipment_id_opt else {
            self.input_errors += 1;
            return Err(anyhow!(
                "no earlier device id for key {obis_key} value {obis_value}"
            ));
        };
        self.last_equipment_id_opt = Some(device_id.to_string());
        Ok(())
    }

    fn add_time_and_unit(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        // obis_value example: 210925140000)(08)(60)(1)(0-1:24.2.0)(m3
        let Some(close_bracket_pos) = obis_value.find(')') else {
            self.input_errors += 1;
            return Err(anyhow!(
                "missing closing bracket for meter time stamp for key {obis_key} value {obis_value}"
            ));
        };
        let msrmnt_time_str =
            strip_optional_suffix_from_time_stamp(&obis_value[..close_bracket_pos]);
        match utc_from_msrmnt_time_str(msrmnt_time_str, self.utc_time_stamp) {
            Err(e) => {
                self.input_errors += 1;
                return Err(e.context(format!("key {obis_key} value {obis_value}")));
            },
            Ok(mutc) => {
                // FIXME: check the unit, and save it.
                self.last_time_stamp_opt = Some(mutc);
            },
        }
        let m3_suffix = "m3"; // closing bracket already used for obis_value
        if obis_value.ends_with(m3_suffix) {
            self.last_msrmnt_type_opt = Some(MeasurementType::NgNl_m3);
            Ok(())
        } else {
            self.input_errors += 1;
            Err(anyhow!(
                "no measurement unit for {obis_key} value {obis_value}",
            ))
        }
    }

    fn add_time_and_value(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        //     0-1:24.2.1(230826000000S)(09100.175*m3)
        let Some(close_bracket_pos) = obis_value.find(')') else {
            self.input_errors += 1;
            return Err(anyhow!(
                "missing closing bracket for meter time stamp for key {obis_key} value {obis_value}"
            ));
        };
        let utc_time_stamp = match utc_from_msrmnt_time_str(
            strip_optional_suffix_from_time_stamp(&obis_value[..close_bracket_pos]),
            self.utc_time_stamp,
        ) {
            Err(e) => {
                self.input_errors += 1;
                return Err(e.context(format!("key {obis_key} value {obis_value}")));
            },
            Ok(mutc) => mutc,
        };
        let Some(ref ref_id) = self.last_equipment_id_opt else {
            self.input_errors += 1;
            return Err(anyhow!(
                "missing earlier equipment id for value {obis_value}",
            ));
        };
        let m3_suffix = "*m3"; // closing bracket already used for obis_value
        let unit = if obis_value.ends_with(m3_suffix) {
            MeasurementType::NgNl_m3
        } else {
            self.input_errors += 1;
            return Err(anyhow!(
                "no measurement unit for {obis_key} value {obis_value}",
            ));
        };

        if Some("(") != obis_value.get(close_bracket_pos + 1..close_bracket_pos + 2) {
            self.input_errors += 1;
            return Err(anyhow!(
                "missing open bracket after closing bracket: {obis_value}",
            ));
        }
        let value = if let Some(meas_value_str) =
            obis_value.get(close_bracket_pos + 2..obis_value.len() - m3_suffix.len())
        {
            match BigIntPowTen::from_str(meas_value_str) {
                Ok(bipt) => bipt,
                Err(e) => {
                    self.input_errors += 1;
                    return Err(e.context(format!("{meas_value_str} in {obis_value}",)));
                },
            }
        } else {
            self.input_errors += 1;
            return Err(anyhow!(
                "missing open bracket after closing bracket: {obis_value}",
            ));
        };
        self.msrmnts.push(Measurement {
            ref_id: ref_id.clone(),
            utc_time_stamp,
            unit,
            value,
        });
        Ok(())
    }

    fn add_empty_key(&mut self, obis_key: &str, obis_value: &str) -> Result<()> {
        let value = match BigIntPowTen::from_str(obis_value) {
            Err(e) => {
                self.input_errors += 1;
                return Err(e.context(format!("for {obis_value} obis_key {obis_key}")));
            },
            Ok(b) => b,
        };
        let Some(ref ref_id) = self.last_equipment_id_opt else {
            self.input_errors += 1;
            return Err(anyhow!(
                "missing earlier equipment id for value {obis_value}",
            ));
        };
        let Some(utc_time_stamp) = self.last_time_stamp_opt else {
            self.input_errors += 1;
            return Err(anyhow!("missing earlier time stamp for value {obis_value}",));
        };
        let Some(unit) = self.last_msrmnt_type_opt else {
            self.input_errors += 1;
            return Err(anyhow!("missing measurement type for value {obis_value}",));
        };
        self.msrmnts.push(Measurement {
            ref_id: ref_id.clone(),
            utc_time_stamp,
            unit,
            value,
        });
        Ok(())
    }

    fn get_last_logged_keys_values(&self) -> HashMap<String, String> {
        // A reference in P1TelegramBuilder would avoid the need for this method.
        // But a reference needs a lifetime, and that makes the cast "as Akv" above
        // non primitive, not allowed.
        self.last_logged_keys_values.clone()
    }
}

enum TerminalError {
    TimeOut,            // no input at all
    Utf8Error,          // possible parity problem.
    Err(anyhow::Error), // other error
}

fn change_parity(input_terminal: &str, even_parity: bool) -> Result<()> {
    // stty -F /dev/ttyUSB0 evenp -icrnl
    // always apply -icrnl to avoid cr as lf:
    let new_par = format!("{}evenp -icrnl", if even_parity { "" } else { "-" });
    let cmd = format!("stty -F {input_terminal} {new_par}");
    info!("executing: {cmd}");
    bash_cmd_output(&cmd).context(cmd)?;
    Ok(())
}

fn change_baudrate(input_terminal: &str, baudrates: &[u64]) -> Result<()> {
    // Check the baudrate, in case it is in the given baudrates before the last one,
    // change the baudrate to the next in baudrate,
    // otherwise change the baudrate to the first given one.
    // To get the baudrate, use output of stty -F for the input terminal,
    // split this into words at space/newline, and use the word after "speed":
    //   pi@raspy6:~ $ stty -F /dev/ttyUSB0
    //   speed 115200 baud; line = 0;
    //   -brkint -icrnl -imaxbel
    if baudrates.len() < 2 {
        return Err(anyhow!("need at least 2 baudrates: {baudrates:?}"));
    }
    let cmd = format!("stty -F {input_terminal}");
    let stty_stdout = bash_cmd_stdout(&cmd).context("baudrate from stdout")?;
    let mut speed_found = false;
    let Some(baudrate_str) = stty_stdout
        .split_ascii_whitespace()
        .skip_while(|&w| {
            if speed_found {
                false
            } else {
                if w == "speed" {
                    speed_found = true
                };
                true
            }
        })
        .take(1)
        .next()
    else {
        return Err(anyhow!("no baudrate in {stty_stdout} from {cmd}"));
    };
    let current_baudrate = baudrate_str
        .parse::<u64>()
        .map_err(Error::new)
        .with_context(|| format!("baudrate {baudrate_str} from {input_terminal}"))?;
    let next_baudrate = if let Some((pos, _)) = baudrates
        .iter()
        .enumerate()
        .find(|(_, &br)| br == current_baudrate)
    {
        if let Some(nbr) = baudrates.get(pos + 1) {
            *nbr
        } else {
            baudrates[0]
        }
    } else {
        baudrates[0]
    };
    bash_cmd_output(&format!("stty -F {input_terminal} {next_baudrate}"))?;
    trace!("changed {input_terminal} baudrate from {current_baudrate} to {next_baudrate}");
    Ok(())
}

fn read_p1_telegrams(
    in_file: &mut File,
    config: &P1EncryptConfig,
    p1_sender: &tokio_mpsc::Sender<P1Telegram>,
    run_state: &RunState,
) -> TerminalError {
    let epoll_raw_fd = match epoll_create() {
        Err(e) => return TerminalError::Err(Error::new(e).context("epoll_create")),
        Ok(raw_fd) => raw_fd,
    };

    if let Err(e) = epoll_ctl(
        epoll_raw_fd,
        EpollOp::EpollCtlAdd,
        in_file.as_raw_fd(),                                // poll on in_file
        Some(&mut EpollEvent::new(EpollFlags::EPOLLIN, 0)), // poll for input available.
    ) {
        return TerminalError::Err(Error::new(e).context("epoll_ctl"));
    }

    let max_wait_for_byte_secs = 60;
    let max_unicode_errors = 300;
    let mut unicode_errors = 0;

    let mut events = [EpollEvent::empty(); 2];
    let mut buf = [0u8; 1];

    let mut p1_telegram_builder_opt: Option<P1TelegramBuilder> = None;
    let mut last_line = String::new();

    let mut last_used_p1_dt_opt: Option<DateTime<Utc>> = None;

    let mut final_telegram_line = false; // allow for final line with crc: !6DF1

    let mut last_logged_keys_values = HashMap::<String, String>::new();
    let mut last_clear_logged_key_values = time_stamp_milli_secs();

    loop {
        match epoll_wait(
            epoll_raw_fd,
            &mut events,
            (max_wait_for_byte_secs * 1000) as isize,
        ) {
            Err(e) => {
                return TerminalError::Err(Error::new(e).context("epoll_wait"));
            },
            Ok(num_events) => {
                if num_events == 0 {
                    return TerminalError::TimeOut;
                } else if num_events != 1 {
                    return TerminalError::Err(anyhow!(
                        "epoll_wait unknown number of events {num_events}"
                    ));
                }
            },
        }

        if let Err(e) = run_state.is_running() {
            return TerminalError::Err(e);
        }

        // single event from POLLIN on in_file:
        let input_char = match in_file.read(&mut buf) {
            Err(e) => return TerminalError::Err(Error::new(e).context("read")),
            Ok(num_bytes) => {
                if num_bytes != 1 {
                    return TerminalError::Err(anyhow!(
                        "unexpected number of read bytes {num_bytes}"
                    ));
                }
                match std::str::from_utf8(&buf) {
                    Err(_) => {
                        unicode_errors += 1;
                        if let Some(ref mut p1_telegram_builder) = p1_telegram_builder_opt {
                            p1_telegram_builder.input_errors += 1;
                        }
                        if unicode_errors >= max_unicode_errors {
                            return TerminalError::Utf8Error;
                        }
                        // poll next byte
                        continue;
                    },
                    Ok(s) => {
                        if let Some(ch) = s.chars().next() {
                            ch
                        } else {
                            error!("buffer {buf:?} valid single byte string without char");
                            continue;
                        }
                    },
                }
            },
        };

        match input_char {
            '/' => {
                let mut p1_telegram_builder = P1TelegramBuilder::new(&last_logged_keys_values);
                p1_telegram_builder.add_crc(input_char as u8);
                final_telegram_line = false; // start a new p1 telegram
                trace!(
                    "starting p1 telegram, utc_time_stamp: {:?}",
                    p1_telegram_builder.utc_time_stamp
                );
                p1_telegram_builder_opt = Some(p1_telegram_builder);
                last_line.clear();
            },
            '!' => {
                if let Some(ref mut p1_telegram_builder) = p1_telegram_builder_opt {
                    p1_telegram_builder.add_crc(input_char as u8);
                }
                final_telegram_line = true;
            },
            '\n' | '\r' => {
                if final_telegram_line {
                    if let Some(ref mut p1_telegram_builder) = p1_telegram_builder_opt.take() {
                        if !last_line.is_empty() {
                            warn!(
                                "ignoring crc on last line {last_line}, expected {:04X}",
                                p1_telegram_builder.crc16
                            );
                        }
                        let now_dt = time_stamp_milli_secs();
                        let use_p1 = if p1_telegram_builder.input_errors == 0 {
                            if let Some(last_used_p1_dt) = last_used_p1_dt_opt {
                                if last_used_p1_dt + config.min_p1_interval.chrono_duration()
                                    < now_dt
                                {
                                    true
                                } else {
                                    warn!(
                                        "ignoring too early p1 telegram, min_p1_interval {}",
                                        config.min_p1_interval
                                    );
                                    false
                                }
                            } else {
                                true
                            }
                        } else {
                            warn!(
                                "ignoring p1 telegram with {} input errors",
                                p1_telegram_builder.input_errors
                            );
                            false
                        };
                        if use_p1 {
                            last_used_p1_dt_opt = Some(now_dt);
                            // end a started P1Telegram.
                            match p1_telegram_builder.build(
                                &config.window_msrmnt_time_start.chrono_duration(),
                                &config.window_msrmnt_time_end.chrono_duration(),
                            ) {
                                Ok(p1_telegram) => {
                                    p1_telegram.log_msrmnts();
                                    if let Err(queue_send_err) = p1_sender.try_send(p1_telegram) {
                                        match queue_send_err {
                                            tokio_mpsc::error::TrySendError::Full(_full_error) => {
                                                warn!("queue full_error, ignored p1_telegram");
                                            },
                                            tokio_mpsc::error::TrySendError::Closed(
                                                _option_mkvs,
                                            ) => {
                                                return TerminalError::Err(anyhow!("queue closed"));
                                            },
                                        }
                                    }
                                },
                                Err(e) => {
                                    error!("{e:#} at p1 telegram build");
                                },
                            }
                            let now = time_stamp_milli_secs();
                            if last_clear_logged_key_values
                                + config.log_ignored_values_interval.chrono_duration()
                                > now
                            {
                                last_logged_keys_values =
                                    p1_telegram_builder.get_last_logged_keys_values()
                            } else {
                                last_logged_keys_values.clear();
                                last_clear_logged_key_values = now;
                            }
                        }
                    } else {
                        info!("ending unstarted p1 telegram: {input_char}"); // ignore end char, no telegram started.
                    }
                    last_line.clear();
                }
                if !last_line.is_empty() {
                    if let Some(ref mut p1_telegram_builder) = p1_telegram_builder_opt {
                        p1_telegram_builder.add_line(&last_line);
                    } else {
                        info!("unstarted p1 telegram line: {last_line}");
                    }
                    last_line.clear();
                }
            },
            _ => {
                if let Some(ref mut p1_telegram_builder) = p1_telegram_builder_opt {
                    p1_telegram_builder.add_crc(input_char as u8);
                }
                last_line.push(input_char);
            },
        }
    }
}

pub(crate) fn read_p1_telegrams_changing_parity(
    config: &P1EncryptConfig,
    p1_sender: &tokio_mpsc::Sender<P1Telegram>,
    run_state: &RunState,
) -> Result<()> {
    let addr_port = addr_port_str(
        &config.p1_excl_bind_address,
        &format!("{}", config.p1_excl_port),
    )?;
    // Keep _udp_sock open to prevent other programs from opening it
    // before using the p1_terminal.
    let _udp_sock = udp_bind(&addr_port)
        .map_err(|e| e.context(format!("udp_bind, refuse to read {}", &config.p1_terminal)))?;
    loop {
        let mut in_file = match File::open(&config.p1_terminal) {
            Ok(f) => f,
            Err(e) => {
                error!("{e:?} at open() p1_terminal {}", &config.p1_terminal);
                error!("waiting 60 secs, is the P1 port connected?");
                thread::sleep(Duration::from_secs(60));
                continue;
            },
        };
        let mut parity_changes: u64 = 0;
        loop {
            match read_p1_telegrams(&mut in_file, config, p1_sender, run_state) {
                TerminalError::Err(e) => return Err(e.context("read_p1_telegrams")),
                TerminalError::TimeOut => {
                    warn!("timeout on P1 input {}, connection ok?", config.p1_terminal);
                    // switch terminal baudrate between 9600 and 115200:
                    if let Err(e) = change_baudrate(&config.p1_terminal, &config.baudrates) {
                        error!("{e:#} at change_baudrate");
                        error!("reopening p1_terminal {}", &config.p1_terminal);
                        break;
                    }
                },
                TerminalError::Utf8Error => {
                    if parity_changes > config.max_parity_changes {
                        error!("reached max_parity_changes {}", config.max_parity_changes);
                        error!("reopening p1_terminal {}", &config.p1_terminal);
                        break;
                    }
                    if let Err(e) = change_parity(&config.p1_terminal, parity_changes % 2 == 1) {
                        error!("{e:#} at change_parity");
                        error!("reopening p1_terminal {}", &config.p1_terminal);
                        break;
                    } else {
                        parity_changes += 1;
                    }
                },
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use enmplib_rs::util::time_stamp_from_str;
    use std::time::SystemTime;
    use MeasurementType::*;

    use std::sync::Once;
    static INIT: Once = Once::new();
    pub fn init_log4rs() {
        // init the log so it shows on stdout
        INIT.call_once(|| {
            let home = std::env::var("HOME").unwrap();
            // yaml file on local machine
            if log4rs::init_file(
                format!("{home}/gitrepos/enmprv/p1encrypt-rs/p1etestlog4rs.yaml"),
                log4rs::config::Deserializers::default(),
            )
            .is_err()
            {
                // yaml file on remote machine
                log4rs::init_file(
                    format!("{home}/enmprv/p1etestlog4rs.yaml"),
                    log4rs::config::Deserializers::default(),
                )
                .unwrap();
            }
        });
    }

    fn test_parse_p1_text(
        p1_text: &str,
        exp_msrmnts: &[Measurement],
        test_now: DateTime<Utc>,
        exp_last_logged_keys_values: &HashMap<&str, &str>,
    ) {
        init_log4rs();
        let mut ignore_keys_values = HashMap::<String, String>::new();
        ignore_keys_values.insert("0-0:96.13.0".to_owned(), "".to_owned()); // ignore empty text message
        let mut p1b = P1TelegramBuilder::new(&ignore_keys_values);
        {
            let mut started = false;
            for b in p1_text.bytes() {
                if b == b'/' {
                    started = true;
                }
                if started {
                    p1b.add_crc(b);
                }
                if b == b'!' {
                    break;
                }
            }
        }
        p1b.utc_time_stamp = test_now;
        for line in p1_text.lines() {
            // dbg!(line);
            p1b.add_line(line);
            assert_eq!(p1b.input_errors, 0);
        }
        // println!("{:#?}", p1b.msrmnts);
        assert_eq!(exp_msrmnts.len(), p1b.msrmnts.len());
        for (i, (exp_msrmnt, result_msrmnt)) in exp_msrmnts.iter().zip(&p1b.msrmnts).enumerate() {
            let mes = format!("msrmnt {i}");
            assert_eq!(exp_msrmnt.ref_id, result_msrmnt.ref_id, "ref_id {mes}");
            assert_eq!(
                exp_msrmnt.utc_time_stamp, result_msrmnt.utc_time_stamp,
                "utc_time_stamp {mes}"
            );
            assert_eq!(exp_msrmnt.unit, result_msrmnt.unit, "unit {mes}");
            assert_eq!(exp_msrmnt.value, result_msrmnt.value, "value {mes}");
        }
        let last_logged_keys_values = p1b.get_last_logged_keys_values();
        assert_eq!(
            exp_last_logged_keys_values.len(),
            last_logged_keys_values.len()
        );
        for (&exp_key, &exp_val) in exp_last_logged_keys_values {
            assert_eq!(
                Some(&exp_val.to_string()),
                last_logged_keys_values.get(exp_key)
            );
        }
    }

    fn msrmnt(ref_id: &str, utc_stamp: &str, unit: MeasurementType, value: &str) -> Measurement {
        Measurement {
            ref_id: ref_id.to_string(),
            utc_time_stamp: time_stamp_from_str(utc_stamp).unwrap(),
            unit,
            value: BigIntPowTen::from_str(value).unwrap(),
        }
    }

    #[test]
    fn test_p1_parse_a() {
        let p1_text = "/XMX5LGBBFFB231204853

1-3:0.2.8(42)
0-0:1.0.0(230826001741S)
0-0:96.1.1(4530303034303031353932393031343134)
1-0:1.8.1(012057.792*kWh)
1-0:2.8.1(000000.000*kWh)
1-0:1.8.2(014488.290*kWh)
1-0:2.8.2(000000.004*kWh)
0-0:96.14.0(0001)
1-0:1.7.0(00.165*kW)
1-0:2.7.0(00.000*kW)
0-0:96.7.21(00000)
0-0:96.7.9(00003)
1-0:99.97.0(3)(0-0:96.7.19)(170720162945S)(0000001598*s)(170326055407S)(0029785553*s)(160415120840S)(0037051525*s)
1-0:32.32.0(00000)
1-0:32.36.0(00000)
0-0:96.13.1()
0-0:96.13.0()
1-0:31.7.0(001*A)
1-0:21.7.0(00.165*kW)
1-0:22.7.0(00.000*kW)
0-1:24.1.0(003)
0-1:96.1.0(4730303233353631323236303936333134)
0-1:24.2.1(230826000000S)(09100.175*m3)
!6DF1
";
        let el_ref_id = "4530303034303031353932393031343134";
        let el_stamp = "2023-08-25T22:17:41Z";
        let exp_msrmnts = vec![
            msrmnt(el_ref_id, el_stamp, El_kWh_to, "26546.082"),
            msrmnt(el_ref_id, el_stamp, El_kWh_by, "0.004"),
            msrmnt(el_ref_id, el_stamp, El_kW_to, "0.165"),
            msrmnt(el_ref_id, el_stamp, El_kW_by, "0"),
            msrmnt(
                "4730303233353631323236303936333134",
                "2023-08-25T22:00:00Z",
                NgNl_m3,
                "9100.175",
            ),
        ];
        let exp_last_logged_keys_values = HashMap::from_iter([
            ("0-0:96.7.9", "00003"),
            ("1-0:32.32.0", "00000"),
            ("0-0:96.7.21", "00000"),
            ("1-3:0.2.8", "42"),
            ("1-0:21.7.0", "00.165*kW"),
            ("0-0:96.14.0", "0001"),
            ("1-0:99.97.0", "3)(0-0:96.7.19)(170720162945S)(0000001598*s)(170326055407S)(0029785553*s)(160415120840S)(0037051525*s"),
            ("1-0:31.7.0", "001*A"),
            ("1-0:22.7.0", "00.000*kW"),
            ("0-0:96.13.0", ""),
            ("0-0:96.13.1", ""),
            ("1-0:32.36.0", "00000"),
        ]);
        test_parse_p1_text(
            p1_text,
            &exp_msrmnts,
            chrono::DateTime::<Utc>::from(SystemTime::now()),
            &exp_last_logged_keys_values,
        );
    }

    #[test]
    fn test_p1_parse_b() {
        let p1_text = "/XMX5XMXABCE000062328

0-0:96.1.1(31333631323039342020202020202020)
1-0:1.8.1(28194.060*kWh)
1-0:1.8.2(37773.828*kWh)
1-0:2.8.1(03016.402*kWh)
1-0:2.8.2(06355.026*kWh)
0-0:96.14.0(0001)
1-0:1.7.0(0003.12*kW)
1-0:2.7.0(0000.00*kW)
0-0:96.13.1()
0-0:96.13.0()
0-1:96.1.0(3338303034303038323431313331363132)
0-1:24.1.0(03)
0-1:24.3.0(230827170000)(08)(60)(1)(0-1:24.2.0)(m3)
(20204.264)
!
";
        let el_ref_id = "31333631323039342020202020202020";
        let el_stamp = "2023-08-27T20:31:45.531Z";
        let test_now = time_stamp_from_str(el_stamp).unwrap();
        let exp_msrmnts = vec![
            msrmnt(el_ref_id, el_stamp, El_kWh_to, "65967.888"),
            msrmnt(el_ref_id, el_stamp, El_kWh_by, "9371.428"),
            msrmnt(el_ref_id, el_stamp, El_kW_to, "3.12"),
            msrmnt(el_ref_id, el_stamp, El_kW_by, "0"),
            msrmnt(
                "3338303034303038323431313331363132",
                "2023-08-27T15:00:00Z",
                NgNl_m3,
                "20204.264",
            ),
        ];
        let exp_last_logged_keys_values = HashMap::from_iter([
            ("0-0:96.13.0", ""),
            ("0-0:96.13.1", ""),
            ("0-0:96.14.0", "0001"),
        ]);
        test_parse_p1_text(
            p1_text,
            &exp_msrmnts,
            test_now,
            &exp_last_logged_keys_values,
        );
    }

    #[test]
    fn test_p1_parse_c() {
        let p1_text = "/XMX5XMXABCE000062328

0-0:96.1.1(31333631323039342020202020202020)
1-0:1.8.1(28194.064*kWh)
1-0:1.8.2(37773.828*kWh)
1-0:2.8.1(03016.402*kWh)
1-0:2.8.2(06355.026*kWh)
0-0:96.14.0(0001)
1-0:1.7.0(0000.99*kW)
1-0:2.7.0(0000.00*kW)
0-0:96.13.1()
0-0:96.13.0()
0-1:96.1.0(3338303034303038323431313331363132)
0-1:24.1.0(03)
0-1:24.3.0(230827170000)(08)(60)(1)(0-1:24.2.0)(m3)
(20204.264)
!
";
        let el_ref_id = "31333631323039342020202020202020";
        let el_stamp = "2023-08-27T20:31:45.531Z";
        let test_now = time_stamp_from_str(el_stamp).unwrap();
        let exp_msrmnts = vec![
            msrmnt(el_ref_id, el_stamp, El_kWh_to, "65967.892"),
            msrmnt(el_ref_id, el_stamp, El_kWh_by, "9371.428"),
            msrmnt(el_ref_id, el_stamp, El_kW_to, "0.99"),
            msrmnt(el_ref_id, el_stamp, El_kW_by, "0"),
            msrmnt(
                "3338303034303038323431313331363132",
                "2023-08-27T15:00:00Z",
                NgNl_m3,
                "20204.264",
            ),
        ];
        let exp_last_logged_keys_values = HashMap::from_iter([
            ("0-0:96.13.0", ""),
            ("0-0:96.13.1", ""),
            ("0-0:96.14.0", "0001"),
        ]);
        test_parse_p1_text(
            p1_text,
            &exp_msrmnts,
            test_now,
            &exp_last_logged_keys_values,
        );
    }
}
