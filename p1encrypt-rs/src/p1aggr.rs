use std::net::{SocketAddr, UdpSocket};
use std::sync::{mpsc, Arc, Mutex};
use std::thread;
use std::time::Instant;

use anyhow::{anyhow, Context, Error, Result};
use enmplib_rs::util::{addr_port_str, ip_addr_in_wg_intf, time_stamp_milli_secs, udp_bind};
use log::{debug, error, log, trace, warn, Level};
use num::{pow, BigInt};

use enmplib_rs::{
    bigintpowten::BigIntPowTen,
    encryptionseed::{encryption_key, ShareSeedAddResult, SharedSeeds},
    util::time_stamp_to_string,
};

use crate::{Measurement, P1EncryptConfig, P1Telegram, P1TelegramOrSharedSeedRequest};

fn key_value_line(key: &str, val: &str) -> String {
    let res = [key, val].join(": ");
    trace!("key_value_line {res}");
    res
}

fn key_msrmnt_value_line(key: &str, val: &BigIntPowTen, decimals: u64) -> String {
    key_value_line(key, &val.to_string(decimals))
}

fn encrypt_value(
    msrmnt: &Measurement,
    timestamp: &str,
    seed: &[u8],
    enc_modulus: &BigInt,
    ten_pow: i32,
) -> BigIntPowTen {
    let enc_key = encryption_key(
        seed,
        msrmnt.ref_id.as_bytes(),
        timestamp.as_bytes(),
        msrmnt.unit,
        enc_modulus,
    );
    &msrmnt.value + &BigIntPowTen::new(enc_key, ten_pow)
}

struct P1DatagramConfig {
    ean_key: &'static str,
    mutc_key: &'static str,
    enc_key_suffix: &'static str,
}

impl P1DatagramConfig {
    const fn new() -> Self {
        Self {
            ean_key: "ean",
            mutc_key: "mutc",
            enc_key_suffix: ".enc",
        }
    }
}

impl P1Telegram {
    fn datagram_str(
        &self,
        p1_datagram_config: &P1DatagramConfig,
        send_non_encrypted: bool,
        shared_seeds: &SharedSeeds,
        dec_digits_unencrypted: u64,
    ) -> Result<String> {
        let mut last_id_opt: Option<String> = None;
        let mut last_timestamp_opt: Option<String> = None;
        let mut lines: Vec<String> = Vec::new();
        let mut some_value = false;
        for msrmnt in &self.msrmnts {
            let put_id = if let Some(ref last_id) = last_id_opt {
                if *last_id == msrmnt.ref_id {
                    false
                } else {
                    last_id_opt = Some(msrmnt.ref_id.clone());
                    true
                }
            } else {
                last_id_opt = Some(msrmnt.ref_id.clone());
                true
            };
            let timestamp = time_stamp_to_string(&msrmnt.utc_time_stamp);
            let put_timestamp = if let Some(ref last_timestamp) = last_timestamp_opt {
                if *last_timestamp == timestamp {
                    false
                } else {
                    last_timestamp_opt = Some(timestamp.clone());
                    true
                }
            } else {
                last_timestamp_opt = Some(timestamp.clone());
                true
            };

            if put_id {
                lines.push(key_value_line(p1_datagram_config.ean_key, &msrmnt.ref_id));
            }
            if put_timestamp {
                lines.push(key_value_line(p1_datagram_config.mutc_key, &timestamp));
            }

            let msrmnt_type_key = msrmnt.unit.to_str();

            if send_non_encrypted {
                lines.push(key_msrmnt_value_line(
                    msrmnt_type_key,
                    &msrmnt.value,
                    dec_digits_unencrypted,
                ));
                some_value = true;
            }
            match shared_seeds.get_encryption_info(msrmnt.ref_id.as_bytes(), timestamp.as_bytes()) {
                Err(e) => {
                    warn!("{e:#}, no shared seed for {} at {timestamp}", msrmnt.ref_id);
                },
                Ok(ei) => {
                    let enc_modulus = pow(BigInt::from(10), ei.ten_pow_modulus as usize);
                    let ten_pow = -match i32::try_from(ei.ten_pow_divisor) {
                        Ok(d) => d,
                        Err(e) => {
                            return Err(Error::new(e)
                                .context(format!("i32::try_from {}", ei.ten_pow_divisor)))
                        },
                    };

                    lines.push(key_msrmnt_value_line(
                        &[msrmnt_type_key, p1_datagram_config.enc_key_suffix].join(""),
                        &encrypt_value(msrmnt, &timestamp, &ei.seed, &enc_modulus, ten_pow),
                        ei.ten_pow_divisor.into(),
                    ));
                    some_value = true;
                },
            }
        }

        if lines.is_empty() {
            Err(anyhow!("empty"))
        } else if !some_value {
            Err(anyhow!("no measurement value"))
        } else {
            Ok(lines.join("\n"))
        }
    }
}

pub(crate) fn encrypt_p1_telegrams_to_udp(
    from_encrypt_chan: &mpsc::Receiver<P1TelegramOrSharedSeedRequest>,
    shared_udp_socket: &Mutex<Option<UdpSocket>>,
    remote_aggr_address_port: SocketAddr,
    config: &P1EncryptConfig,
) -> Result<()> {
    let p1e_ssl_signer = config
        .p1e_ssl_signer_file
        .open_ssl_signer(&config.temp_dir)?;
    let p1_datagram_config = P1DatagramConfig::new();

    let dec_digits_unencrypted: u64 = config.shared_seeds.ten_pow_divisor.into();
    let keep_interval = config.shared_seeds.keep_interval.chrono_duration();

    let mut shared_seeds = SharedSeeds::new();
    let mut bytes = Vec::new();

    let mut udp_socket_opt: Option<UdpSocket> = None;
    loop {
        if udp_socket_opt.is_none() {
            match ip_addr_in_wg_intf(&config.bind_address_aggr_meters) {
                Err(e) => {
                    error!(
                        "{e:#} at: ip_addr_in_wg_intf ip interface bind_address_aggr_meters {}",
                        &config.bind_address_aggr_meters
                    );
                },
                Ok(in_wg_intf) => {
                    if in_wg_intf {
                        let bind_addr_port_aggr_meters = addr_port_str(
                            &config.bind_address_aggr_meters,
                            &config.bind_port_aggr_meters.to_string(),
                        )?;
                        match udp_bind(&bind_addr_port_aggr_meters) {
                            Ok(udp_socket) => {
                                match udp_socket.try_clone() {
                                    Ok(udp_socket_clone) => match shared_udp_socket.lock() {
                                        Ok(mut udp_sock_opt_guard) => {
                                            *udp_sock_opt_guard = Some(udp_socket_clone);
                                        },
                                        Err(poison) => {
                                            error!("{poison:?} at lock");
                                        },
                                    },
                                    Err(e) => {
                                        error!("{e:?} at try_clone");
                                    },
                                }
                                udp_socket_opt = Some(udp_socket);
                            },
                            Err(e) => {
                                if let Some(ioe) = e.downcast_ref::<std::io::ErrorKind>() {
                                    if ioe == &std::io::ErrorKind::AddrInUse {
                                        return Err(
                                            e.context("udp_bind, no way to connect to aggregator")
                                        );
                                    }
                                }
                                error!("{e:#} at udp_bind {bind_addr_port_aggr_meters}");
                            },
                        }
                    } else {
                        warn!(
                            "address {} is not at a wireguard interface",
                            &config.bind_address_aggr_meters
                        );
                    }
                },
            };
        }

        match from_encrypt_chan.recv().context("recv")? {
            P1TelegramOrSharedSeedRequest::ShareSeedRequest(shared_seed_req) => {
                let current_time_dt = time_stamp_milli_secs();
                let keep_oldest_dt = current_time_dt - keep_interval;
                match shared_seeds.add_share_seed_request(
                    &shared_seed_req,
                    &keep_oldest_dt,
                    &current_time_dt,
                ) {
                    Ok(ShareSeedAddResult::Ok) => {},
                    Ok(ShareSeedAddResult::Duplicate) => {
                        trace!("duplicate shared_seed_req");
                    },
                    Err(e) => {
                        error!("{e:#} at add_share_seed_request");
                    },
                }
            },
            P1TelegramOrSharedSeedRequest::P1Telegram(p1_telegram) => {
                let datagram_res = p1_telegram.datagram_str(
                    &p1_datagram_config,
                    config.send_non_encrypted.unwrap_or(false),
                    &shared_seeds,
                    dec_digits_unencrypted,
                );
                let telegram_bytes = match datagram_res {
                    Ok(ref b) => b.as_bytes(),
                    Err(e) => {
                        // this can be before the first shared seed interval
                        warn!("{e:#} at datagram_res, ignore");
                        continue; // next p1_telegram_or_shared_seed
                    },
                };
                let sign_start_time = Instant::now();
                let signature = p1e_ssl_signer
                    .generate_signature(telegram_bytes)
                    .context("generate_signature")?;
                let sign_duration_opt = Instant::now().checked_duration_since(sign_start_time);
                bytes.clear();
                bytes.extend([0u8, 0u8]); // version
                bytes.extend(telegram_bytes);
                bytes.extend("\n\n".as_bytes());
                bytes.extend(signature);

                if let Some(ref udp_socket) = udp_socket_opt {
                    match udp_socket.send_to(&bytes, remote_aggr_address_port) {
                        Ok(size) => {
                            if size != bytes.len() {
                                error!("sent {size} bytes of {}", bytes.len());
                            }
                        },
                        Err(e) => error!("{e:?} for length {}", bytes.len()),
                    }
                } else {
                    warn!("no udp socket available to send p1_telegram")
                }
                if let Some(sign_duration) = sign_duration_opt {
                    debug!(
                        "telegram_bytes: {}, total including signature {}, sign_duration {} ms",
                        telegram_bytes.len(),
                        bytes.len(),
                        sign_duration.as_millis(),
                    );
                } else {
                    error!(
                        "telegram_bytes: {}, total including signature {}, but no sign duration",
                        telegram_bytes.len(),
                        bytes.len()
                    );
                }
            },
        }
    }
}

pub(crate) fn show_udp_replies(
    shared_udp_sock: &Arc<Mutex<Option<UdpSocket>>>,
    remote_aggr_addr_port: SocketAddr,
    config: &P1EncryptConfig,
) -> ! {
    // return error message, if any.
    const BUF_SIZE: usize = 1024;
    let mut receive_buffer = [0u8; BUF_SIZE];
    let mut udp_socket_opt: Option<UdpSocket> = None;
    let mut waited_once: bool = false;
    loop {
        match shared_udp_sock.lock() {
            Ok(mut udp_sock_opt_guard) => match udp_sock_opt_guard.take() {
                Some(udp_socket) => {
                    udp_socket_opt = Some(udp_socket);
                },
                None => {
                    if udp_socket_opt.is_none() && waited_once {
                        warn!("no socket available");
                    }
                },
            },
            Err(poison) => {
                error!("{poison:?} at lock");
            },
        }
        if let Some(ref udp_sock) = udp_socket_opt {
            match udp_sock.recv_from(&mut receive_buffer) {
                Err(sock_err) => {
                    error!("{sock_err:?} at recv_from");
                    udp_socket_opt = None; // try and take a new socket from shared_udp_sock
                },
                Ok((num_bytes_received, src_addr_port)) => {
                    if src_addr_port != remote_aggr_addr_port {
                        warn!(
                            "unexpected address_port {src_addr_port}, message length {num_bytes_received}"
                        );
                    } else if num_bytes_received == BUF_SIZE {
                        warn!("buffer full, message length {num_bytes_received}");
                    } else {
                        match std::str::from_utf8(&receive_buffer[..num_bytes_received]) {
                            Err(utf8_err) => warn!("utf8_err {utf8_err:?}"),
                            Ok(s) => {
                                // s may contain an aggregation result after a dot.
                                log!(
                                    if s.starts_with('.') {
                                        Level::Info
                                    } else {
                                        Level::Warn
                                    },
                                    "{s}",
                                );
                            },
                        }
                    }
                },
            }
        } else {
            thread::sleep(config.aggr_meters_timeout.std_duration());
            waited_once = true;
        }
    }
}
