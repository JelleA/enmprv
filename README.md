# enmprv

# Energy meter privacy by multi party encrypted addition of meter values

This repository contains a minimal prototype for smart meter aggregation method
that is described in this report (for good viewing, please download the pdf and use a local pdf viewer):
https://gitlab.com/YpeKingma/publications/-/blob/master/meterpriv/Kingma2020MeterPrivEN.pdf

Dutch versions of the report and slides for the report are available here:
https://gitlab.com/YpeKingma/publications/

See Figures 1 and 2 of this report for a quick overview. Figure 2 shows two meters
that each have their P1 port connected to a small computer that encrypts a meter value by adding a random value as an encryption key. This computer is called the p1 encryptor here.
The key adder S receives all these encryption keys, sums them up into a total key, and sends this total as a decryption key to the decryptor/aggregator T.
The decryptor of the total receives the encrypted meter values, adds them, and subtracts the total key that receives from the key adder. The result is the aggregation of the meter values.

A report on the prototype of September 2021 is here
https://gitlab.com/enmprivaggr/enmprv/-/blob/master/Report202109.md .


# License

Licensed under either of:

- Apache License, Version 2.0
  (https://gitlab.com/enmprivaggr/enmprv/-/tree/master/LICENSE-APACHE or
  https://www.apache.org/licenses/LICENSE-2.0)
- MIT license 
  (https://gitlab.com/enmprivaggr/enmprv/-/tree/master/LICENSE-MIT or
  https://opensource.org/licenses/MIT)

at your option.

# Contribution

Unless you explicitly state otherwise, any Contribution intentionally submitted for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.
